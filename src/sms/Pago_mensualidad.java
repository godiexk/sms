package sms;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import javax.swing.ImageIcon;

public class Pago_mensualidad extends javax.swing.JFrame {

    public Pago_mensualidad() {

        initComponents();
        setLocationRelativeTo(null);
        setTitle("Pago de la mensualidad");

        Code.Verificar_conexion();

        jTextField_beneficiario.requestFocus();
    }

    public static int id_usuario;

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDialog_beneficiario = new javax.swing.JDialog();
        jPanel8 = new javax.swing.JPanel();
        jScrollPane6 = new javax.swing.JScrollPane();
        jTable_beneficiario = new javax.swing.JTable();
        jTextField_buscar_beneficiario = new javax.swing.JTextField();
        jDialog_pagar = new javax.swing.JDialog();
        jPanel9 = new javax.swing.JPanel();
        jTextField_factura = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jCheckBox_carnet = new javax.swing.JCheckBox();
        jPanel1 = new javax.swing.JPanel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel4 = new javax.swing.JPanel();
        jTextField_beneficiario = new javax.swing.JTextField();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTable_detalle = new javax.swing.JTable();

        jDialog_beneficiario.setResizable(false);

        jPanel8.setBackground(new java.awt.Color(255, 255, 255));
        jPanel8.setPreferredSize(new java.awt.Dimension(600, 600));

        jTable_beneficiario.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Nombre"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable_beneficiario.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable_beneficiarioMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jTable_beneficiarioMouseEntered(evt);
            }
        });
        jTable_beneficiario.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTable_beneficiarioKeyPressed(evt);
            }
        });
        jScrollPane6.setViewportView(jTable_beneficiario);
        if (jTable_beneficiario.getColumnModel().getColumnCount() > 0) {
            jTable_beneficiario.getColumnModel().getColumn(0).setResizable(false);
            jTable_beneficiario.getColumnModel().getColumn(0).setPreferredWidth(30);
            jTable_beneficiario.getColumnModel().getColumn(1).setResizable(false);
            jTable_beneficiario.getColumnModel().getColumn(1).setPreferredWidth(370);
        }

        jTextField_buscar_beneficiario.setBorder(javax.swing.BorderFactory.createTitledBorder("Buscar"));
        jTextField_buscar_beneficiario.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField_buscar_beneficiarioKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextField_buscar_beneficiarioKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jTextField_buscar_beneficiario)
                    .addComponent(jScrollPane6))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTextField_buscar_beneficiario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 533, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jDialog_beneficiarioLayout = new javax.swing.GroupLayout(jDialog_beneficiario.getContentPane());
        jDialog_beneficiario.getContentPane().setLayout(jDialog_beneficiarioLayout);
        jDialog_beneficiarioLayout.setHorizontalGroup(
            jDialog_beneficiarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, 472, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        jDialog_beneficiarioLayout.setVerticalGroup(
            jDialog_beneficiarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, 593, Short.MAX_VALUE)
        );

        jDialog_pagar.setUndecorated(true);
        jDialog_pagar.setResizable(false);

        jPanel9.setBackground(new java.awt.Color(255, 255, 255));
        jPanel9.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel9.setPreferredSize(new java.awt.Dimension(600, 600));

        jTextField_factura.setBorder(javax.swing.BorderFactory.createTitledBorder("Número de Factura"));
        jTextField_factura.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField_facturaActionPerformed(evt);
            }
        });
        jTextField_factura.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField_facturaKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextField_facturaKeyReleased(evt);
            }
        });

        jButton1.setText("Imprimir");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Cancelar");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jCheckBox_carnet.setBackground(new java.awt.Color(255, 255, 255));
        jCheckBox_carnet.setText("Pagar Carnet");

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jTextField_factura, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jButton2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 178, Short.MAX_VALUE)
                    .addComponent(jCheckBox_carnet, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap(14, Short.MAX_VALUE)
                .addComponent(jTextField_factura, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jCheckBox_carnet)
                .addGap(23, 23, 23)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jDialog_pagarLayout = new javax.swing.GroupLayout(jDialog_pagar.getContentPane());
        jDialog_pagar.getContentPane().setLayout(jDialog_pagarLayout);
        jDialog_pagarLayout.setHorizontalGroup(
            jDialog_pagarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel9, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        jDialog_pagarLayout.setVerticalGroup(
            jDialog_pagarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE)
        );

        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowActivated(java.awt.event.WindowEvent evt) {
                formWindowActivated(evt);
            }
        });
        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                formKeyPressed(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));

        jTextField_beneficiario.setToolTipText("");
        jTextField_beneficiario.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Beneficiario", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(51, 51, 255))); // NOI18N
        jTextField_beneficiario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField_beneficiarioActionPerformed(evt);
            }
        });
        jTextField_beneficiario.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField_beneficiarioKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextField_beneficiarioKeyReleased(evt);
            }
        });

        jTable_detalle.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Factura", "Nombre", "Plan", "Vencimiento", "Importe", "Estado"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, true, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable_detalle.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable_detalleMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(jTable_detalle);
        if (jTable_detalle.getColumnModel().getColumnCount() > 0) {
            jTable_detalle.getColumnModel().getColumn(0).setResizable(false);
            jTable_detalle.getColumnModel().getColumn(0).setPreferredWidth(30);
            jTable_detalle.getColumnModel().getColumn(1).setResizable(false);
            jTable_detalle.getColumnModel().getColumn(2).setResizable(false);
            jTable_detalle.getColumnModel().getColumn(2).setPreferredWidth(300);
            jTable_detalle.getColumnModel().getColumn(4).setResizable(false);
            jTable_detalle.getColumnModel().getColumn(5).setResizable(false);
            jTable_detalle.getColumnModel().getColumn(6).setResizable(false);
        }

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 989, Short.MAX_VALUE)
                    .addComponent(jTextField_beneficiario))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTextField_beneficiario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 350, Short.MAX_VALUE)
                .addGap(12, 12, 12))
        );

        jTabbedPane1.addTab("Información General", jPanel4);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowActivated(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowActivated
//        Conexion.Verificar_conexion();
    }//GEN-LAST:event_formWindowActivated

    private void formKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyPressed


    }//GEN-LAST:event_formKeyPressed

    public void jDialog_beneficiario() {
        jDialog_beneficiario.setVisible(true);
        jDialog_beneficiario.setTitle("Buscar");
        jDialog_beneficiario.setSize(472, 593);
        jDialog_beneficiario.setLocationRelativeTo(null);
    }

    public void jDialog_pagar() {
        jDialog_pagar.setVisible(true);
        jDialog_pagar.setTitle("Buscar");
        jDialog_pagar.setSize((200), (200));
        jDialog_pagar.setLocationRelativeTo(null);
    }

    private void jTextField_beneficiarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField_beneficiarioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField_beneficiarioActionPerformed

    private void jTextField_beneficiarioKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField_beneficiarioKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            jDialog_beneficiario();
        }
    }//GEN-LAST:event_jTextField_beneficiarioKeyPressed


    private void jTextField_beneficiarioKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField_beneficiarioKeyReleased

    }//GEN-LAST:event_jTextField_beneficiarioKeyReleased

    private void jTextField_facturaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField_facturaKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField_facturaKeyPressed

    private void jTextField_facturaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField_facturaKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField_facturaKeyReleased

    private void jTextField_buscar_beneficiarioKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField_buscar_beneficiarioKeyReleased

    }//GEN-LAST:event_jTextField_buscar_beneficiarioKeyReleased

    private void jTextField_buscar_beneficiarioKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField_buscar_beneficiarioKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

            Code.Mensualidad_beneficiario_jtable(jTextField_buscar_beneficiario.getText());
        }
        if ((evt.getKeyCode() == KeyEvent.VK_DOWN)) {
            try {
                jTable_beneficiario.requestFocus();
                Robot r = new Robot();
                r.keyPress(KeyEvent.VK_DOWN);
            } catch (AWTException ex) {
                System.err.println(ex);
            }
        }
    }//GEN-LAST:event_jTextField_buscar_beneficiarioKeyPressed

    private void jTable_beneficiarioKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable_beneficiarioKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            Code.Mensualidad_beneficiario_selected();
            jDialog_beneficiario.setVisible(false);
            Code.Mensualidad_detalle_jtable();
        }
    }//GEN-LAST:event_jTable_beneficiarioKeyPressed

    private void jTable_beneficiarioMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable_beneficiarioMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_jTable_beneficiarioMouseEntered

    private void jTable_beneficiarioMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable_beneficiarioMouseClicked

    }//GEN-LAST:event_jTable_beneficiarioMouseClicked

    private void jTextField_facturaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField_facturaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField_facturaActionPerformed

    private void jTable_detalleMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable_detalleMouseClicked
        Code.Mensualidad_selected();
        jDialog_pagar();
    }//GEN-LAST:event_jTable_detalleMouseClicked

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        jDialog_pagar.setVisible(false);
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

        if (jTextField_factura.getText().length() > 1) {
            Code.Mensualidad_imprimir(jTextField_factura.getText());
            jDialog_pagar.setVisible(false);
            Code.Mensualidad_detalle_jtable();
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    public static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Pago_mensualidad.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        java.awt.EventQueue.invokeLater(() -> {
            new Pago_mensualidad().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    public static javax.swing.JCheckBox jCheckBox_carnet;
    private javax.swing.JDialog jDialog_beneficiario;
    private javax.swing.JDialog jDialog_pagar;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JTabbedPane jTabbedPane1;
    public static javax.swing.JTable jTable_beneficiario;
    public static javax.swing.JTable jTable_detalle;
    public static javax.swing.JTextField jTextField_beneficiario;
    private javax.swing.JTextField jTextField_buscar_beneficiario;
    private javax.swing.JTextField jTextField_factura;
    // End of variables declaration//GEN-END:variables
}
