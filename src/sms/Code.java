package sms;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

public class Code {

    public static void MostrarError(Object error, String metodo) {
        JOptionPane.showMessageDialog(null, "Se ha detectado un error con los datos ingresados." + error);
        //  sendEmail(String.valueOf(error + " Ref: " + metodo));
    }

    public static String getNombreMetodo() {
        return new Exception().getStackTrace()[1].getMethodName();
    }

    public static Connection conexion = null;
    public static int llamar_de = 0;
    public static int beneficiarios_id_sexo = 0;
    public static int id_mensualidad = 0;
    public static int contrato_id_titular = 0;
    public static int mensualidad_id_beneficiario = 0;
    public static int producto_id_descuento = 0;
    public static int producto_id_frecuencia = 0;
    public static int aporte_solidario_buscar_id = 0;
    public static int beneficiarios_id_sub_beneficiario = 0;
    public static int productos_id_plan = 0;
    public static int fs_compra_id_proveedor = 0;
    public static int id_vencimiento = 0;
    public static int productos_id_tipo = 0;
    public static int vencimiento_id_producto = 0;
    public static int id_fs_productos_vencimiento = 0;
    public static int fs_facturacion_detalle_id = 0;
    public static int farmacia_social_ventas_id_beneficiario = 0;
    public static int id_detalle_acceso = 0;
    public static int facturacion_id_seguro = 0;

    public static int seguro_id_beneficiario = 0;
    public static int seguro_id_plan = 0;
    public static int configuraciones_id_caja_usuario = 0;
    public static int configuraciones_id_usuario = 0;
    public static int id_caja_usuario = 0;
    public static int id_subbeneficiario = 0;
    public static int facturacion_id_producto = 0;
    public static int farmacia_social_producto_id_producto = 0;
    public static String usuario_activo = "";
    public static String institucion = "";
    public static boolean error = false;
    public static String institucion_ruc = "";
    public static String institucion_direccion = "";
    public static String facturacion_tipo = "";
    public static String credito = "";
    public static String contado = "";
    public static String institucion_telefono = "";
    public static int producto_id_tipo = 0;
    public static int id_barrio = 0;
    public static int id_auditoria = 0;
    public static int id_facturacion_detalle = 0;
    public static int id_usuario = 0;
    public static int id_fs_facturacion = 0;
    public static int servicios_utilizados = 0;
    public static int porcentaje = 0;
    public static int cantidad = 0;
    public static int frecuencia = 0;
    public static int id_beneficiario = 0;
    public static int contrato_id_beneficiario = 0;
    public static int beneficiarios_id_grupo_sanguineo = 0;
    public static int beneficiarios_id_barrio = 0;
    public static int acceso_id_usuario = 0;
    public static int acceso_id_menu = 0;
    public static int beneficiarios_id_menu = 0;
    public static int id_farmacia_social_producto = 0;
    public static int id_producto = 0;
    public static int vencimiento_id_estado = 0;
    public static int id_caja = 1;
    public static int id_fs_facturacion_detalle = 0;
    public static int fs_facturacion_detalle_id_producto = 0;
    public static int fs_facturacion_id_producto = 0;
    public static int configuraciones_id_caja = 0;
    public static int id_acceso = 0;
    public static int id_seguro_solidario = 0;
    public static int id_facturacion = 0;
    public static int fs_facturacion_id_beneficiario = 0;
    public static int facturacion_id_beneficiario = 0;
    public static int beneficiarios_id_usuario = 0;
    public static long facturacion_total_iva5 = 0;
    public static long facturacion_total_iva10 = 0;

    public static String ubicacion_proyecto = new File("").getAbsolutePath();

    public static void Configuraciones_caja_usuario_delete() {
        try {
            Statement st = conexion.createStatement();
            st.executeUpdate("DELETE FROM caja_usuario WHERE id_caja_usuario = '" + configuraciones_id_caja_usuario + "'");
        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }
    }

    public static void Productos_descuento_borrar() {
        try {
            Statement st = conexion.createStatement();
            st.executeUpdate("DELETE FROM descuento WHERE id_descuento = '" + producto_id_descuento + "'");
            Code.Productos_descuento_jtable();
        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }
    }

    public static void Fs_Imprimir_stock() {
        try {

            Statement stAuxiliar = conexion.createStatement();
            stAuxiliar.executeUpdate("truncate table fs_stock");

            int id = 0;
            int id_producto = 0;
            int compras = 0;
            int ventas = 0;
            String nombre = "";
            Statement st = conexion.createStatement();
            ResultSet rs = st.executeQuery(""
                    + "SELECT * FROM farmacia_social_producto "
                    + "order by descripcion ASC ");
            while (rs.next()) {

                compras = 0;
                ventas = 0;

                id_producto = rs.getInt("id_farmacia_social_producto");
                nombre = rs.getString("descripcion");

                Statement st2 = conexion.createStatement();
                ResultSet rs2 = st2.executeQuery("SELECT SUM(unidades) FROM fs_compra_detalle where id_farmacia_social_producto = '" + id_producto + "' ");
                if (rs2.next()) {
                    compras = rs2.getInt(1);
                }
                st2 = conexion.createStatement();
                rs2 = st2.executeQuery("SELECT SUM(unidades) FROM fs_facturacion_detalle where id_farmacia_social_producto = '" + id_producto + "' ");
                if (rs2.next()) {
                    ventas = rs2.getInt(1);
                }

                id = id + 1;
                PreparedStatement stReporteCaja = conexion.prepareStatement("INSERT INTO fs_stock VALUES(?,?,?,?,?)");
                stReporteCaja.setInt(1, id);
                stReporteCaja.setString(2, nombre);
                stReporteCaja.setInt(3, compras);
                stReporteCaja.setInt(4, ventas);
                stReporteCaja.setInt(5, compras - ventas);
                stReporteCaja.executeUpdate();
            }

            Map parametros = new HashMap();
            JasperReport jr = (JasperReport) JRLoader.loadObjectFromFile(ubicacion_proyecto + "\\reportes\\fs_stock.jasper");
            JasperPrint jp = JasperFillManager.fillReport(jr, parametros, conexion);
            JasperViewer jv = new JasperViewer(jp, false);
            jv.setVisible(true);

        } catch (JRException | SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }
    }

    public static void Contrato_agregar_beneficiario() {
        try {

            PreparedStatement stReporteCaja = conexion.prepareStatement("INSERT INTO seguro_beneficiario VALUES(?,?,?)");
            stReporteCaja.setInt(1, MAX_tabla("id_seguro_beneficiario", "seguro_beneficiario"));
            stReporteCaja.setInt(2, id_seguro_solidario);
            stReporteCaja.setInt(3, contrato_id_beneficiario);
            stReporteCaja.executeUpdate();

        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }
    }

    public static void Mensualidad_imprimir(String factura) {
        try {

            Date hoy = new Date();
            int importe = 0;
            Statement st2 = conexion.createStatement();
            ResultSet rs2 = st2.executeQuery("SELECT * FROM mensualidad where id_mensualidad = '" + id_mensualidad + "' ");
            if (rs2.next()) {
                importe = rs2.getInt("importe");
            }

//            if (Pago_mensualidad.jCheckBox_carnet.isSelected() == true) {
//                importe = importe + 10000;
//            }
            PreparedStatement stUpdateAuxiliar4 = conexion.prepareStatement(""
                    + "UPDATE mensualidad "
                    + "SET id_mensualidad_estado = '2',"
                    + "factura = '" + Integer.valueOf(factura) + "', "
                    + "importe = '" + importe + "', "
                    + "descripcion = ' y carnet', "
                    + "fecha_pago = '" + util_Date_to_sql_date(hoy) + "' "
                    + "WHERE id_mensualidad = '" + id_mensualidad + "'");
            stUpdateAuxiliar4.executeUpdate();

            contado = "X";
            credito = "";

            String total_letras = Numero_a_String(importe);

            Map parametros = new HashMap();

            importe = importe / 11;

            parametros.put("contado", contado);
            parametros.put("id_mensualidad", id_mensualidad);
            parametros.put("total_letras", total_letras);
            parametros.put("total_iva5", 0);
            parametros.put("total_iva10", importe);
            JasperReport jr = (JasperReport) JRLoader.loadObjectFromFile(ubicacion_proyecto + "\\reportes\\factura_mensualidad.jasper");
            JasperPrint jp = JasperFillManager.fillReport(jr, parametros, conexion);
            JasperViewer jv = new JasperViewer(jp, false);
            jv.setVisible(true);

        } catch (JRException | SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Code.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void Seguro_generar_chequera() {
        try {
            if (id_seguro_solidario > 0) {

                Date fecha_resgitro = null;
                int id_plan = 0;
                int monto = 0;

                Statement st2 = conexion.createStatement();
                ResultSet rs2 = st2.executeQuery(""
                        + "SELECT * FROM seguro_solidario "
                        + "inner join plan on plan.id_plan = seguro_solidario.id_plan "
                        + "where id_seguro = '" + id_seguro_solidario + "' ");
                if (rs2.next()) {
                    fecha_resgitro = rs2.getDate("fecha_registro");
                    id_plan = rs2.getInt("id_plan");
                    monto = rs2.getInt("monto");
                }

                int i = 1;
                while (i <= 12) {

                    PreparedStatement stReporteCaja = conexion.prepareStatement("INSERT INTO mensualidad VALUES(?,?,?,?,?,?,?,?,?)");
                    stReporteCaja.setInt(1, MAX_tabla("id_mensualidad", "mensualidad"));
                    stReporteCaja.setInt(2, i);
                    stReporteCaja.setDate(3, null);
                    stReporteCaja.setDate(4, util_Date_to_sql_date(fecha_resgitro));
                    stReporteCaja.setInt(5, id_plan);
                    stReporteCaja.setInt(6, 1);
                    stReporteCaja.setInt(7, id_seguro_solidario);
                    stReporteCaja.setInt(8, monto);
                    stReporteCaja.setInt(9, 0);
                    stReporteCaja.executeUpdate();
                    i = i + 1;

                    Calendar cal = Calendar.getInstance();
                    cal.setTime(fecha_resgitro);
                    cal.add(Calendar.MONTH, 1);
                    fecha_resgitro = cal.getTime();

                }
                JOptionPane.showMessageDialog(null, "Datos generados correctamente.");
            }
        } catch (SQLException ex) {
            System.err.println(ex);
        }
    }

    public static int MAX_tabla(String id, String tabla) {
        int dato = 0;
        try {
            Statement st = conexion.createStatement();
            ResultSet rs = st.executeQuery("SELECT MAX(" + id + ") FROM " + tabla + " ");
            while (rs.next()) {
                dato = rs.getInt(1) + 1;
            }

        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }
        return dato;
    }

    public static void Lote_vencimiento(String lote, Date vencimiento) {
        try {
            if (lote.length() > 0) {
                PreparedStatement st = conexion.prepareStatement("INSERT INTO lote VALUES(?,?,?,?)");
                st.setInt(1, MAX_tabla("id_lote", "lote"));
                st.setString(2, lote);
                st.setDate(3, util_Date_to_sql_date(vencimiento));
//                st.setInt(4, Farmacia_social_compras.fs_compra_id_producto);
                st.executeUpdate();
            }
        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }
    }

    public static void Farmacia_social_totales() {
        try {

            Verificar_conexion();

            long total = 0;
            long sub_total_exentas = 0;
            long sub_total_5 = 0;
            long sub_total_10 = 0;
            long iva5 = 0;
            long iva10 = 0;

            Statement st = conexion.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM fs_facturacion_detalle where id_fs_facturacion = '" + id_fs_facturacion + "' ");
            while (rs.next()) {
                sub_total_exentas = rs.getLong("exentas") + sub_total_exentas;
                sub_total_5 = rs.getLong("iva5") + sub_total_5;
                sub_total_10 = rs.getLong("iva10") + sub_total_10;
            }

            total = sub_total_exentas + sub_total_5 + sub_total_10 + total;

//            Farmacia_social_ventas.jTextField_total.setText(String.valueOf(total));
//            Farmacia_social_ventas.jTextField_total_exentas.setText(String.valueOf(sub_total_exentas));
//            Farmacia_social_ventas.jTextField_total_10.setText(String.valueOf(sub_total_10));
//            Farmacia_social_ventas.jTextField_total_5.setText(String.valueOf(sub_total_5));
//            Farmacia_social_ventas.jTextField_iva_10.setText(String.valueOf(sub_total_10 / 11));
//            Farmacia_social_ventas.jTextField_iva_5.setText(String.valueOf(sub_total_5 / 21));
        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }
    }
//    public static int ObtenerIVAdelPRODUCTO(int id_producto) {
//        try {
//
//            Statement st = conexion.createStatement();
//            ResultSet rs = st.executeQuery(""
//                    + "SELECT * FROM farmacia_social_producto "
//                    + "where id_farmacia_social_producto = '" + id_producto + "' ");
//            while (rs.next()) {
//                
//            }
//
//        } catch (SQLException ex) {
//            MostrarError(ex, getNombreMetodo());
//        }
//    }

    public static void Facturacion_contrato_buscar(String contrato) {
        try {

            if (contrato.length() < 2) {
                contrato = "00" + contrato;
            }
            if (contrato.length() < 3) {
                contrato = "0" + contrato;
            }
            boolean encontrado = false;

            facturacion_id_beneficiario = 0;
            facturacion_id_seguro = 0;
//            Facturacion_coronel_bogado.jt_beneficiario.setText("NO ESPECIFICADO");
//            Facturacion_coronel_bogado.jTextField_seguro.setText("NO");
//            Facturacion_coronel_bogado.jt_contrato.setText("");

            Statement st = conexion.createStatement();
            ResultSet rs = st.executeQuery(""
                    + "SELECT * "
                    + "FROM seguro_solidario "
                    + "inner join beneficiario on beneficiario.id_beneficiario = seguro_solidario.id_beneficiario "
                    + "where numero = '" + contrato + "' ");
            if (rs.next()) {
                encontrado = true;
                facturacion_id_beneficiario = rs.getInt("id_beneficiario");
                facturacion_id_seguro = 1;
//                Facturacion_coronel_bogado.jt_beneficiario.setText(rs.getString("nombre"));
//                Facturacion_coronel_bogado.jt_cedula.setText(rs.getString("documento"));
//                Facturacion_coronel_bogado.jTextField_seguro.setText("SI");
//                Facturacion_coronel_bogado.jt_contrato.setText(contrato);
            }

            if (encontrado == false) {
                JOptionPane.showMessageDialog(null, "Contrato no registrado. Consulte con la administración.");
            }

//            Facturacion_update(Facturacion_coronel_bogado.jDateChooser3.getDate());
        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }
    }

    static void Farmacia_social_ventas_nuevo() {
        try {
            Statement st = conexion.createStatement();
            ResultSet rs = st.executeQuery("SELECT MAX(id_fs_facturacion) FROM fs_facturacion ");
            if (rs.next()) {
                id_fs_facturacion = rs.getInt(1) + 1;
            }
            int factura = 0;
            st = conexion.createStatement();
            rs = st.executeQuery("SELECT * "
                    + "FROM caja_usuario "
                    + "inner join caja on caja_usuario.id_caja = caja.id_caja "
                    + "where caja_usuario.id_usuario = '" + id_usuario + "'");
            if (rs.next()) {
                factura = rs.getInt("factura") + 1;
                id_caja = rs.getInt("id_caja");
            }

            hoy = new Date();

            PreparedStatement stReporteCaja = conexion.prepareStatement("INSERT INTO fs_facturacion VALUES(?,?,?,?,?,?,?,?,?)");
            stReporteCaja.setInt(1, id_fs_facturacion);
            stReporteCaja.setInt(2, 0);
            stReporteCaja.setDate(3, util_Date_to_sql_date(hoy));
            stReporteCaja.setInt(4, 0);
            stReporteCaja.setInt(5, 0);
            stReporteCaja.setInt(6, 1);
            stReporteCaja.setInt(7, factura);
            stReporteCaja.setInt(8, 0);
            stReporteCaja.setInt(9, 0);
            stReporteCaja.executeUpdate();

            PreparedStatement stUpdateAuxiliar4 = conexion.prepareStatement(""
                    + "UPDATE caja "
                    + "SET factura = '" + factura + "' "
                    + "WHERE id_caja = '" + id_caja + "'");
            stUpdateAuxiliar4.executeUpdate();

            Code.Auditoria_guardar("GENERADO", "F.S. FACTURA", "Número: " + factura);

        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }
    }

    static void Facturacion_update_tipo() {
        try {

            // verificamos 
            int facturacion_id_tipo = 0;
            Statement st = conexion.createStatement();
            ResultSet rs = st.executeQuery("SELECT id_tipo FROM facturacion where id_facturacion = '" + id_facturacion + "' ");
            if (rs.next()) {
                facturacion_id_tipo = rs.getInt(1);
            }
            if (facturacion_id_tipo == 1) {
                PreparedStatement stUpdateAuxiliar4 = conexion.prepareStatement(""
                        + "UPDATE facturacion "
                        + "SET id_tipo = '2' "
                        + "WHERE id_facturacion = '" + id_facturacion + "'");
                stUpdateAuxiliar4.executeUpdate();
//                Facturacion_coronel_bogado.tipo.setText("CREDITO");
            } else {
                PreparedStatement stUpdateAuxiliar4 = conexion.prepareStatement(""
                        + "UPDATE facturacion "
                        + "SET id_tipo = '1' "
                        + "WHERE id_facturacion = '" + id_facturacion + "'");
                stUpdateAuxiliar4.executeUpdate();
//                Facturacion_coronel_bogado.tipo.setText("CONTADO");

            }

        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }
    }

    public static void Vencimientos_guardar(String lote, Date vencimiento) {
        try {
            Statement st = conexion.createStatement();
            ResultSet rs = st.executeQuery("SELECT MAX(id_fs_productos_vencimiento) FROM fs_productos_vencimiento ");
            if (rs.next()) {
                id_fs_productos_vencimiento = rs.getInt(1) + 1;
            }

            PreparedStatement stReporteCaja = conexion.prepareStatement("INSERT INTO fs_productos_vencimiento VALUES(?,?,?,?,?)");
            stReporteCaja.setInt(1, id_fs_productos_vencimiento);
            stReporteCaja.setInt(2, vencimiento_id_producto);
            stReporteCaja.setString(3, lote);
            stReporteCaja.setDate(4, util_Date_to_sql_date(hoy));
            stReporteCaja.setInt(5, vencimiento_id_estado);

            stReporteCaja.executeUpdate();

            Code.Auditoria_guardar("GUARDAR", "F.S. VENCIMIENTO", "ID PRODUCTO: " + vencimiento_id_producto);

            JOptionPane.showMessageDialog(null, "Agregado correctamente.");

        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }
    }

    public static void Seguro_pago_guardar(String monto, Date fecha) {
        try {
            if (id_seguro_solidario > 0) {
                PreparedStatement stReporteCaja = conexion.prepareStatement("INSERT INTO seguro_pagos VALUES(?,?,?,?)");
                stReporteCaja.setInt(1, MAX_tabla("id_pago", "seguro_pagos"));
                stReporteCaja.setInt(2, id_seguro_solidario);
                stReporteCaja.setDate(3, util_Date_to_sql_date(fecha));
                stReporteCaja.setLong(4, Long.valueOf(monto));
                stReporteCaja.executeUpdate();
//                Metodos.Auditoria_guardar("GUARDAR", "F.S. VENCIMIENTO", "ID PRODUCTO: " + vencimiento_id_producto);
                JOptionPane.showMessageDialog(null, "Agregado correctamente.");
            } else {
                JOptionPane.showMessageDialog(null, "Selecciona un contrato antes de continuar");
            }
        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }
    }

    public static void Productos_descuentos_guardar(String cantidad, String porcentaje) {
        try {
            int id_descuento = 0;
            Statement st = conexion.createStatement();
            ResultSet rs = st.executeQuery("SELECT MAX(id_descuento) FROM descuento ");
            if (rs.next()) {
                id_descuento = rs.getInt(1) + 1;
            }

            PreparedStatement stReporteCaja = conexion.prepareStatement("INSERT INTO descuento VALUES(?,?,?,?,?,?)");
            stReporteCaja.setInt(1, id_descuento);
            stReporteCaja.setInt(2, Integer.parseInt(porcentaje));
            stReporteCaja.setInt(3, id_producto);
            stReporteCaja.setInt(4, productos_id_plan);
            stReporteCaja.setInt(5, Integer.parseInt(cantidad));
            stReporteCaja.setInt(6, producto_id_frecuencia);

            stReporteCaja.executeUpdate();

        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }
    }

    public static void Acceso_borrar() {
        try {
            Statement st = conexion.createStatement();
            st.executeUpdate("DELETE FROM acceso WHERE id_acceso = '" + id_acceso + "'");

        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }
    }

    public static void Farmacia_social_facturacion_detalle_borrar() {
        try {
            Statement st = conexion.createStatement();
            st.executeUpdate("DELETE FROM fs_facturacion_detalle WHERE id_fs_facturacion_detalle = '" + fs_facturacion_detalle_id + "'");

        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }
    }

    public static void Verificar_conexion() {
        try {
            if (conexion.isClosed() == false) {
                //     System.err.println("conexion OK");
            } else {
                JOptionPane.showMessageDialog(null, "Error de conexion a la Base de Datos. Reiciando conexión...");
                Iniciar_Conexion();
                if (conexion.isClosed() == false) {
                    System.err.println("conexion reiniciada; Conexion OK");
                } else {
                    JOptionPane.showMessageDialog(null, "Error de conexion con la base de datos.");
                }
            }
        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }
    }

    public static void Beneficiario_delete() {
        try {
            Statement st = conexion.createStatement();
            st.executeUpdate("DELETE FROM beneficiario WHERE id_beneficiario = '" + id_beneficiario + "'");
            JOptionPane.showMessageDialog(null, "Borrado correctamente.");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "ERROR: No se puede borrar. (Beneficiario en uso)");
        }
    }

    public static void Arqueo_imprimir(Date fecha) {
        try {
            Statement st = conexion.createStatement();
            st.executeUpdate("delete from arqueo_imprimir");
            long total_credito = 0;
            long total_contado = 0;
            st = conexion.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM facturacion "
                    + "inner join beneficiario on beneficiario.id_beneficiario = facturacion.id_beneficiario "
                    + "where fecha = '" + fecha + "' "
                    + "and total > '0'  ");
            while (rs.next()) {

                PreparedStatement stReporteCaja = conexion.prepareStatement("INSERT INTO arqueo_imprimir VALUES(?,?,?,?,?,?)");
                stReporteCaja.setInt(1, MAX_tabla("id", "arqueo_imprimir"));
                stReporteCaja.setDate(2, util_Date_to_sql_date(rs.getDate("fecha")));
                stReporteCaja.setString(3, rs.getString("factura"));
                stReporteCaja.setString(4, rs.getString("nombre").trim().toUpperCase());
                stReporteCaja.setString(5, "CREDITO");
                stReporteCaja.setLong(6, rs.getLong("total"));

                stReporteCaja.executeUpdate();

                total_credito = rs.getLong("total") + total_credito;

            }
            st = conexion.createStatement();
            rs = st.executeQuery("SELECT * FROM mensualidad "
                    + "inner join seguro_solidario on seguro_solidario.id_seguro = mensualidad.id_seguro "
                    + "inner join beneficiario on beneficiario.id_beneficiario = seguro_solidario.id_beneficiario "
                    + "where fecha_pago = '" + fecha + "' "
                    + "");
            while (rs.next()) {

                PreparedStatement stReporteCaja = conexion.prepareStatement("INSERT INTO arqueo_imprimir VALUES(?,?,?,?,?,?)");
                stReporteCaja.setInt(1, MAX_tabla("id", "arqueo_imprimir"));
                stReporteCaja.setDate(2, util_Date_to_sql_date(rs.getDate("fecha_pago")));
                stReporteCaja.setString(3, rs.getString("factura"));
                stReporteCaja.setString(4, rs.getString("nombre").trim().toUpperCase() + " - Cuota " + rs.getString("cuota") + "/12 ");
                stReporteCaja.setString(5, "CONTADO");
                stReporteCaja.setLong(6, rs.getLong("importe"));

                stReporteCaja.executeUpdate();

                total_contado = rs.getLong("importe") + total_contado;

            }

            Map parametros = new HashMap();
            parametros.put("credito", total_credito);
            parametros.put("contado", total_contado);
            parametros.put("fecha", fecha);
            parametros.put("titulo", "Hospital Pediátrico Municipal de Encarnación");
            JasperReport jr = (JasperReport) JRLoader.loadObjectFromFile(ubicacion_proyecto + "\\reportes\\arqueo_caja.jasper");
            JasperPrint jp = JasperFillManager.fillReport(jr, parametros, conexion);
            JasperViewer jv = new JasperViewer(jp, false);
            jv.setVisible(true);
        } catch (JRException ex) {
            MostrarError(ex, getNombreMetodo());
        } catch (SQLException ex) {
            Logger.getLogger(Code.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void Factura_imprimir() {
        try {
            Map parametros = new HashMap();
            long tot = Facturacion_total();
            String total_letras = Numero_a_String(tot);

            parametros.put("contado", contado);
            parametros.put("credito", credito);
            parametros.put("id_facturacion", id_facturacion);
            parametros.put("total_letras", total_letras);
            parametros.put("total_iva5", facturacion_total_iva5);
            parametros.put("total_iva10", facturacion_total_iva10);
            parametros.put("direccion", Code.ubicacion_proyecto + "\\reportes\\");
//            JasperReport jr = (JasperReport) JRLoader.loadObjectFromFile(ubicacion_proyecto + "\\reportes\\factura.jasper");
            JasperReport jr = (JasperReport) JRLoader.loadObjectFromFile(ubicacion_proyecto + "\\reportes\\factura_coronel_bogado.jasper");
            JasperPrint jp = JasperFillManager.fillReport(jr, parametros, conexion);
            JasperViewer jv = new JasperViewer(jp, false);
            jv.setVisible(true);
        } catch (JRException | ClassNotFoundException | SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }
    }

    public static long Facturacion_total() {
        long total = 0;

        facturacion_total_iva10 = 0;
        facturacion_total_iva5 = 0;
        try {
            Statement st3 = conexion.createStatement();
            ResultSet rs3 = st3.executeQuery(""
                    + "select * from facturacion where id_facturacion = '" + id_facturacion + "' "
                    + "");
            while (rs3.next()) {
                total = rs3.getLong("total");
                if (rs3.getInt("id_tipo") == 1) {
                    contado = "X";
                    credito = "";
                } else {
                    credito = "X";
                    contado = "";
                }
            }
            return total;
        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }
        return total;

    }

    public static void FS_listado_compras_imprimir(Date desde, Date hasta) {
        try {
            Map parametros = new HashMap();
            parametros.put("desde", desde);
            parametros.put("hasta", hasta);
            JasperReport jr = (JasperReport) JRLoader.loadObjectFromFile(ubicacion_proyecto + "\\reportes\\fs_listado_compras.jasper");
            JasperPrint jp = JasperFillManager.fillReport(jr, parametros, conexion);
            JasperViewer jv = new JasperViewer(jp, false);
            jv.setVisible(true);
        } catch (JRException ex) {
            MostrarError(ex, getNombreMetodo());
        }
    }

    public static void FS_listado_ventas_imprimir(Date desde, Date hasta) {
        try {
            Map parametros = new HashMap();
            parametros.put("desde", desde);
            parametros.put("hasta", hasta);
            JasperReport jr = (JasperReport) JRLoader.loadObjectFromFile(ubicacion_proyecto + "\\reportes\\fs_listado_ventas.jasper");
            JasperPrint jp = JasperFillManager.fillReport(jr, parametros, conexion);
            JasperViewer jv = new JasperViewer(jp, false);
            jv.setVisible(true);
        } catch (JRException ex) {
            MostrarError(ex, getNombreMetodo());
        }
    }

    public static void FS_arqueo_imprimir(Date fecha) {
        try {
            Map parametros = new HashMap();
            parametros.put("fecha", fecha);
            JasperReport jr = (JasperReport) JRLoader.loadObjectFromFile(ubicacion_proyecto + "\\reportes\\fs_arqueo.jasper");
            JasperPrint jp = JasperFillManager.fillReport(jr, parametros, conexion);
            JasperViewer jv = new JasperViewer(jp, false);
            jv.setVisible(true);
        } catch (JRException ex) {
            MostrarError(ex, getNombreMetodo());
        }
    }

    public static void Facturacion_listado_servicios_imprimir(Date desde, Date hasta) {
        try {
            long total_contado = 0;
            long total_credito = 0;

            Statement st3 = conexion.createStatement();
            ResultSet rs3 = st3.executeQuery(""
                    + "select * from facturacion "
                    + "where fecha >= '" + desde + "' "
                    + "and fecha <= '" + hasta + "' "
                    + "and factura > '0'"
                    + "and id_estado != '1' "
                    + "");
            while (rs3.next()) {
                if (rs3.getInt("id_tipo") == 1) {
                    total_contado = total_contado + rs3.getLong("total");
                } else {
                    total_credito = total_credito + rs3.getLong("total");
                }
            }

            Map parametros = new HashMap();
            parametros.put("desde", desde);
            parametros.put("hasta", hasta);
            parametros.put("total_contado", total_contado);
            parametros.put("total_credito", total_credito);
            parametros.put("institucion", institucion);
            parametros.put("institucion_ruc", institucion_ruc);
            parametros.put("institucion_telefono", institucion_telefono);
            parametros.put("institucion_direccion", institucion_direccion);
            JasperReport jr = (JasperReport) JRLoader.loadObjectFromFile(ubicacion_proyecto + "\\reportes\\listado_servicios_prestados.jasper");
            JasperPrint jp = JasperFillManager.fillReport(jr, parametros, conexion);
            JasperViewer jv = new JasperViewer(jp, false);
            jv.setVisible(true);
        } catch (JRException ex) {
            MostrarError(ex, getNombreMetodo());
        } catch (SQLException ex) {
            Logger.getLogger(Code.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void Facturacion_listado_servicios_detallado_imprimir(Date desde, Date hasta) {
        try {
            Statement stAuxiliar = conexion.createStatement();
            stAuxiliar.executeUpdate("truncate table listados");

            int id = 0;

            Statement st3 = conexion.createStatement();
            ResultSet rs3 = st3.executeQuery(""
                    + "select * from producto order by descripcion "
                    + "");
            while (rs3.next()) {
                int id_producto = rs3.getInt("id_producto");
                Statement st2 = conexion.createStatement();
                ResultSet rs2 = st2.executeQuery(""
                        + "select *, facturacion_detalle.precio as fdprecio from facturacion_detalle "
                        + "inner join facturacion on facturacion.id_facturacion = facturacion_detalle.id_facturacion "
                        + "inner join producto on producto.id_producto = facturacion_detalle.id_producto "
                        + "inner join beneficiario on beneficiario.id_beneficiario = facturacion.id_beneficiario "
                        + "where fecha >= '" + desde + "' and fecha <= '" + hasta + "' and facturacion_detalle.id_producto = '" + id_producto + "'"
                        + "and id_facturacion_seguro = '0' "
                        + "and factura > '0' "
                        + "order by factura");
                while (rs2.next()) {
                    id = id + 1;
                    PreparedStatement stReporteCaja = conexion.prepareStatement("INSERT INTO listados VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                    stReporteCaja.setInt(1, id);
                    stReporteCaja.setString(2, rs2.getString("descripcion"));
                    stReporteCaja.setString(3, rs2.getString("nombre"));
                    stReporteCaja.setString(4, rs2.getString("factura"));
                    stReporteCaja.setString(5, "");
                    stReporteCaja.setString(6, "");
                    stReporteCaja.setString(7, "");
                    stReporteCaja.setString(8, "");
                    stReporteCaja.setString(9, "");
                    stReporteCaja.setString(10, "");
                    stReporteCaja.setLong(11, rs2.getInt("unidades"));
                    stReporteCaja.setLong(12, rs2.getInt("fdprecio"));
                    stReporteCaja.setLong(13, id_producto);
                    stReporteCaja.setLong(14, rs2.getLong("unidades") * rs2.getLong("fdprecio"));
                    stReporteCaja.executeUpdate();

                }
            }

            Map parametros = new HashMap();
            parametros.put("desde", desde);
            parametros.put("hasta", hasta);
            parametros.put("institucion", institucion);
            parametros.put("institucion_ruc", institucion_ruc);
            parametros.put("institucion_telefono", institucion_telefono);
            parametros.put("institucion_direccion", institucion_direccion);
            JasperReport jr = (JasperReport) JRLoader.loadObjectFromFile(ubicacion_proyecto + "\\reportes\\listado_servicios_prestados_detallado2.jasper");
            JasperPrint jp = JasperFillManager.fillReport(jr, parametros, conexion);
            JasperViewer jv = new JasperViewer(jp, false);
            jv.setVisible(true);
        } catch (JRException ex) {
            MostrarError(ex, getNombreMetodo());
        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }

    }

    public static void Facturacion_listado_servicios_detallado_asegurado_imprimir(Date desde, Date hasta) {

        try {

            Statement stAuxiliar = conexion.createStatement();
            stAuxiliar.executeUpdate("truncate table listados");

            int id = 0;

            Statement st3 = conexion.createStatement();
            ResultSet rs3 = st3.executeQuery(""
                    + "select * from producto order by descripcion "
                    + "");
            while (rs3.next()) {
                int id_producto = rs3.getInt("id_producto");
                int id_listado = 0;
                long precio = 0;
                Statement st2 = conexion.createStatement();
                ResultSet rs2 = st2.executeQuery(""
                        + "select * from facturacion_detalle "
                        + "inner join facturacion on facturacion.id_facturacion = facturacion_detalle.id_facturacion "
                        + "where fecha >= '" + desde + "' and fecha <= '" + hasta + "' and id_producto = '" + id_producto + "'"
                        + "and id_estado != '1' "
                        + "order by precio ASC ");
                while (rs2.next()) {

                    precio = rs2.getLong("precio");
                    int unidades = 1;
                    boolean encontro = false;
                    Statement st4 = conexion.createStatement();
                    ResultSet rs4 = st4.executeQuery(""
                            + "select * from listados "
                            + "where i3 = '" + id_producto + "' and i2 = '" + precio + "' "
                            + "");
                    while (rs4.next()) {
                        encontro = true;
                        unidades = rs4.getInt(11);
                        id_listado = rs4.getInt(1);
                    }
                    id = id + 1;

                    if (encontro == false) {

                        PreparedStatement stReporteCaja = conexion.prepareStatement("INSERT INTO listados VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                        stReporteCaja.setInt(1, id);
                        stReporteCaja.setString(2, rs3.getString("descripcion"));
                        stReporteCaja.setString(3, "");
                        stReporteCaja.setString(4, "");
                        stReporteCaja.setString(5, "");
                        stReporteCaja.setString(6, "");
                        stReporteCaja.setString(7, "");
                        stReporteCaja.setString(8, "");
                        stReporteCaja.setString(9, "");
                        stReporteCaja.setString(10, "");
                        stReporteCaja.setLong(11, unidades);
                        stReporteCaja.setLong(12, precio);
                        stReporteCaja.setLong(13, id_producto);
                        stReporteCaja.setLong(14, unidades * precio);
                        stReporteCaja.executeUpdate();

                    } else {
                        PreparedStatement stUpdateAuxiliar4 = conexion.prepareStatement(""
                                + "UPDATE listados "
                                + "SET i1 = '" + (unidades + 1) + "', "
                                + "i4 = '" + ((unidades + 1) * precio) + "' "
                                + "WHERE id = '" + id_listado + "'");
                        stUpdateAuxiliar4.executeUpdate();
                    }
                }
            }

            Map parametros = new HashMap();
            parametros.put("desde", desde);
            parametros.put("hasta", hasta);
//            parametros.put("titulo", "Hospital Pediátrico Municipal de Encarnación");
            parametros.put("institucion", institucion);
            parametros.put("institucion_ruc", institucion_ruc);
            parametros.put("institucion_telefono", institucion_telefono);
            parametros.put("institucion_direccion", institucion_direccion);
            JasperReport jr = (JasperReport) JRLoader.loadObjectFromFile(ubicacion_proyecto + "\\reportes\\listado_servicios_prestados_detallado.jasper");
            JasperPrint jp = JasperFillManager.fillReport(jr, parametros, conexion);
            JasperViewer jv = new JasperViewer(jp, false);
            jv.setVisible(true);
        } catch (JRException ex) {
            MostrarError(ex, getNombreMetodo());
        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }

    }

    public static void Facturacion_listado_servicios_totales_imprimir(Date desde, Date hasta) {

        try {

            Statement stAuxiliar = conexion.createStatement();
            stAuxiliar.executeUpdate("truncate table listados");

            int id = 0;
            int unidades = 0;
            long total = 0;
            boolean producto_encontrado = false;
            Statement st5 = conexion.createStatement();
            ResultSet rs5 = st5.executeQuery("SELECT * from producto ");
            while (rs5.next()) {
                producto_encontrado = false;
                total = 0;
                unidades = 0;
                Statement st2 = conexion.createStatement();
                ResultSet rs2 = st2.executeQuery("select unidades, exentas, iva5, iva10 from facturacion_detalle "
                        + "inner join facturacion on facturacion.id_facturacion = facturacion_detalle.id_facturacion "
                        + "where fecha >= '" + desde + "' and fecha <= '" + hasta + "' and id_producto = '" + rs5.getInt("id_producto") + "' "
                        + "and id_facturacion_seguro = '0' ");
                while (rs2.next()) {
                    unidades = unidades + rs2.getInt("unidades");
                    total = total + rs2.getInt("exentas") + rs2.getInt("iva5") + rs2.getInt("iva10");
                    producto_encontrado = true;
                }

                if (producto_encontrado == true) {

                    Statement st4 = conexion.createStatement();
                    ResultSet rs4 = st4.executeQuery("SELECT MAX(id) FROM listados ");
                    if (rs4.next()) {
                        id = rs4.getInt(1) + 1;
                    }

                    PreparedStatement stReporteCaja = conexion.prepareStatement("INSERT INTO listados VALUES(?,?,?,?,?,?,?,?,?,?,?,?)");
                    stReporteCaja.setInt(1, id);
                    stReporteCaja.setString(2, rs5.getString("descripcion"));
                    stReporteCaja.setString(3, "");
                    stReporteCaja.setString(4, "");
                    stReporteCaja.setString(5, "");
                    stReporteCaja.setString(6, "");
                    stReporteCaja.setString(7, "");
                    stReporteCaja.setString(8, "");
                    stReporteCaja.setString(9, "");
                    stReporteCaja.setString(10, "");
                    stReporteCaja.setLong(11, unidades);
                    stReporteCaja.setLong(12, total);
                    stReporteCaja.executeUpdate();
                }
            }

            Map parametros = new HashMap();
            parametros.put("desde", desde);
            parametros.put("hasta", hasta);
            parametros.put("institucion", institucion);
            parametros.put("institucion_ruc", institucion_ruc);
            parametros.put("institucion_telefono", institucion_telefono);
            parametros.put("institucion_direccion", institucion_direccion);
            JasperReport jr = (JasperReport) JRLoader.loadObjectFromFile(ubicacion_proyecto + "\\reportes\\listado_servicios_prestados_totales.jasper");
            JasperPrint jp = JasperFillManager.fillReport(jr, parametros, conexion);
            JasperViewer jv = new JasperViewer(jp, false);
            jv.setVisible(true);
        } catch (JRException | SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }

    }

    public static void Facturacion_borrar(String factura) {

        try {
            Statement st = conexion.createStatement();
            st.executeUpdate("DELETE FROM facturacion_detalle WHERE id_facturacion = '" + factura + "'");
            st.executeUpdate("DELETE FROM facturacion WHERE id_facturacion = '" + factura + "'");
            JOptionPane.showMessageDialog(null, "Borrado correctamente.");
        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }

    }

    public synchronized static String getSepararMiles(String txtprec) {
        String valor = txtprec;

        int largo = valor.length();
        if (largo > 9) {
            valor = valor.substring(largo - 12, largo - 9) + "." + valor.substring(largo - 9, largo - 6) + "." + valor.substring(largo - 6, largo - 3) + "." + valor.substring(largo - 3, largo);
        }
        if (largo > 8) {
            valor = valor.substring(largo - 9, largo - 6) + "." + valor.substring(largo - 6, largo - 3) + "." + valor.substring(largo - 3, largo);
        } else if (largo > 7) {
            valor = valor.substring(largo - 8, largo - 6) + "." + valor.substring(largo - 6, largo - 3) + "." + valor.substring(largo - 3, largo);
        } else if (largo > 6) {
            valor = valor.substring(largo - 7, largo - 6) + "." + valor.substring(largo - 6, largo - 3) + "." + valor.substring(largo - 3, largo);
        } else if (largo > 5) {
            valor = valor.substring(largo - 6, largo - 3) + "." + valor.substring(largo - 3, largo);
        } else if (largo > 4) {
            valor = valor.substring(largo - 5, largo - 3) + "." + valor.substring(largo - 3, largo);
        } else if (largo > 3) {
            valor = valor.substring(largo - 4, largo - 3) + "." + valor.substring(largo - 3, largo);
        }
        txtprec = valor;
        return valor;
    }

    public static void Facturacion_buscar_servicio(String servicio) {
        try {
            error = false;
            if (servicio.length() > 0) {
                if (isNumeric(servicio) == true) {
                    Statement st = conexion.createStatement();
                    ResultSet rs = st.executeQuery("SELECT * FROM producto where id_producto = '" + servicio + "' ");
                    if (rs.next()) {
                        facturacion_id_producto = rs.getInt("id_producto");
//                        Facturacion_coronel_bogado.jTextField_servicio_descripcion.setText(rs.getString("descripcion"));
//                        Facturacion_coronel_bogado.jTextField_servicio_precio.setText(rs.getString("precio"));
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "ERROR: Puedes ingresar solo números.");
                    error = true;
                }
            }
        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }
    }

    public static boolean Verificar_acceso(int menu) {
        boolean acceso = false;
        try {
            Statement st = conexion.createStatement();
            ResultSet rs = st.executeQuery(""
                    + "SELECT * FROM acceso "
                    + "where id_usuario = '" + id_usuario + "' "
                    + "and id_acceso_detalle = '1' "
                    + "and id_menu = '" + menu + "'"
                    + "");
            if (rs.next()) {
                acceso = true;
            }
        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }
        return acceso;
    }

    public static void Beneficiarios_buscar_ficha(String ficha) {
        try {
            Statement st = conexion.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM beneficiario where ficha = '" + ficha + "' ");
            while (rs.next()) {
                id_beneficiario = rs.getInt("id_beneficiario");
                JOptionPane.showMessageDialog(null, "Ficha encontrada.");
                Beneficiario_traer_datos();
            }
        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }
    }

    public static void Facturacion_detalle_delete() {
        try {

            Statement st = conexion.createStatement();
            ResultSet rs = st.executeQuery(""
                    + "SELECT * FROM facturacion "
                    + "where id_facturacion = '" + id_facturacion + "'");
            if (rs.next()) {

                if (rs.getInt("id_estado") == 2) {
                    JOptionPane.showMessageDialog(null, "Factura impresa. No se puede modificar.");
                } else {
//                    Metodos.Auditoria_guardar("BORRADO", "FACTURA", rs.getString("factura").trim() + " " + rs.getString("descripcion").trim() + " " + rs.getString("precio").trim() + " " + rs.getString("unidades").trim());
                    st = conexion.createStatement();
                    rs = st.executeQuery(""
                            + "SELECT * FROM facturacion_detalle "
                            + "inner join facturacion on facturacion.id_facturacion = facturacion_detalle.id_facturacion "
                            + "inner join producto on producto.id_producto = facturacion_detalle.id_producto "
                            + "where id_facturacion_detalle = '" + id_facturacion_detalle + "'");
                    if (rs.next()) {
                        Code.Auditoria_guardar("BORRADO", "FACTURA", rs.getString("factura").trim() + " " + rs.getString("descripcion").trim() + " " + rs.getString("precio").trim() + " " + rs.getString("unidades").trim());
                    }

                    st = conexion.createStatement();
                    st.executeUpdate("DELETE FROM facturacion_detalle "
                            + "WHERE id_facturacion_detalle = '" + id_facturacion_detalle + "'");
                }
            }
        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }
    }

    public static void Beneficiario_update(String nombre, String ci, String direccion, Date fecha, String telefono, String ficha) {
        try {

            long ficha_long = Long.parseLong(ficha);
            boolean existe = false;
            String mensaje = "";
//            Statement st = conexion.createStatement();
//            ResultSet rs = st.executeQuery("SELECT * FROM beneficiario where ficha = '" + ficha_long + "' "
//                    + "and id_beneficiario != '" + id_beneficiario + "'");
//            if (rs.next()) {
//                existe = true;
//                mensaje = "Ficha ya existe.";
//            }
            Statement st = conexion.createStatement();
            ResultSet rs = st.executeQuery(""
                    + "SELECT * FROM beneficiario "
                    + "where documento = '" + Long.parseLong(ci) + "' "
                    + "and id_beneficiario != '" + id_beneficiario + "'");
            if (rs.next()) {
                existe = true;
                mensaje = mensaje + "CI ya existe.";
            }
            if (existe == false) {

                PreparedStatement stUpdateAuxiliar4 = conexion.prepareStatement(""
                        + "UPDATE beneficiario "
                        + "SET nombre = '" + nombre + "', "
                        + "direccion = '" + direccion + "', "
                        + "telefono = '" + Long.parseLong(telefono) + "', "
                        + "ficha = '" + Long.parseLong(ficha) + "', "
                        + "nacimiento = '" + util_Date_to_sql_date(hoy) + "', "
                        + "documento = '" + ci + "', "
                        + "id_sexo = '" + beneficiarios_id_sexo + "', "
                        + "id_barrio = '" + beneficiarios_id_barrio + "', "
                        + "id_grupo_sanguineo = '" + beneficiarios_id_grupo_sanguineo + "' "
                        + "WHERE id_beneficiario = '" + id_beneficiario + "'");
                stUpdateAuxiliar4.executeUpdate();
                JOptionPane.showMessageDialog(null, "Actualizado correctamente.");

            } else {
                JOptionPane.showMessageDialog(null, mensaje);
            }
        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }

    }

    public static void Farmacia_social_ventas_update() {
        try {
            PreparedStatement stUpdateAuxiliar4 = conexion.prepareStatement(""
                    + "UPDATE fs_facturacion "
                    + "SET id_beneficiario = '" + fs_facturacion_id_beneficiario + "' "
                    + "WHERE id_fs_facturacion = '" + id_fs_facturacion + "'");
            stUpdateAuxiliar4.executeUpdate();
        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }
    }

    public static void Farmacia_social_facturacion_seguro_update() {
        try {
            Statement st = conexion.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM fs_facturacion where id_fs_facturacion = '" + id_fs_facturacion + "' ");
            if (rs.next()) {

                int seguro = 1;
                String seguro_str = "SI";

                if (rs.getInt("id_facturacion_seguro") == 1) {
                    seguro = 0;
                    seguro_str = "NO";
                }

                PreparedStatement stUpdateAuxiliar4 = conexion.prepareStatement(""
                        + "UPDATE fs_facturacion "
                        + "SET id_facturacion_seguro = '" + seguro + "' "
                        + "WHERE id_fs_facturacion = '" + id_fs_facturacion + "' "
                        + "");
                stUpdateAuxiliar4.executeUpdate();

                Code.Auditoria_guardar("GENERADO", "F. S. FACTURA", "Seguro: " + seguro_str);

//                Farmacia_social_ventas.jTextField_seguro.setText(seguro_str);
//                Farmacia_social_ventas.jt_codigo.requestFocus();
            }
        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }
    }

    public static void Facturacion_update_total() {

        try {

            long total = 0;
            Statement st = conexion.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM facturacion_detalle where id_facturacion = '" + id_facturacion + "' "
                    + "");
            while (rs.next()) {
                total = total + rs.getLong("exentas") + rs.getLong("iva5") + rs.getLong("iva10");
            }

            PreparedStatement stUpdateAuxiliar4 = conexion.prepareStatement(""
                    + "UPDATE facturacion "
                    + "SET total = '" + total + "', "
                    + "id_tipo = '" + producto_id_tipo + "' "
                    + "WHERE id_facturacion = '" + id_facturacion + "'");
            stUpdateAuxiliar4.executeUpdate();

        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }

    }

    public static void Facturacion_update(Date fecha) {
        try {

            Statement st = conexion.createStatement();
            ResultSet rs = st.executeQuery(""
                    + "SELECT * FROM facturacion "
                    + "where id_facturacion = '" + id_facturacion + "'");
            if (rs.next()) {

                if (rs.getInt("id_estado") == 2) {
                    JOptionPane.showMessageDialog(null, "Factura impresa. No se puede modificar.");
                } else {

                    long seguro = 0;
                    st = conexion.createStatement();
                    rs = st.executeQuery("SELECT * FROM seguro_solidario where id_beneficiario = '" + facturacion_id_beneficiario + "' "
                            + "");
                    if (rs.next()) {
                        seguro = 10000;
                    }

                    PreparedStatement stUpdateAuxiliar4 = conexion.prepareStatement(""
                            + "UPDATE facturacion "
                            + "SET id_beneficiario = '" + facturacion_id_beneficiario + "', "
                            + "fecha = '" + util_Date_to_sql_date(fecha) + "', "
                            + "seguro = '" + seguro + "', "
                            + "id_facturacion_seguro = '" + facturacion_id_seguro + "' "
                            + "WHERE id_facturacion = '" + id_facturacion + "'");
                    stUpdateAuxiliar4.executeUpdate();

                    st = conexion.createStatement();
                    rs = st.executeQuery(""
                            + "SELECT *, facturacion_seguro.seguro as fseguro FROM facturacion "
                            + "inner join facturacion_seguro on facturacion_seguro.id_facturacion_seguro = facturacion.id_facturacion_seguro "
                            + "inner join beneficiario on beneficiario.id_beneficiario = facturacion.id_beneficiario "
                            + "where id_facturacion = '" + id_facturacion + "' ");
                    if (rs.next()) {
                        Code.Auditoria_guardar("ACTUALIZADO", "FACTURA", " " + rs.getString("factura").trim() + "  " + rs.getString("nombre").trim() + " Seguro: " + rs.getString("fseguro") + " " + rs.getString("fecha"));
                    }
                }
            }
        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }

    }

    public static void Facturacion_anular(String factura) {
        try {
            Verificar_conexion();

            PreparedStatement stUpdateAuxiliar4 = conexion.prepareStatement(""
                    + "UPDATE facturacion "
                    + "SET id_estado = '1', "
                    + "total = '0' "
                    + "WHERE id_facturacion = '" + factura + "'");
            stUpdateAuxiliar4.executeUpdate();

            stUpdateAuxiliar4 = conexion.prepareStatement(""
                    + "UPDATE facturacion_detalle "
                    + "SET exentas = '0', "
                    + "iva5 = '0', "
                    + "iva10 = '0' "
                    + "WHERE id_facturacion = '" + factura + "'");
            stUpdateAuxiliar4.executeUpdate();

            JOptionPane.showMessageDialog(null, "Anulado correctamente.");
        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }

    }

    public static void Caja_update(String descripcion, String factura) {
        try {
            Verificar_conexion();

            PreparedStatement stUpdateAuxiliar4 = conexion.prepareStatement(""
                    + "UPDATE caja "
                    + "SET factura = '" + Long.parseLong(factura) + "', "
                    + "descripcion = '" + descripcion + "' "
                    + "WHERE id_caja = '" + 1 + "'");
            stUpdateAuxiliar4.executeUpdate();
            JOptionPane.showMessageDialog(null, "Actualizado correctamente.");
        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }

    }

    public static void Farmacia_social_producto_update(String codigo, String descripcion, String precio, String iva, String recargo) {
        try {
            Verificar_conexion();

            precio = precio.replace(".", "");
            PreparedStatement stUpdateAuxiliar4 = conexion.prepareStatement(""
                    + "UPDATE farmacia_social_producto "
                    + "SET codigo = '" + codigo + "', "
                    + "descripcion = '" + descripcion + "', "
                    + "iva = '" + Integer.parseInt(iva) + "', "
                    + "recargo = '" + Integer.parseInt(recargo) + "', "
                    + "precio = '" + precio + "' "
                    + "WHERE id_farmacia_social_producto = '" + farmacia_social_producto_id_producto + "'");
            stUpdateAuxiliar4.executeUpdate();
            JOptionPane.showMessageDialog(null, "Actualizado correctamente.");
        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }

    }

    public static void Beneficiario_sexo_jtable() {
        try {
            Verificar_conexion();
            PreparedStatement ps = conexion.prepareStatement(""
                    + "select id_sexo, sexo "
                    + "from sexo "
                    + "");
            ResultSet rs = ps.executeQuery();
            ResultSetMetaData rsm = rs.getMetaData();
            DefaultTableModel dtm = (DefaultTableModel) Beneficiarios.jTable_sexo.getModel();
            for (int j = 0; j < Beneficiarios.jTable_sexo.getRowCount(); j++) {
                dtm.removeRow(j);
                j -= 1;
            }

            ArrayList<Object[]> data = new ArrayList<>();
            while (rs.next()) {
                Object[] rows = new Object[rsm.getColumnCount()];
                for (int i = 0; i < rows.length; i++) {
                    rows[i] = rs.getObject(i + 1).toString().trim();
                }
                data.add(rows);
            }

            dtm = (DefaultTableModel) Beneficiarios.jTable_sexo.getModel();
            for (int i = 0; i < data.size(); i++) {
                dtm.addRow(data.get(i));
            }

        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }
    }

    public static void Mensualidad_detalle_jtable() {
        try {
            Verificar_conexion();
            PreparedStatement ps = conexion.prepareStatement(""
                    + "select id_mensualidad, factura, nombre, plan, vencimiento, importe, estado "
                    + " from mensualidad "
                    + " inner join mensualidad_estado on mensualidad_estado.id_mensualidad_estado = mensualidad.id_mensualidad_estado "
                    + "  inner join plan on plan.id_plan = mensualidad.id_plan "
                    + "  inner join seguro_solidario on seguro_solidario.id_seguro = mensualidad.id_seguro "
                    + "  inner join seguro_beneficiario on seguro_beneficiario.id_seguro = seguro_solidario.id_seguro "
                    + "  inner join beneficiario on beneficiario.id_beneficiario = seguro_beneficiario.id_beneficiario "
                    + "where seguro_beneficiario.id_beneficiario = '" + mensualidad_id_beneficiario + "' "
                    + "  order by vencimiento DESC");
            ResultSet rs = ps.executeQuery();
            ResultSetMetaData rsm = rs.getMetaData();
            DefaultTableModel dtm = (DefaultTableModel) Pago_mensualidad.jTable_detalle.getModel();
            for (int j = 0; j < Pago_mensualidad.jTable_detalle.getRowCount(); j++) {
                dtm.removeRow(j);
                j -= 1;
            }

            ArrayList<Object[]> data = new ArrayList<>();
            while (rs.next()) {
                Object[] rows = new Object[rsm.getColumnCount()];
                for (int i = 0; i < rows.length; i++) {
                    rows[i] = rs.getObject(i + 1).toString().trim();
                }
                data.add(rows);
            }

            dtm = (DefaultTableModel) Pago_mensualidad.jTable_detalle.getModel();
            for (int i = 0; i < data.size(); i++) {
                dtm.addRow(data.get(i));
            }

        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }
    }

    public static void AporteSolidario_buscar_jtable(String buscar) {
//        try {
//
//            Verificar_conexion();
//
//            PreparedStatement ps = conexion.prepareStatement(""
//                    + "select id_seguro, nombre, numero "
//                    + "from seguro_solidario "
//                    + "inner join beneficiario on beneficiario.id_beneficiario = seguro_solidario.id_beneficiario "
//                    + "where nombre ilike '%" + buscar + "%'"
//                    + "");
//            ResultSet rs = ps.executeQuery();
//            ResultSetMetaData rsm = rs.getMetaData();
//            DefaultTableModel dtm = (DefaultTableModel) Contrato.jTable_seguro.getModel();
//            for (int j = 0; j < Contrato.jTable_seguro.getRowCount(); j++) {
//                dtm.removeRow(j);
//                j -= 1;
//            }
//
//            ArrayList<Object[]> data = new ArrayList<>();
//            while (rs.next()) {
//                Object[] rows = new Object[rsm.getColumnCount()];
//                for (int i = 0; i < rows.length; i++) {
//                    rows[i] = rs.getObject(i + 1).toString().trim();
//                }
//                data.add(rows);
//            }
//
//            dtm = (DefaultTableModel) Contrato.jTable_seguro.getModel();
//            for (int i = 0; i < data.size(); i++) {
//                dtm.addRow(data.get(i));
//            }
//
//        } catch (SQLException ex) {
//            MostrarError(ex, getNombreMetodo());
//        }
    }

//    public static void Beneficiarios_sub_beneficiarios_jtable() {
//        try {
//            PreparedStatement ps = conexion.prepareStatement(""
//                    + "select id_subbeneficiario, nombre "
//                    + "from subbeneficiario "
//                    + "inner join beneficiario on beneficiario.id_beneficiario = subbeneficiario.subbeneficiario "
//                    + "where subbeneficiario.id_beneficiario = '" + id_beneficiario + "' ");
//            ResultSet rs = ps.executeQuery();
//            ResultSetMetaData rsm = rs.getMetaData();
//            DefaultTableModel dtm = (DefaultTableModel) Beneficiarios.jTable_subbeneficiarios.getModel();
//            for (int j = 0; j < Beneficiarios.jTable_subbeneficiarios.getRowCount(); j++) {
//                dtm.removeRow(j);
//                j -= 1;
//            }
//
//            ArrayList<Object[]> data = new ArrayList<>();
//            while (rs.next()) {
//                Object[] rows = new Object[rsm.getColumnCount()];
//                for (int i = 0; i < rows.length; i++) {
//                    rows[i] = rs.getObject(i + 1).toString().trim();
//                }
//                data.add(rows);
//            }
//
//            dtm = (DefaultTableModel) Beneficiarios.jTable_subbeneficiarios.getModel();
//            for (int i = 0; i < data.size(); i++) {
//                dtm.addRow(data.get(i));
//            }
//
//        } catch (SQLException ex) {
//            MostrarError(ex, getNombreMetodo());
//        }
//    }
    public static void Beneficiarios_sub_beneficiario_jtable(String buscar) {
        try {
            PreparedStatement ps = conexion.prepareStatement(""
                    + "select id_beneficiario, nombre, ficha, documento "
                    + "from beneficiario where nombre ilike '%" + buscar + "%' "
                    + "");
            ResultSet rs = ps.executeQuery();
            ResultSetMetaData rsm = rs.getMetaData();
            DefaultTableModel dtm = (DefaultTableModel) Beneficiarios.jTable_buscar_subbeneficiario.getModel();
            for (int j = 0; j < Beneficiarios.jTable_buscar_subbeneficiario.getRowCount(); j++) {
                dtm.removeRow(j);
                j -= 1;
            }

            ArrayList<Object[]> data = new ArrayList<>();
            while (rs.next()) {
                Object[] rows = new Object[rsm.getColumnCount()];
                for (int i = 0; i < rows.length; i++) {
                    rows[i] = rs.getObject(i + 1).toString().trim();
                }
                data.add(rows);
            }

            dtm = (DefaultTableModel) Beneficiarios.jTable_buscar_subbeneficiario.getModel();
            for (int i = 0; i < data.size(); i++) {
                dtm.addRow(data.get(i));
            }

        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }
    }

    public static void Productos_descuento_jtable() {
//        try {
//            PreparedStatement ps = conexion.prepareStatement(""
//                    + "select id_descuento, plan, cantidad, porcentaje, frecuencia "
//                    + "from descuento "
//                    + "inner join plan on plan.id_plan = descuento.id_plan "
//                    + "inner join frecuencia on frecuencia.id_frecuencia = descuento.id_frecuencia "
//                    + " where id_producto = '" + id_producto + "' ");
//            ResultSet rs = ps.executeQuery();
//            ResultSetMetaData rsm = rs.getMetaData();
//            DefaultTableModel dtm = (DefaultTableModel) Servicios.jTable_descuento.getModel();
//            for (int j = 0; j < Servicios.jTable_descuento.getRowCount(); j++) {
//                dtm.removeRow(j);
//                j -= 1;
//            }
//
//            ArrayList<Object[]> data = new ArrayList<>();
//            while (rs.next()) {
//                Object[] rows = new Object[rsm.getColumnCount()];
//                for (int i = 0; i < rows.length; i++) {
//                    rows[i] = rs.getObject(i + 1).toString().trim();
//                }
//                data.add(rows);
//            }
//
//            dtm = (DefaultTableModel) Servicios.jTable_descuento.getModel();
//            for (int i = 0; i < data.size(); i++) {
//                dtm.addRow(data.get(i));
//            }
//
//        } catch (SQLException ex) {
//            // MostrarError(ex, getNombreMetodo());
//        }
    }

    public static void Contrato_beneficiarios_jtable() {
//        try {
//            PreparedStatement ps = conexion.prepareStatement(""
//                    + "select beneficiario.id_beneficiario, nombre "
//                    + "from seguro_beneficiario "
//                    + "inner join beneficiario on seguro_beneficiario.id_beneficiario = beneficiario.id_beneficiario "
//                    + "where id_seguro = '" + id_seguro_solidario + "' ");
//            ResultSet rs = ps.executeQuery();
//            ResultSetMetaData rsm = rs.getMetaData();
//            DefaultTableModel dtm = (DefaultTableModel) Contrato.jTable_beneficiarios.getModel();
//            for (int j = 0; j < Contrato.jTable_beneficiarios.getRowCount(); j++) {
//                dtm.removeRow(j);
//                j -= 1;
//            }
//
//            ArrayList<Object[]> data = new ArrayList<>();
//            while (rs.next()) {
//                Object[] rows = new Object[rsm.getColumnCount()];
//                for (int i = 0; i < rows.length; i++) {
//                    rows[i] = rs.getObject(i + 1).toString().trim();
//                }
//                data.add(rows);
//            }
//
//            dtm = (DefaultTableModel) Contrato.jTable_beneficiarios.getModel();
//            for (int i = 0; i < data.size(); i++) {
//                dtm.addRow(data.get(i));
//            }
//
//        } catch (SQLException ex) {
//            // MostrarError(ex, getNombreMetodo());
//        }
    }

    public static void Contrato_beneficiarios_jtable_clear() {
//        try {
//            PreparedStatement ps = conexion.prepareStatement(""
//                    + "select beneficiario.id_beneficiario, nombre "
//                    + "from seguro_beneficiario "
//                    + "inner join beneficiario on seguro_beneficiario.id_beneficiario = beneficiario.id_beneficiario "
//                    + "");
//            ResultSet rs = ps.executeQuery();
//            ResultSetMetaData rsm = rs.getMetaData();
//            DefaultTableModel dtm = (DefaultTableModel) Contrato.jTable_beneficiarios.getModel();
//            for (int j = 0; j < Contrato.jTable_beneficiarios.getRowCount(); j++) {
//                dtm.removeRow(j);
//                j -= 1;
//            }
//
//            ArrayList<Object[]> data = new ArrayList<>();
//            while (rs.next()) {
//                Object[] rows = new Object[rsm.getColumnCount()];
//                for (int i = 0; i < rows.length; i++) {
//                    rows[i] = rs.getObject(i + 1).toString().trim();
//                }
//                data.add(rows);
//            }
//
////            dtm = (DefaultTableModel) Contrato.jTable_beneficiarios.getModel();
////            for (int i = 0; i < data.size(); i++) {
////                dtm.addRow(data.get(i));
////            }
//        } catch (SQLException ex) {
//            // MostrarError(ex, getNombreMetodo());
//        }
    }

    public static void Mensualidad_beneficiario_jtable(String buscar) {
        try {
            PreparedStatement ps = conexion.prepareStatement(""
                    + "select id_beneficiario, nombre "
                    + "from beneficiario "
                    + " where nombre ilike '%" + buscar + "%' ");
            ResultSet rs = ps.executeQuery();
            ResultSetMetaData rsm = rs.getMetaData();
            DefaultTableModel dtm = (DefaultTableModel) Pago_mensualidad.jTable_beneficiario.getModel();
            for (int j = 0; j < Pago_mensualidad.jTable_beneficiario.getRowCount(); j++) {
                dtm.removeRow(j);
                j -= 1;
            }

            ArrayList<Object[]> data = new ArrayList<>();
            while (rs.next()) {
                Object[] rows = new Object[rsm.getColumnCount()];
                for (int i = 0; i < rows.length; i++) {
                    rows[i] = rs.getObject(i + 1).toString().trim();
                }
                data.add(rows);
            }

            dtm = (DefaultTableModel) Pago_mensualidad.jTable_beneficiario.getModel();
            for (int i = 0; i < data.size(); i++) {
                dtm.addRow(data.get(i));
            }

        } catch (SQLException ex) {
            // MostrarError(ex, getNombreMetodo());
        }
    }

//    public static void Seguro_pago_jtable() {
//        try {
//            PreparedStatement ps = conexion.prepareStatement(""
//                    + "select id_pago, fecha, monto "
//                    + "from seguro_pagos "
//                    + " where id_seguro = '" + seguro_id_seguro + "' ");
//            ResultSet rs = ps.executeQuery();
//            ResultSetMetaData rsm = rs.getMetaData();
//            DefaultTableModel dtm = (DefaultTableModel) Contrato.jTable_pagos.getModel();
//            for (int j = 0; j < Contrato.jTable_pagos.getRowCount(); j++) {
//                dtm.removeRow(j);
//                j -= 1;
//            }
//
//            ArrayList<Object[]> data = new ArrayList<>();
//            while (rs.next()) {
//                Object[] rows = new Object[rsm.getColumnCount()];
//                for (int i = 0; i < rows.length; i++) {
//                    rows[i] = rs.getObject(i + 1).toString().trim();
//                }
//                data.add(rows);
//            }
//
//            dtm = (DefaultTableModel) Contrato.jTable_pagos.getModel();
//            for (int i = 0; i < data.size(); i++) {
//                dtm.addRow(data.get(i));
//            }
//
//        } catch (SQLException ex) {
//            // MostrarError(ex, getNombreMetodo());
//        }
//    }
    public static void Productos_plan_jtable() {
//        try {
//            PreparedStatement ps = conexion.prepareStatement(""
//                    + "select id_plan, plan "
//                    + "from plan "
//                    + "");
//            ResultSet rs = ps.executeQuery();
//            ResultSetMetaData rsm = rs.getMetaData();
//            DefaultTableModel dtm = (DefaultTableModel) Servicios.jTable_plan.getModel();
//            for (int j = 0; j < Servicios.jTable_plan.getRowCount(); j++) {
//                dtm.removeRow(j);
//                j -= 1;
//            }
//
//            ArrayList<Object[]> data = new ArrayList<>();
//            while (rs.next()) {
//                Object[] rows = new Object[rsm.getColumnCount()];
//                for (int i = 0; i < rows.length; i++) {
//                    rows[i] = rs.getObject(i + 1).toString().trim();
//                }
//                data.add(rows);
//            }
//
//            dtm = (DefaultTableModel) Servicios.jTable_plan.getModel();
//            for (int i = 0; i < data.size(); i++) {
//                dtm.addRow(data.get(i));
//            }
//
//        } catch (SQLException ex) {
//            MostrarError(ex, getNombreMetodo());
//        }
    }

    public static void Productos_frecuencia_jtable() {
//        try {
//            PreparedStatement ps = conexion.prepareStatement(""
//                    + "select id_frecuencia, frecuencia "
//                    + "from frecuencia "
//                    + "");
//            ResultSet rs = ps.executeQuery();
//            ResultSetMetaData rsm = rs.getMetaData();
//            DefaultTableModel dtm = (DefaultTableModel) Servicios.jTable_frecuencia.getModel();
//            for (int j = 0; j < Servicios.jTable_frecuencia.getRowCount(); j++) {
//                dtm.removeRow(j);
//                j -= 1;
//            }
//
//            ArrayList<Object[]> data = new ArrayList<>();
//            while (rs.next()) {
//                Object[] rows = new Object[rsm.getColumnCount()];
//                for (int i = 0; i < rows.length; i++) {
//                    rows[i] = rs.getObject(i + 1).toString().trim();
//                }
//                data.add(rows);
//            }
//
//            dtm = (DefaultTableModel) Servicios.jTable_frecuencia.getModel();
//            for (int i = 0; i < data.size(); i++) {
//                dtm.addRow(data.get(i));
//            }
//
//        } catch (SQLException ex) {
//            MostrarError(ex, getNombreMetodo());
//        }
    }

    public static void FS_Compras_proveedor_jtable(String buscar) {
//        try {
//            PreparedStatement ps = conexion.prepareStatement(""
//                    + "select id_proveedor, nombre "
//                    + "from proveedor "
//                    + "where nombre ilike '%" + buscar + "%'");
//            ResultSet rs = ps.executeQuery();
//            ResultSetMetaData rsm = rs.getMetaData();
//            DefaultTableModel dtm = (DefaultTableModel) Farmacia_social_compras.jTable_proveedor.getModel();
//            for (int j = 0; j < Farmacia_social_compras.jTable_proveedor.getRowCount(); j++) {
//                dtm.removeRow(j);
//                j -= 1;
//            }
//
//            ArrayList<Object[]> data = new ArrayList<>();
//            while (rs.next()) {
//                Object[] rows = new Object[rsm.getColumnCount()];
//                for (int i = 0; i < rows.length; i++) {
//                    rows[i] = rs.getObject(i + 1).toString().trim();
//                }
//                data.add(rows);
//            }
//
//            dtm = (DefaultTableModel) Farmacia_social_compras.jTable_proveedor.getModel();
//            for (int i = 0; i < data.size(); i++) {
//                dtm.addRow(data.get(i));
//            }
//
//        } catch (SQLException ex) {
//            MostrarError(ex, getNombreMetodo());
//        }
    }

    public static void Vencimientos_productos_jtable(String buscar) {
//        try {
//            PreparedStatement ps = conexion.prepareStatement(""
//                    + "select id_farmacia_social_producto, descripcion "
//                    + "from farmacia_social_producto "
//                    + "where descripcion ilike '%" + buscar + "%'");
//            ResultSet rs = ps.executeQuery();
//            ResultSetMetaData rsm = rs.getMetaData();
//            DefaultTableModel dtm = (DefaultTableModel) Vencimientos.jTable_productos.getModel();
//            for (int j = 0; j < Vencimientos.jTable_productos.getRowCount(); j++) {
//                dtm.removeRow(j);
//                j -= 1;
//            }
//
//            ArrayList<Object[]> data = new ArrayList<>();
//            while (rs.next()) {
//                Object[] rows = new Object[rsm.getColumnCount()];
//                for (int i = 0; i < rows.length; i++) {
//                    rows[i] = rs.getObject(i + 1).toString().trim();
//                }
//                data.add(rows);
//            }
//
//            dtm = (DefaultTableModel) Vencimientos.jTable_productos.getModel();
//            for (int i = 0; i < data.size(); i++) {
//                dtm.addRow(data.get(i));
//            }
//
//        } catch (SQLException ex) {
//            MostrarError(ex, getNombreMetodo());
//        }
    }

    public static void Farmacia_social_buscar_jtable(String buscar) {
//        try {
//            PreparedStatement ps = conexion.prepareStatement(""
//                    + "select id_fs_facturacion, nombre, fecha, factura "
//                    + "from fs_facturacion "
//                    + "inner join beneficiario on beneficiario.id_beneficiario = fs_facturacion.id_beneficiario "
//                    + "where nombre ilike '%" + buscar + "%' "
//                    + "order by id_fs_facturacion DESC");
//            ResultSet rs = ps.executeQuery();
//            ResultSetMetaData rsm = rs.getMetaData();
//            DefaultTableModel dtm = (DefaultTableModel) Farmacia_social_ventas.jTable_buscar.getModel();
//            for (int j = 0; j < Farmacia_social_ventas.jTable_buscar.getRowCount(); j++) {
//                dtm.removeRow(j);
//                j -= 1;
//            }
//
//            ArrayList<Object[]> data = new ArrayList<>();
//            while (rs.next()) {
//                Object[] rows = new Object[rsm.getColumnCount()];
//                for (int i = 0; i < rows.length; i++) {
//                    rows[i] = rs.getObject(i + 1).toString().trim();
//                }
//                data.add(rows);
//            }
//
//            dtm = (DefaultTableModel) Farmacia_social_ventas.jTable_buscar.getModel();
//            for (int i = 0; i < data.size(); i++) {
//                dtm.addRow(data.get(i));
//            }
//
//        } catch (SQLException ex) {
//            MostrarError(ex, getNombreMetodo());
//        }
    }

    public static void Farmacia_social_facturacion_detalle_jtable() {
//        try {
//
//            Verificar_conexion();
//
//            PreparedStatement ps = conexion.prepareStatement(""
//                    + "select id_fs_facturacion_detalle, descripcion, unidades, fs_facturacion_detalle.precio, exentas, iva5, iva10  "
//                    + "from fs_facturacion_detalle "
//                    + "inner join farmacia_social_producto on farmacia_social_producto.id_farmacia_social_producto = fs_facturacion_detalle.id_farmacia_social_producto "
//                    + "where id_fs_facturacion = '" + id_fs_facturacion + "' ");
//            ResultSet rs = ps.executeQuery();
//            ResultSetMetaData rsm = rs.getMetaData();
//            DefaultTableModel dtm = (DefaultTableModel) Farmacia_social_ventas.jTable_detalle.getModel();
//            for (int j = 0; j < Farmacia_social_ventas.jTable_detalle.getRowCount(); j++) {
//                dtm.removeRow(j);
//                j -= 1;
//            }
//
//            ArrayList<Object[]> data = new ArrayList<>();
//            while (rs.next()) {
//                Object[] rows = new Object[rsm.getColumnCount()];
//                for (int i = 0; i < rows.length; i++) {
//                    rows[i] = rs.getObject(i + 1).toString().trim();
//                }
//                data.add(rows);
//            }
//
//            dtm = (DefaultTableModel) Farmacia_social_ventas.jTable_detalle.getModel();
//            for (int i = 0; i < data.size(); i++) {
//                dtm.addRow(data.get(i));
//            }
//
//        } catch (SQLException ex) {
//            MostrarError(ex, getNombreMetodo());
//        }
    }

    public static void Acceso_jtable() {
//        try {
//            PreparedStatement ps = conexion.prepareStatement(""
//                    + "select id_acceso, nombre, menu, acceso_detalle "
//                    + "from acceso "
//                    + "inner join usuario on usuario.id_usuario = acceso.id_usuario "
//                    + "inner join acceso_detalle on acceso_detalle.id_acceso_detalle = acceso.id_acceso_detalle "
//                    + "inner join menu on menu.id_menu = acceso.id_menu "
//                    + "and acceso.id_usuario > '0' "
//                    + "order by nombre");
//            ResultSet rs = ps.executeQuery();
//            ResultSetMetaData rsm = rs.getMetaData();
//            DefaultTableModel dtm = (DefaultTableModel) Acceso.jTable_acceso.getModel();
//            for (int j = 0; j < Acceso.jTable_acceso.getRowCount(); j++) {
//                dtm.removeRow(j);
//                j -= 1;
//            }
//
//            ArrayList<Object[]> data = new ArrayList<>();
//            while (rs.next()) {
//                Object[] rows = new Object[rsm.getColumnCount()];
//                for (int i = 0; i < rows.length; i++) {
//                    rows[i] = rs.getObject(i + 1).toString().trim();
//                }
//                data.add(rows);
//            }
//
//            dtm = (DefaultTableModel) Acceso.jTable_acceso.getModel();
//            for (int i = 0; i < data.size(); i++) {
//                dtm.addRow(data.get(i));
//            }
//
//        } catch (SQLException ex) {
//            MostrarError(ex, getNombreMetodo());
//        }
    }

    public static void Acceso_detalle_jtable() {
//        try {
//            PreparedStatement ps = conexion.prepareStatement(""
//                    + "select id_acceso_detalle, acceso_detalle "
//                    + "from acceso_detalle "
//                    + "");
//            ResultSet rs = ps.executeQuery();
//            ResultSetMetaData rsm = rs.getMetaData();
//            DefaultTableModel dtm = (DefaultTableModel) Acceso.jTable_acceso_detalle.getModel();
//            for (int j = 0; j < Acceso.jTable_acceso_detalle.getRowCount(); j++) {
//                dtm.removeRow(j);
//                j -= 1;
//            }
//
//            ArrayList<Object[]> data = new ArrayList<>();
//            while (rs.next()) {
//                Object[] rows = new Object[rsm.getColumnCount()];
//                for (int i = 0; i < rows.length; i++) {
//                    rows[i] = rs.getObject(i + 1).toString().trim();
//                }
//                data.add(rows);
//            }
//
//            dtm = (DefaultTableModel) Acceso.jTable_acceso_detalle.getModel();
//            for (int i = 0; i < data.size(); i++) {
//                dtm.addRow(data.get(i));
//            }
//
//        } catch (SQLException ex) {
//            MostrarError(ex, getNombreMetodo());
//        }
    }

    public static void Productos_jtable(String buscar) {
//        try {
//            PreparedStatement ps = conexion.prepareStatement(""
//                    + "select id_producto, descripcion "
//                    + "from producto  "
//                    + " where descripcion ilike '%" + buscar + "%' ");
//            ResultSet rs = ps.executeQuery();
//            ResultSetMetaData rsm = rs.getMetaData();
//            DefaultTableModel dtm = (DefaultTableModel) Servicios.jTable_productos.getModel();
//            for (int j = 0; j < Servicios.jTable_productos.getRowCount(); j++) {
//                dtm.removeRow(j);
//                j -= 1;
//            }
//
//            ArrayList<Object[]> data = new ArrayList<>();
//            while (rs.next()) {
//                Object[] rows = new Object[rsm.getColumnCount()];
//                for (int i = 0; i < rows.length; i++) {
//                    rows[i] = rs.getObject(i + 1).toString().trim();
//                }
//                data.add(rows);
//            }
//
//            dtm = (DefaultTableModel) Servicios.jTable_productos.getModel();
//            for (int i = 0; i < data.size(); i++) {
//                dtm.addRow(data.get(i));
//            }
//
//        } catch (SQLException ex) {
//            MostrarError(ex, getNombreMetodo());
//        }
    }

    public static void Vencimientos_buscar_jtable(String buscar) {
//        try {
//            PreparedStatement ps = conexion.prepareStatement(""
//                    + "select id_fs_productos_vencimiento, descripcion, lote, vencimiento  "
//                    + "from fs_productos_vencimiento  "
//                    + "inner join farmacia_social_producto on farmacia_social_producto.id_farmacia_social_producto =  fs_productos_vencimiento.id_farmacia_social_producto "
//                    + " where descripcion ilike '%" + buscar + "%' ");
//            ResultSet rs = ps.executeQuery();
//            ResultSetMetaData rsm = rs.getMetaData();
//            DefaultTableModel dtm = (DefaultTableModel) Vencimientos.jTable_buscar.getModel();
//            for (int j = 0; j < Vencimientos.jTable_buscar.getRowCount(); j++) {
//                dtm.removeRow(j);
//                j -= 1;
//            }
//
//            ArrayList<Object[]> data = new ArrayList<>();
//            while (rs.next()) {
//                Object[] rows = new Object[rsm.getColumnCount()];
//                for (int i = 0; i < rows.length; i++) {
//                    rows[i] = rs.getObject(i + 1).toString().trim();
//                }
//                data.add(rows);
//            }
//
//            dtm = (DefaultTableModel) Vencimientos.jTable_buscar.getModel();
//            for (int i = 0; i < data.size(); i++) {
//                dtm.addRow(data.get(i));
//            }
//
//        } catch (SQLException ex) {
//            MostrarError(ex, getNombreMetodo());
//        }
    }

    public static void Auditoria_jtable() {
//        try {
//            PreparedStatement ps = conexion.prepareStatement(""
//                    + "select * "
//                    + "from auditoria "
//                    + "order by id_auditoria DESC");
//            ResultSet rs = ps.executeQuery();
//            ResultSetMetaData rsm = rs.getMetaData();
//            DefaultTableModel dtm = (DefaultTableModel) Auditoria.jTable1.getModel();
//            for (int j = 0; j < Auditoria.jTable1.getRowCount(); j++) {
//                dtm.removeRow(j);
//                j -= 1;
//            }
//
//            ArrayList<Object[]> data = new ArrayList<>();
//            while (rs.next()) {
//                Object[] rows = new Object[rsm.getColumnCount()];
//                for (int i = 0; i < rows.length; i++) {
//                    rows[i] = rs.getObject(i + 1).toString().trim();
//                }
//                data.add(rows);
//            }
//
//            dtm = (DefaultTableModel) Auditoria.jTable1.getModel();
//            for (int i = 0; i < data.size(); i++) {
//                dtm.addRow(data.get(i));
//            }
//
//        } catch (SQLException ex) {
//            MostrarError(ex, getNombreMetodo());
//        }
    }

    public static void Facturacion_detalle_jtable() {
//        try {
//
//            long total = 0;
//            long exentas = 0;
//            long iva5 = 0;
//            long iva10 = 0;
//            long subtotal_iva10 = 0;
//            long subtotal_exentas = 0;
//            long subtotal_iva5 = 0;
//
//            Statement st = conexion.createStatement();
//            ResultSet rs = st.executeQuery("SELECT * FROM facturacion_detalle where id_facturacion = '" + id_facturacion + "'");
//            while (rs.next()) {
//
//                total = rs.getLong("exentas") + rs.getLong("iva5") + rs.getLong("iva10") + total;
//
//                exentas = rs.getLong("exentas") + exentas;
//                iva5 = rs.getLong("iva5") + iva5;
//                iva10 = rs.getLong("iva10") + iva10;
//
//                subtotal_exentas = exentas;
//                subtotal_iva5 = iva5;
//                subtotal_iva10 = iva10;
//
//                iva5 = iva5 / 21;
//                iva10 = iva10 / 11;
//
//            }
//
////            Facturacion_coronel_bogado.jTextField_total.setText(SepararMiles(String.valueOf(total)));
////            Facturacion_coronel_bogado.jTextField_subtotal_exentas.setText(SepararMiles(String.valueOf(subtotal_exentas)));
////            Facturacion_coronel_bogado.jTextField_subtotal5.setText(SepararMiles(String.valueOf(subtotal_iva5)));
////            Facturacion_coronel_bogado.jTextField_subtotal10.setText(SepararMiles(String.valueOf(subtotal_iva10)));
////            Facturacion_coronel_bogado.jTextField_iva10.setText(SepararMiles(String.valueOf(iva10)));
////            Facturacion_coronel_bogado.jTextField_iva_5.setText(SepararMiles(String.valueOf(iva5)));
////            Facturacion_coronel_bogado.jTextField_subtotal_exentas.setText(SepararMiles(String.valueOf(exentas)));
//
//            PreparedStatement ps = conexion.prepareStatement(""
//                    + "select id_facturacion_detalle, descripcion, unidades, facturacion_detalle.precio as fprecio, exentas, iva5, iva10 "
//                    + "from facturacion_detalle "
//                    + "inner join producto on producto.id_producto = facturacion_detalle.id_producto "
//                    + "where id_facturacion = '" + id_facturacion + "'");
//            rs = ps.executeQuery();
//            ResultSetMetaData rsm = rs.getMetaData();
//            DefaultTableModel dtm = (DefaultTableModel) Facturacion_coronel_bogado.jTable_detalle.getModel();
//            for (int j = 0; j < Facturacion_coronel_bogado.jTable_detalle.getRowCount(); j++) {
//                dtm.removeRow(j);
//                j -= 1;
//            }
//
//            ArrayList<Object[]> data = new ArrayList<>();
//            while (rs.next()) {
//                Object[] rows = new Object[rsm.getColumnCount()];
//                for (int i = 0; i < rows.length; i++) {
//                    rows[i] = rs.getObject(i + 1).toString().trim();
//                }
//                data.add(rows);
//            }
//
//            dtm = (DefaultTableModel) Facturacion_coronel_bogado.jTable_detalle.getModel();
//            for (int i = 0; i < data.size(); i++) {
//                dtm.addRow(data.get(i));
//            }
//
//        } catch (SQLException ex) {
//            MostrarError(ex, getNombreMetodo());
//        }
    }

    public static void Caja_jtable() {
        try {
            PreparedStatement ps = conexion.prepareStatement(""
                    + "select id_caja, factura, descripcion "
                    + "from caja "
                    + "");
            ResultSet rs = ps.executeQuery();
            ResultSetMetaData rsm = rs.getMetaData();
            DefaultTableModel dtm = (DefaultTableModel) Caja.jTable1.getModel();
            for (int j = 0; j < Caja.jTable1.getRowCount(); j++) {
                dtm.removeRow(j);
                j -= 1;
            }

            ArrayList<Object[]> data = new ArrayList<>();
            while (rs.next()) {
                Object[] rows = new Object[rsm.getColumnCount()];
                for (int i = 0; i < rows.length; i++) {
                    rows[i] = rs.getObject(i + 1).toString().trim();
                }
                data.add(rows);
            }

            dtm = (DefaultTableModel) Caja.jTable1.getModel();
            for (int i = 0; i < data.size(); i++) {
                dtm.addRow(data.get(i));
            }

        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }
    }

    public static void Facturacion_buscar_jtable(String buscar) {
//        try {
//            Date hoy = new Date();
//
//            String sql = "select id_facturacion, nombre, factura, tipo, fecha, total, estado "
//                    + "from facturacion "
//                    + "inner join beneficiario on beneficiario.id_beneficiario = facturacion.id_beneficiario "
//                    + "inner join tipo on tipo.id_tipo = facturacion.id_tipo "
//                    + "inner join estado on estado.id_estado = facturacion.id_estado "
//                    + "where nombre ilike '%" + buscar + "%' and factura > '0' "
//                    + "order by factura DESC";
//
//            if (buscar.length() < 1) {
//                sql = "select id_facturacion, nombre, factura, tipo, fecha, total, estado "
//                        + "from facturacion "
//                        + "inner join beneficiario on beneficiario.id_beneficiario = facturacion.id_beneficiario "
//                        + "inner join tipo on tipo.id_tipo = facturacion.id_tipo "
//                        + "inner join estado on estado.id_estado = facturacion.id_estado "
//                        + "where factura != '0' "
//                        + "order by fecha DESC"
//                        + " limit 200";
//            }
//
//            if (isNumeric(buscar.trim())) {
//                sql = "select id_facturacion, nombre, factura, tipo, fecha, total, estado "
//                        + "from facturacion "
//                        + "inner join beneficiario on beneficiario.id_beneficiario = facturacion.id_beneficiario "
//                        + "inner join tipo on tipo.id_tipo = facturacion.id_tipo "
//                        + "inner join estado on estado.id_estado = facturacion.id_estado "
//                        + "where factura = '" + buscar + "' "
//                        + "order by factura DESC";
//
//            }
//
//            PreparedStatement ps = conexion.prepareStatement(sql);
//
//            ResultSet rs = ps.executeQuery();
//            ResultSetMetaData rsm = rs.getMetaData();
//            DefaultTableModel dtm = (DefaultTableModel) Facturacion_coronel_bogado.jTable_buscar.getModel();
//            for (int j = 0; j < Facturacion_coronel_bogado.jTable_buscar.getRowCount(); j++) {
//                dtm.removeRow(j);
//                j -= 1;
//            }
//
//            ArrayList<Object[]> data = new ArrayList<>();
//            while (rs.next()) {
//                int i = 0;
//                Object[] rows = new Object[rsm.getColumnCount()];
//                rows[i] = rs.getObject(1);
//                rows[++i] = rs.getObject(2);
//                rows[++i] = rs.getObject(3);
//                rows[++i] = rs.getObject(4);
//                rows[++i] = rs.getObject(5);
//                rows[++i] = rs.getDouble(6);
//                rows[++i] = rs.getObject(7);
//                data.add(rows);
//            }
//
//            dtm = (DefaultTableModel) Facturacion_coronel_bogado.jTable_buscar.getModel();
//            for (int i = 0; i < data.size(); i++) {
//                dtm.addRow(data.get(i));
//            }
//
//        } catch (SQLException ex) {
//            MostrarError(ex, getNombreMetodo());
//        }
    }

    public static void Facturacion_seguro_jtable() {
//        try {
//            PreparedStatement ps = conexion.prepareStatement(""
//                    + "select id_facturacion_seguro, seguro "
//                    + "from facturacion_seguro "
//                    + "order by seguro");
//            ResultSet rs = ps.executeQuery();
//            ResultSetMetaData rsm = rs.getMetaData();
//            DefaultTableModel dtm = (DefaultTableModel) Facturacion_coronel_bogado.jTable_seguro.getModel();
//            for (int j = 0; j < Facturacion_coronel_bogado.jTable_seguro.getRowCount(); j++) {
//                dtm.removeRow(j);
//                j -= 1;
//            }
//
//            ArrayList<Object[]> data = new ArrayList<>();
//            while (rs.next()) {
//                Object[] rows = new Object[rsm.getColumnCount()];
//                for (int i = 0; i < rows.length; i++) {
//                    rows[i] = rs.getObject(i + 1).toString().trim();
//                }
//                data.add(rows);
//            }
//
//            dtm = (DefaultTableModel) Facturacion_coronel_bogado.jTable_seguro.getModel();
//            for (int i = 0; i < data.size(); i++) {
//                dtm.addRow(data.get(i));
//            }
//
//        } catch (SQLException ex) {
//            MostrarError(ex, getNombreMetodo());
//        }
    }

    public static void Configuracion_caja_jtable() {
//        try {
//            PreparedStatement ps = conexion.prepareStatement(""
//                    + "select id_caja, descripcion "
//                    + "from caja "
//                    + "");
//            ResultSet rs = ps.executeQuery();
//            ResultSetMetaData rsm = rs.getMetaData();
//            DefaultTableModel dtm = (DefaultTableModel) Configuraciones.jTable_caja_buscar.getModel();
//            for (int j = 0; j < Configuraciones.jTable_caja_buscar.getRowCount(); j++) {
//                dtm.removeRow(j);
//                j -= 1;
//            }
//
//            ArrayList<Object[]> data = new ArrayList<>();
//            while (rs.next()) {
//                Object[] rows = new Object[rsm.getColumnCount()];
//                for (int i = 0; i < rows.length; i++) {
//                    rows[i] = rs.getObject(i + 1).toString().trim();
//                }
//                data.add(rows);
//            }
//
//            dtm = (DefaultTableModel) Configuraciones.jTable_caja_buscar.getModel();
//            for (int i = 0; i < data.size(); i++) {
//                dtm.addRow(data.get(i));
//            }
//
//        } catch (SQLException ex) {
//            MostrarError(ex, getNombreMetodo());
//        }
    }

    public static void Configuraciones_caja_jtable() {
//        try {
//            PreparedStatement ps = conexion.prepareStatement(""
//                    + "select id_caja_usuario, nombre, descripcion, factura "
//                    + "from caja_usuario "
//                    + "inner join usuario on usuario.id_usuario = caja_usuario.id_usuario "
//                    + "inner join caja on caja.id_caja = caja_usuario.id_caja "
//                    + "");
//            ResultSet rs = ps.executeQuery();
//            ResultSetMetaData rsm = rs.getMetaData();
//            DefaultTableModel dtm = (DefaultTableModel) Configuraciones.jTable_caja.getModel();
//            for (int j = 0; j < Configuraciones.jTable_caja.getRowCount(); j++) {
//                dtm.removeRow(j);
//                j -= 1;
//            }
//
//            ArrayList<Object[]> data = new ArrayList<>();
//            while (rs.next()) {
//                Object[] rows = new Object[rsm.getColumnCount()];
//                for (int i = 0; i < rows.length; i++) {
//                    rows[i] = rs.getObject(i + 1).toString().trim();
//                }
//                data.add(rows);
//            }
//
//            dtm = (DefaultTableModel) Configuraciones.jTable_caja.getModel();
//            for (int i = 0; i < data.size(); i++) {
//                dtm.addRow(data.get(i));
//            }
//
//        } catch (SQLException ex) {
//            MostrarError(ex, getNombreMetodo());
//        }
    }

    public static void Configuracion_usuarios_jtable() {
//        try {
//            PreparedStatement ps = conexion.prepareStatement(""
//                    + "select id_usuario, nombre "
//                    + "from usuario "
//                    + "");
//            ResultSet rs = ps.executeQuery();
//            ResultSetMetaData rsm = rs.getMetaData();
//            DefaultTableModel dtm = (DefaultTableModel) Configuraciones.jTable_usuario.getModel();
//            for (int j = 0; j < Configuraciones.jTable_usuario.getRowCount(); j++) {
//                dtm.removeRow(j);
//                j -= 1;
//            }
//
//            ArrayList<Object[]> data = new ArrayList<>();
//            while (rs.next()) {
//                Object[] rows = new Object[rsm.getColumnCount()];
//                for (int i = 0; i < rows.length; i++) {
//                    rows[i] = rs.getObject(i + 1).toString().trim();
//                }
//                data.add(rows);
//            }
//
//            dtm = (DefaultTableModel) Configuraciones.jTable_usuario.getModel();
//            for (int i = 0; i < data.size(); i++) {
//                dtm.addRow(data.get(i));
//            }
//
//        } catch (SQLException ex) {
//            MostrarError(ex, getNombreMetodo());
//        }
    }

    public static void Facturacion_productos_jtable() {
//        try {
//            PreparedStatement ps = conexion.prepareStatement(""
//                    + "select id_producto, descripcion "
//                    + "from producto "
//                    + "order by descripcion");
//            ResultSet rs = ps.executeQuery();
//            ResultSetMetaData rsm = rs.getMetaData();
//            DefaultTableModel dtm = (DefaultTableModel) Facturacion_coronel_bogado.jTable_productos.getModel();
//            for (int j = 0; j < Facturacion_coronel_bogado.jTable_productos.getRowCount(); j++) {
//                dtm.removeRow(j);
//                j -= 1;
//            }
//
//            ArrayList<Object[]> data = new ArrayList<>();
//            while (rs.next()) {
//                Object[] rows = new Object[rsm.getColumnCount()];
//                for (int i = 0; i < rows.length; i++) {
//                    rows[i] = rs.getObject(i + 1).toString().trim();
//                }
//                data.add(rows);
//            }
//
//            dtm = (DefaultTableModel) Facturacion_coronel_bogado.jTable_productos.getModel();
//            for (int i = 0; i < data.size(); i++) {
//                dtm.addRow(data.get(i));
//            }
//
//        } catch (SQLException ex) {
//            MostrarError(ex, getNombreMetodo());
//        }
    }

    public static void Contrato_buscar_por_numero_jtable(String buscar) {
//        try {
//            PreparedStatement ps = conexion.prepareStatement(""
//                    + "select id_seguro, numero "
//                    + "from seguro_solidario "
//                    + "where numero = '" + buscar + "' ");
//            ResultSet rs = ps.executeQuery();
//            ResultSetMetaData rsm = rs.getMetaData();
//            DefaultTableModel dtm = (DefaultTableModel) Contrato.jTable_seguro.getModel();
//            for (int j = 0; j < Contrato.jTable_seguro.getRowCount(); j++) {
//                dtm.removeRow(j);
//                j -= 1;
//            }
//
//            ArrayList<Object[]> data = new ArrayList<>();
//            while (rs.next()) {
//                Object[] rows = new Object[rsm.getColumnCount()];
//                for (int i = 0; i < rows.length; i++) {
//                    rows[i] = rs.getObject(i + 1).toString().trim();
//                }
//                data.add(rows);
//            }
//
//            dtm = (DefaultTableModel) Contrato.jTable_seguro.getModel();
//            for (int i = 0; i < data.size(); i++) {
//                dtm.addRow(data.get(i));
//            }
////            ps = conexion.prepareStatement(""
////                    + "select seguro_solidario.id_seguro, numero, nombre "
////                    + "from seguro_solidario "
////                    + "inner join seguro_beneficiario on seguro_beneficiario.id_seguro = seguro_solidario.id_seguro "
////                    + "inner join beneficiario on beneficiario.id_beneficiario = seguro_beneficiario.id_beneficiario "
////                    + "where numero = '" + buscar + "' ");
////            rs = ps.executeQuery();
////            rsm = rs.getMetaData();
////            dtm = (DefaultTableModel) Contrato.jTable_seguro.getModel();
////            for (int j = 0; j < Contrato.jTable_seguro.getRowCount(); j++) {
////                dtm.removeRow(j);
////                j -= 1;
////            }
////
////            data = new ArrayList<>();
////            while (rs.next()) {
////                Object[] rows = new Object[rsm.getColumnCount()];
////                for (int i = 0; i < rows.length; i++) {
////                    rows[i] = rs.getObject(i + 1).toString().trim();
////                }
////                data.add(rows);
////            }
////
////            dtm = (DefaultTableModel) Contrato.jTable_seguro.getModel();
////            for (int i = 0; i < data.size(); i++) {
////                dtm.addRow(data.get(i));
////            }
//
//        } catch (SQLException ex) {
//            MostrarError(ex, getNombreMetodo());
//        }
    }

    public static void Contrato_buscar_por_nombre_jtable(String buscar) {
//        try {
//            PreparedStatement ps = conexion.prepareStatement(""
//                    + "select seguro_solidario.id_seguro, numero, nombre "
//                    + "from seguro_solidario "
//                    + "inner join seguro_beneficiario on seguro_beneficiario.id_seguro = seguro_solidario.id_seguro "
//                    + "inner join beneficiario on seguro_beneficiario.id_beneficiario = beneficiario.id_beneficiario "
//                    + "where nombre ilike '%" + buscar + "%' ");
//            ResultSet rs = ps.executeQuery();
//            ResultSetMetaData rsm = rs.getMetaData();
//            DefaultTableModel dtm = (DefaultTableModel) Contrato.jTable_seguro.getModel();
//            for (int j = 0; j < Contrato.jTable_seguro.getRowCount(); j++) {
//                dtm.removeRow(j);
//                j -= 1;
//            }
//
//            ArrayList<Object[]> data = new ArrayList<>();
//            while (rs.next()) {
//                Object[] rows = new Object[rsm.getColumnCount()];
//                for (int i = 0; i < rows.length; i++) {
//                    rows[i] = rs.getObject(i + 1).toString().trim();
//                }
//                data.add(rows);
//            }
//
//            dtm = (DefaultTableModel) Contrato.jTable_seguro.getModel();
//            for (int i = 0; i < data.size(); i++) {
//                dtm.addRow(data.get(i));
//            }
//
//        } catch (SQLException ex) {
//            MostrarError(ex, getNombreMetodo());
//        }
    }

    public static void Seguro_plan_jtable() {
//        try {
//            PreparedStatement ps = conexion.prepareStatement(""
//                    + "select id_plan, plan "
//                    + "from plan "
//                    + " ");
//            ResultSet rs = ps.executeQuery();
//            ResultSetMetaData rsm = rs.getMetaData();
//            DefaultTableModel dtm = (DefaultTableModel) Contrato.jTable_beneficiario1.getModel();
//            for (int j = 0; j < Contrato.jTable_beneficiario1.getRowCount(); j++) {
//                dtm.removeRow(j);
//                j -= 1;
//            }
//
//            ArrayList<Object[]> data = new ArrayList<>();
//            while (rs.next()) {
//                Object[] rows = new Object[rsm.getColumnCount()];
//                for (int i = 0; i < rows.length; i++) {
//                    rows[i] = rs.getObject(i + 1).toString().trim();
//                }
//                data.add(rows);
//            }
//
//            dtm = (DefaultTableModel) Contrato.jTable_beneficiario1.getModel();
//            for (int i = 0; i < data.size(); i++) {
//                dtm.addRow(data.get(i));
//            }
//
//        } catch (SQLException ex) {
//            MostrarError(ex, getNombreMetodo());
//        }
    }

    public static void Facturacion_beneficiarios_jtable(String buscar) {
//        try {
//
//            String sql = ""
//                    + "select beneficiario.id_beneficiario, nombre "
//                    + "from beneficiario "
//                    //                    + "inner join seguro_solidario on seguro_solidario.id_beneficiario = beneficiario.id_beneficiario "
//                    + "where nombre ilike '%" + buscar + "%'  "
//                    + "and beneficiario.id_beneficiario > '0' ";
//
//            if (isNumeric(buscar)) {
//                sql = ""
//                        + "select beneficiario.id_beneficiario, nombre, numero "
//                        + "from beneficiario "
//                        + "inner join seguro_solidario on seguro_solidario.id_beneficiario = beneficiario.id_beneficiario "
//                        + "where numero = '" + buscar + "' "
//                        + "and beneficiario.id_beneficiario > '0' ";
//            }
////            if (isNumeric(buscar)) {
////                sql = ""
////                        + "select beneficiario.id_beneficiario, nombre, numero "
////                        + "from beneficiario "
////                        + "inner join subbeneficiario on subbeneficiario.id_beneficiario = beneficiario.id_beneficiario "
////                        + "inner join seguro_solidario on seguro_solidario.id_beneficiario = beneficiario.id_beneficiario "
////                        + "where numero = '" + buscar + "' "
////                        + "and beneficiario.id_beneficiario > '0' ";
////            }
//
//            PreparedStatement ps = conexion.prepareStatement(sql);
//            ResultSet rs = ps.executeQuery();
//            ResultSetMetaData rsm = rs.getMetaData();
//            DefaultTableModel dtm = (DefaultTableModel) Facturacion_coronel_bogado.jTable_beneficiario.getModel();
//            for (int j = 0; j < Facturacion_coronel_bogado.jTable_beneficiario.getRowCount(); j++) {
//                dtm.removeRow(j);
//                j -= 1;
//            }
//
//            ArrayList<Object[]> data = new ArrayList<>();
//            while (rs.next()) {
//                Object[] rows = new Object[rsm.getColumnCount()];
//                for (int i = 0; i < rows.length; i++) {
//                    rows[i] = rs.getObject(i + 1).toString().trim();
//                }
//                data.add(rows);
//            }
//
//            dtm = (DefaultTableModel) Facturacion_coronel_bogado.jTable_beneficiario.getModel();
//            for (int i = 0; i < data.size(); i++) {
//                dtm.addRow(data.get(i));
//            }
//
//        } catch (SQLException ex) {
//            MostrarError(ex, getNombreMetodo());
//        }
    }

    public static void farmacia_social_buscar_jtable(String buscar) {
//        try {
//            PreparedStatement ps = conexion.prepareStatement(""
//                    + "select id_farmacia_social_producto, descripcion, precio "
//                    + "from farmacia_social_producto "
//                    + " where descripcion ilike '%" + buscar + "%' or codigo ilike '%" + buscar + "%' "
//            );
//            ResultSet rs = ps.executeQuery();
//            ResultSetMetaData rsm = rs.getMetaData();
//            DefaultTableModel dtm = (DefaultTableModel) Farmacia_social_productos.jTable_buscar.getModel();
//            for (int j = 0; j < Farmacia_social_productos.jTable_buscar.getRowCount(); j++) {
//                dtm.removeRow(j);
//                j -= 1;
//            }
//
//            ArrayList<Object[]> data = new ArrayList<>();
//            while (rs.next()) {
//                Object[] rows = new Object[rsm.getColumnCount()];
//                for (int i = 0; i < rows.length; i++) {
//                    rows[i] = rs.getObject(i + 1).toString().trim();
//                }
//                data.add(rows);
//            }
//
//            dtm = (DefaultTableModel) Farmacia_social_productos.jTable_buscar.getModel();
//            for (int i = 0; i < data.size(); i++) {
//                dtm.addRow(data.get(i));
//            }
//
//        } catch (SQLException ex) {
//            MostrarError(ex, getNombreMetodo());
//        }
    }

    public static void Acceso_usuario_jtable() {
//        try {
//            PreparedStatement ps = conexion.prepareStatement(""
//                    + "select id_usuario, nombre "
//                    + "from usuario "
//                    + " order by nombre");
//            ResultSet rs = ps.executeQuery();
//            ResultSetMetaData rsm = rs.getMetaData();
//            DefaultTableModel dtm = (DefaultTableModel) Acceso.jTable_usuario.getModel();
//            for (int j = 0; j < Acceso.jTable_usuario.getRowCount(); j++) {
//                dtm.removeRow(j);
//                j -= 1;
//            }
//
//            ArrayList<Object[]> data = new ArrayList<>();
//            while (rs.next()) {
//                Object[] rows = new Object[rsm.getColumnCount()];
//                for (int i = 0; i < rows.length; i++) {
//                    rows[i] = rs.getObject(i + 1).toString().trim();
//                }
//                data.add(rows);
//            }
//
//            dtm = (DefaultTableModel) Acceso.jTable_usuario.getModel();
//            for (int i = 0; i < data.size(); i++) {
//                dtm.addRow(data.get(i));
//            }
//
//        } catch (SQLException ex) {
//            MostrarError(ex, getNombreMetodo());
//        }
    }

    public static void Acceso_menu_jtable() {
//        try {
//            PreparedStatement ps = conexion.prepareStatement(""
//                    + "select id_menu, menu "
//                    + "from menu "
//                    + "order by menu");
//            ResultSet rs = ps.executeQuery();
//            ResultSetMetaData rsm = rs.getMetaData();
//            DefaultTableModel dtm = (DefaultTableModel) Acceso.jTable_menu.getModel();
//            for (int j = 0; j < Acceso.jTable_menu.getRowCount(); j++) {
//                dtm.removeRow(j);
//                j -= 1;
//            }
//
//            ArrayList<Object[]> data = new ArrayList<>();
//            while (rs.next()) {
//                Object[] rows = new Object[rsm.getColumnCount()];
//                for (int i = 0; i < rows.length; i++) {
//                    rows[i] = rs.getObject(i + 1).toString().trim();
//                }
//                data.add(rows);
//            }
//
//            dtm = (DefaultTableModel) Acceso.jTable_menu.getModel();
//            for (int i = 0; i < data.size(); i++) {
//                dtm.addRow(data.get(i));
//            }
//
//        } catch (SQLException ex) {
//            MostrarError(ex, getNombreMetodo());
//        }
    }

    public static void Beneficiarios_barrios_jtable() {
        try {
            PreparedStatement ps = conexion.prepareStatement(""
                    + "select id_barrio, barrio "
                    + "from barrio "
                    + "");
            ResultSet rs = ps.executeQuery();
            ResultSetMetaData rsm = rs.getMetaData();
            DefaultTableModel dtm = (DefaultTableModel) Beneficiarios.jTable_barrio.getModel();
            for (int j = 0; j < Beneficiarios.jTable_barrio.getRowCount(); j++) {
                dtm.removeRow(j);
                j -= 1;
            }

            ArrayList<Object[]> data = new ArrayList<>();
            while (rs.next()) {
                Object[] rows = new Object[rsm.getColumnCount()];
                for (int i = 0; i < rows.length; i++) {
                    rows[i] = rs.getObject(i + 1).toString().trim();
                }
                data.add(rows);
            }

            dtm = (DefaultTableModel) Beneficiarios.jTable_barrio.getModel();
            for (int i = 0; i < data.size(); i++) {
                dtm.addRow(data.get(i));
            }

        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }
    }

    public static void Beneficiarios_buscar_jtable(String buscar) {
        try {
            String sql = "";
            if (isNumeric(buscar)) {
                sql = "select id_beneficiario, nombre, ficha, documento "
                        + "from beneficiario "
                        + "where documento = '" + buscar + "' or ficha = '" + buscar + "' "
                        + "and id_beneficiario > '0' "
                        + "order by nombre ";
            } else {
                sql = "select id_beneficiario, nombre, ficha, documento "
                        + "from beneficiario "
                        + "where nombre ilike '%" + buscar + "%' "
                        + "and id_beneficiario > '0' "
                        + "order by nombre ";
            }

            PreparedStatement ps = conexion.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            ResultSetMetaData rsm = rs.getMetaData();
            DefaultTableModel dtm = (DefaultTableModel) Beneficiarios.jTable_buscar.getModel();
            for (int j = 0; j < Beneficiarios.jTable_buscar.getRowCount(); j++) {
                dtm.removeRow(j);
                j -= 1;
            }

            ArrayList<Object[]> data = new ArrayList<>();
            while (rs.next()) {
                Object[] rows = new Object[rsm.getColumnCount()];
                for (int i = 0; i < rows.length; i++) {
                    rows[i] = rs.getObject(i + 1).toString().toUpperCase().trim();
                }
                data.add(rows);
            }

            dtm = (DefaultTableModel) Beneficiarios.jTable_buscar.getModel();
            for (int i = 0; i < data.size(); i++) {
                dtm.addRow(data.get(i));
            }

        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }
    }

    public static void Barrio_guardar(String barrio) {
        try {
            Statement st = conexion.createStatement();
            ResultSet rs = st.executeQuery("SELECT MAX(id_barrio) FROM barrio");
            if (rs.next()) {
                id_barrio = rs.getInt(1) + 1;
            }

            PreparedStatement stReporteCaja = conexion.prepareStatement("INSERT INTO barrio VALUES(?,?)");
            stReporteCaja.setInt(1, id_barrio);
            stReporteCaja.setString(2, barrio);
            stReporteCaja.executeUpdate();

            JOptionPane.showMessageDialog(null, "Guardado correctamente.");

        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }
    }

    public static void Configuraciones_guardar(String nombre, String ruc, String telefono, String direccion) {
        try {

            PreparedStatement stUpdateAuxiliar4 = conexion.prepareStatement(""
                    + "UPDATE configuracion "
                    + "SET nombre = '" + nombre + "', "
                    + "ruc = '" + ruc + "', "
                    + "telefono = '" + telefono + "', "
                    + "direccion = '" + direccion + "' "
                    + "WHERE id_configuracion = '1'");
            stUpdateAuxiliar4.executeUpdate();

            institucion = nombre;
            institucion_ruc = ruc;
            institucion_telefono = telefono;
            institucion_direccion = direccion;

            JOptionPane.showMessageDialog(null, "Actualizado correctamente.");

        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }
    }

    public static void Auditoria_guardar(String accion, String tabla, String descripcion) {
        try {
            Statement st = conexion.createStatement();
            ResultSet rs = st.executeQuery("SELECT MAX(id_auditoria) FROM auditoria");
            if (rs.next()) {
                id_auditoria = rs.getInt(1) + 1;
            }

            DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            Date today = Calendar.getInstance().getTime();
            String ahora = df.format(today);

            PreparedStatement stReporteCaja = conexion.prepareStatement("INSERT INTO auditoria VALUES(?,?,?,?,?,?)");
            stReporteCaja.setInt(1, id_auditoria);
            stReporteCaja.setString(2, ahora);
            stReporteCaja.setString(3, usuario_activo);
            stReporteCaja.setString(4, accion);
            stReporteCaja.setString(5, tabla);
            stReporteCaja.setString(6, descripcion);
            stReporteCaja.executeUpdate();

        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }
    }

    public static void Farmacia_social_facturacion_max() {
        try {
            Statement st = conexion.createStatement();
            ResultSet rs = st.executeQuery("SELECT MAX(id_fs_facturacion) FROM fs_facturacion ");
            if (rs.next()) {
                id_fs_facturacion = rs.getInt(1);
            }

        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }
    }

    public static void Facturacion_guardar_detalle(String precio, String unidades) {
        try {
            long precio_long = Long.parseLong(precio);
            servicios_utilizados = 0;
            porcentaje = 0;
            cantidad = 0;

            Date fecha_pago = null;
            Date vencimiento = null;

            Statement st = conexion.createStatement();
            ResultSet rs = st.executeQuery(""
                    + "SELECT * FROM facturacion "
                    + "where id_facturacion = '" + id_facturacion + "'");
            if (rs.next()) {

                if (rs.getInt("id_estado") == 2) {
                    JOptionPane.showMessageDialog(null, "Factura impresa. No se puede modificar.");
                } else {

                    st = conexion.createStatement();
                    rs = st.executeQuery("SELECT MAX(id_facturacion_detalle) FROM facturacion_detalle");
                    if (rs.next()) {
                        id_facturacion_detalle = rs.getInt(1) + 1;
                    }
                    int iva = 0;
                    int bloqueo = 0;
                    String nombre = "";
                    String factura = "";
                    long producto_precio = 0;
                    //obtenemos la descripcion del producto
                    st = conexion.createStatement();
                    rs = st.executeQuery(""
                            + "SELECT * FROM producto "
                            + "where id_producto = '" + facturacion_id_producto + "'");
                    if (rs.next()) {
                        iva = rs.getInt("iva");
                        producto_id_tipo = rs.getInt("id_tipo");
                        producto_precio = rs.getLong("precio");
                        nombre = rs.getString("descripcion").trim();
                        bloqueo = rs.getInt("bloquear");

                    }
                    System.err.println("Producto: " + facturacion_id_producto + " " + nombre);
                    boolean encontrado = false;

                    //obtenemos el id del plan    
                    int facturacion_id_plan = 0;
                    int facturacion_id_beneficiario_del_subbeneficiario = 0;
                    st = conexion.createStatement();
                    rs = st.executeQuery(""
                            + "SELECT * FROM subbeneficiario "
                            + "where subbeneficiario = '" + facturacion_id_beneficiario + "'");
                    if (rs.next()) {
                        facturacion_id_beneficiario_del_subbeneficiario = rs.getInt("id_beneficiario");
                    }

                    st = conexion.createStatement();
                    rs = st.executeQuery(""
                            + "SELECT * FROM seguro_solidario "
                            + "inner join seguro_pagos on seguro_pagos.id_seguro = seguro_solidario.id_seguro "
                            + "where id_beneficiario = '" + facturacion_id_beneficiario + "' order by fecha DESC");
                    if (rs.next()) {
                        facturacion_id_plan = rs.getInt("id_plan");
                        fecha_pago = rs.getDate("fecha");
                        encontrado = true;
                    }

                    st = conexion.createStatement();
                    rs = st.executeQuery(""
                            + "SELECT * FROM mensualidad "
                            + "inner join seguro_solidario on seguro_solidario.id_seguro = mensualidad.id_seguro  "
                            + "where id_beneficiario = '" + facturacion_id_beneficiario + "' "
                            + "and id_mensualidad_estado = '1' "
                            + "order by vencimiento ASC");
                    if (rs.next()) {
                        facturacion_id_plan = rs.getInt("id_plan");
                        vencimiento = rs.getDate("fecha");
                        encontrado = true;
                    }

                    if (encontrado == false) {
                        st = conexion.createStatement();
                        rs = st.executeQuery(""
                                + "SELECT * FROM seguro_solidario "
                                + "inner join seguro_pagos on seguro_pagos.id_seguro = seguro_solidario.id_seguro "
                                + "where id_beneficiario = '" + facturacion_id_beneficiario_del_subbeneficiario + "' order by fecha DESC");
                        if (rs.next()) {
                            facturacion_id_plan = rs.getInt("id_plan");
                            fecha_pago = rs.getDate("fecha");
                            encontrado = true;
                        }
                    }

                    if (encontrado == false) {
                        st = conexion.createStatement();
                        rs = st.executeQuery(""
                                + "SELECT * FROM mensualidad "
                                + "inner join seguro_solidario on seguro_solidario.id_seguro = mensualidad.id_seguro  "
                                + "where id_beneficiario = '" + facturacion_id_beneficiario_del_subbeneficiario + "' "
                                + "and id_mensualidad_estado = '1' "
                                + "order by vencimiento ASC");
                        if (rs.next()) {
                            facturacion_id_plan = rs.getInt("id_plan");
                            vencimiento = rs.getDate("fecha");
                            encontrado = true;
                        }
                    }

                    if (bloqueo == 0) {
                        fecha_pago = new Date();
                        encontrado = true;
                        cantidad = 10;
                    }

                    Calendar fecha_pago_menos_1 = Calendar.getInstance();
                    fecha_pago_menos_1.setTime(fecha_pago);
                    fecha_pago_menos_1.add(Calendar.MONTH, 1);
                    fecha_pago_menos_1.getTime();

                    Calendar fecha_pago_calendar = Calendar.getInstance();
                    fecha_pago_calendar.setTime(fecha_pago);

                    Date pago_menos_1 = fecha_pago_menos_1.getTime();
                    Date hoy = new Date();

                    int dias_de_pago = (int) ((hoy.getTime() - pago_menos_1.getTime()) / 86400000);

//                    System.err.println("Dias:" + da);
                    if (dias_de_pago < 0) {

                        if (encontrado == true) {
                            // si no encontro el beneficiario buscamos el subbeneficiario
                            // buscamos al beneficiario del seguro
                            st = conexion.createStatement();
                            rs = st.executeQuery(""
                                    + "SELECT * FROM subbeneficiario "
                                    + "where subbeneficiario = '" + facturacion_id_beneficiario + "' ");
                            if (rs.next()) {
                                //obtenemos los datos
                                int beneficiario = rs.getInt("id_beneficiario");
                                facturacion_id_plan = 0;
                                st = conexion.createStatement();
                                rs = st.executeQuery(""
                                        + "SELECT * FROM seguro_solidario "
                                        + "inner join seguro_pagos on seguro_pagos.id_seguro = seguro_solidario.id_seguro "
                                        + "where id_beneficiario = '" + beneficiario + "' order by fecha DESC");
                                if (rs.next()) {
                                    facturacion_id_plan = rs.getInt("id_plan");
                                    fecha_pago = rs.getDate("fecha");
                                }
                            }

                            //obtenemos el descuento del producto segun el plan
                            porcentaje = 0;
                            cantidad = 0;
                            st = conexion.createStatement();
                            rs = st.executeQuery(""
                                    + "SELECT * FROM descuento "
                                    + "where id_producto = '" + facturacion_id_producto + "' "
                                    + "and id_plan = '" + facturacion_id_plan + "'");
                            if (rs.next()) {
                                porcentaje = rs.getInt("porcentaje");
                                cantidad = rs.getInt("cantidad");
                            }

                            System.err.println("descuento: " + cantidad);

                            //obtenemos la cantidad utilizada en el mes
                            if (cantidad > 0) {

                                servicios_utilizados = 0;

                                System.out.println(fecha_pago);

                                DateFormat df_day = new SimpleDateFormat("dd");
                                DateFormat df_month = new SimpleDateFormat("MM");
                                DateFormat df_year = new SimpleDateFormat("yyyy");

                                int anho = Integer.valueOf(df_year.format(fecha_pago));
                                int mes = Integer.valueOf(df_month.format(fecha_pago));
                                int dias = Integer.valueOf(df_day.format(fecha_pago));

                                System.err.println(dias + " " + mes + " " + anho);

                                String sql = ""
                                        + "SELECT * FROM facturacion "
                                        + "inner join facturacion_detalle on facturacion_detalle.id_facturacion = facturacion.id_facturacion "
                                        + "where id_beneficiario = '" + facturacion_id_beneficiario + "'  "
                                        + "and id_estado = '0' "
                                        + "and fecha >= '" + anho + "-" + mes + "-" + dias + "' "
                                        + "and fecha <= '" + anho + "-" + (mes + 1) + "-" + dias + "' "
                                        + "and factura > '0' "
                                        + "and id_producto = '" + facturacion_id_producto + "' ";

                                System.err.println(sql);
                                st = conexion.createStatement();
                                rs = st.executeQuery(sql);
                                while (rs.next()) {
                                    servicios_utilizados = servicios_utilizados + 1;
                                    System.err.println(servicios_utilizados + "-");
                                }

                                if (servicios_utilizados < cantidad) {
//                                    precio_long = 0;
                                }

                            }

                            st = conexion.createStatement();
                            rs = st.executeQuery("SELECT * FROM facturacion where id_facturacion = '" + id_facturacion + "'");
                            if (rs.next()) {
                                factura = rs.getString("factura").trim();
                            }

                            PreparedStatement stReporteCaja = conexion.prepareStatement("INSERT INTO facturacion_detalle VALUES(?,?,?,?,?,?,?,?,?,?)");
                            stReporteCaja.setInt(1, id_facturacion_detalle);
                            stReporteCaja.setInt(2, id_facturacion);
                            stReporteCaja.setInt(3, facturacion_id_producto);
                            stReporteCaja.setLong(4, precio_long);
                            stReporteCaja.setLong(5, Long.parseLong(unidades));
                            long total_iva5 = 0;
                            long total_iva10 = 0;
                            if (iva == 0) {
                                stReporteCaja.setLong(6, Long.parseLong(unidades) * precio_long);
                            } else {
                                stReporteCaja.setLong(6, 0);
                            }
                            if (iva == 5) {
                                stReporteCaja.setLong(7, Long.parseLong(unidades) * precio_long);
                                total_iva5 = Long.parseLong(unidades) * precio_long / 21;
                            } else {
                                stReporteCaja.setLong(7, 0);
                            }
                            if (iva == 10) {
                                stReporteCaja.setLong(8, Long.parseLong(unidades) * precio_long);
                                total_iva10 = Long.parseLong(unidades) * precio_long / 11;
                            } else {
                                stReporteCaja.setLong(8, 0);
                            }
                            stReporteCaja.setLong(9, total_iva5);
                            stReporteCaja.setLong(10, total_iva10);
                            stReporteCaja.executeUpdate();
                            Code.Auditoria_guardar("AGREGAR DETALLE", "FACTURA", factura + " " + nombre + " " + precio_long + " " + unidades);
                        } else {
                            JOptionPane.showMessageDialog(null, "Problemas con el aporte del beneficiario.");
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "El beneficiario no esta al dia con el pago de sus cuotas.");
                    }
                    if (bloqueo == 0) {

                        cantidad = 10;
                    }

                }
            }

        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }
    }

    public static int producto_iva = 0;
    public static long producto_precio = 0;
    public static String producto_nombre = "";

    public static void Producto_informacion(int id_producto) {
        try {
            Statement st = conexion.createStatement();
            ResultSet rs = st.executeQuery(""
                    + "SELECT * FROM producto "
                    + "where id_producto = '" + facturacion_id_producto + "'");
            if (rs.next()) {
                producto_iva = rs.getInt("iva");
                producto_precio = rs.getLong("precio");
                producto_nombre = rs.getString("descripcion").trim();
            }
        } catch (SQLException ex) {
            System.out.println(ex);
        }
    }

    public static Date fecha_nueva_mes(Date fecha, int dias) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(fecha);
        cal.add(Calendar.MONTH, dias);
        return cal.getTime();
    }

    public static void Facturacion_guardar_detalle2(String precio, String unidades) {
        try {

            long precio_long = Long.parseLong(precio);
            servicios_utilizados = 0;
            porcentaje = 0;
            cantidad = 0;
            int facturacion_id_plan = 0;

            Date vencimiento = null;

            Statement st = conexion.createStatement();
            ResultSet rs_facturacion = st.executeQuery(""
                    + "SELECT * FROM facturacion "
                    + "where id_facturacion = '" + id_facturacion + "' "
                    + "and id_estado != '2' ");
            if (rs_facturacion.next()) {

                //obtenemos la descripcion del producto
                Producto_informacion(facturacion_id_producto);

                st = conexion.createStatement();

                ResultSet rs = st.executeQuery(""
                        + "SELECT * FROM mensualidad "
                        + "inner join seguro_solidario on seguro_solidario.id_seguro = mensualidad.id_seguro  "
                        + "inner join seguro_beneficiario on seguro_beneficiario.id_seguro = mensualidad.id_seguro  "
                        + "where seguro_beneficiario.id_beneficiario = '" + facturacion_id_beneficiario + "' "
                        + "and id_mensualidad_estado = '1' "
                        + "order by vencimiento ASC");
                if (rs.next()) {
                    facturacion_id_plan = rs.getInt("id_plan");
                    vencimiento = rs.getDate("vencimiento");
                }

                Date hoy = new Date();

                if (vencimiento != null) {

                    if (hoy.after(vencimiento)) {
                        JOptionPane.showMessageDialog(null, "Verificar el pago de la mensualidad");
                    } else {

                        //obtenemos el descuento del producto segun el plan
                        porcentaje = 0;
                        cantidad = 0;
                        frecuencia = 0;
                        st = conexion.createStatement();
                        rs = st.executeQuery(""
                                + "SELECT * FROM descuento "
                                + "where id_producto = '" + facturacion_id_producto + "' "
                                + "and id_plan = '" + facturacion_id_plan + "'");
                        if (rs.next()) {
                            porcentaje = rs.getInt("porcentaje");
                            cantidad = rs.getInt("cantidad");
                            frecuencia = rs.getInt("id_frecuencia");
                        }

                        //obtenemos la cantidad utilizada en el mes
                        if (cantidad > 0) {
                            servicios_utilizados = 0;

                            Date fecha_menos_uno = null;
                            if (frecuencia == 1) {
                                fecha_menos_uno = fecha_nueva_mes(vencimiento, -1);
                            }

                            if (frecuencia == 2) {
                                fecha_menos_uno = fecha_nueva_mes(vencimiento, -6);
                            }

                            if (frecuencia == 3) {
                                fecha_menos_uno = fecha_nueva_mes(vencimiento, -12);
                            }

                            String sql = ""
                                    + "SELECT * FROM facturacion "
                                    + "inner join facturacion_detalle on facturacion_detalle.id_facturacion = facturacion.id_facturacion "
                                    + "where id_beneficiario = '" + facturacion_id_beneficiario + "'  "
                                    + "and id_estado = '0' "
                                    + "and fecha >= '" + fecha_menos_uno + "' "
                                    + "and fecha <= '" + vencimiento + "' "
                                    + "and factura > '0' "
                                    + "and id_producto = '" + facturacion_id_producto + "' ";

                            System.err.println(sql);
                            st = conexion.createStatement();
                            rs = st.executeQuery(sql);
                            while (rs.next()) {
                                servicios_utilizados = servicios_utilizados + 1;
                            }
                        }

                        if (servicios_utilizados < cantidad) {

                            String factura = rs_facturacion.getString("factura").trim();

                            PreparedStatement stReporteCaja = conexion.prepareStatement("INSERT INTO facturacion_detalle VALUES(?,?,?,?,?,?,?,?,?,?)");
                            stReporteCaja.setInt(1, MAX_tabla("id_facturacion_detalle", "facturacion_detalle"));
                            stReporteCaja.setInt(2, id_facturacion);
                            stReporteCaja.setInt(3, facturacion_id_producto);
                            stReporteCaja.setLong(4, precio_long);
                            stReporteCaja.setLong(5, Long.parseLong(unidades));
                            long total_iva5 = 0;
                            long total_iva10 = 0;
                            if (producto_iva == 0) {
                                stReporteCaja.setLong(6, Long.parseLong(unidades) * precio_long);
                            } else {
                                stReporteCaja.setLong(6, 0);
                            }
                            if (producto_iva == 5) {
                                stReporteCaja.setLong(7, Long.parseLong(unidades) * precio_long);
                                total_iva5 = Long.parseLong(unidades) * precio_long / 21;
                            } else {
                                stReporteCaja.setLong(7, 0);
                            }
                            if (producto_iva == 10) {
                                stReporteCaja.setLong(8, Long.parseLong(unidades) * precio_long);
                                total_iva10 = Long.parseLong(unidades) * precio_long / 11;
                            } else {
                                stReporteCaja.setLong(8, 0);
                            }
                            stReporteCaja.setLong(9, total_iva5);
                            stReporteCaja.setLong(10, total_iva10);
                            stReporteCaja.executeUpdate();
                            Code.Auditoria_guardar("AGREGAR DETALLE", "FACTURA", factura + " " + producto_nombre + " " + precio_long + " " + unidades);
                        } else {
                            JOptionPane.showMessageDialog(null, "El beneficiario ya utilizo el máximo permitido para este servicio");
                        }
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Verificar el pago de la mensualidad");
                }
            }
        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }
    }

    public static void Usuario_guardar(String nombre, String usuario) {
//        try {
//
//            char[] arrayC = Usuarios.jPasswordField1.getPassword();
//            String pass = new String(arrayC);
//
//            Statement st = conexion.createStatement();
//            ResultSet rs = st.executeQuery("SELECT MAX(id_usuario) FROM usuario");
//            if (rs.next()) {
//                id_usuario = rs.getInt(1) + 1;
//            }
//
//            PreparedStatement stReporteCaja = conexion.prepareStatement("INSERT INTO usuario VALUES(?,?,?,?)");
//            stReporteCaja.setInt(1, id_usuario);
//            stReporteCaja.setString(2, nombre);
//            stReporteCaja.setString(3, usuario);
//            stReporteCaja.setString(4, pass);
//
//            stReporteCaja.executeUpdate();
//
//            JOptionPane.showMessageDialog(null, "Guardado correctamente.");
//
//        } catch (SQLException ex) {
//            MostrarError(ex, getNombreMetodo());
//        }
    }

    public static void Beneficiario_traer_datos() {
        try {
            Statement st = conexion.createStatement();
            ResultSet rs = st.executeQuery(""
                    + "SELECT * "
                    + "FROM beneficiario "
                    + "inner join barrio on barrio.id_barrio = beneficiario.id_barrio "
                    + "inner join sexo on sexo.id_sexo = beneficiario.id_sexo "
                    + "inner join grupo_sanguineo on grupo_sanguineo.id_grupo_sanguineo = beneficiario.id_grupo_sanguineo "
                    + "where id_beneficiario = '" + id_beneficiario + "'");
            if (rs.next()) {
                beneficiarios_id_barrio = rs.getInt("id_barrio");
                beneficiarios_id_sexo = rs.getInt("id_sexo");
                beneficiarios_id_grupo_sanguineo = rs.getInt("id_grupo_sanguineo");
                Beneficiarios.jTextField_barrio.setText(rs.getString("barrio"));
                Beneficiarios.jTextField_sexo.setText(rs.getString("sexo"));
//                Beneficiarios.jTextField_grupo_sanguineo.setText(rs.getString("grupo_sanguineo"));
                Beneficiarios.jTextField_ci.setText(rs.getString("documento"));
                Beneficiarios.jTextField_direccion.setText(rs.getString("direccion"));
                Beneficiarios.jTextField_nombre.setText(rs.getString("nombre"));
                Beneficiarios.jTextField_telefono.setText(rs.getString("telefono"));
                Beneficiarios.jTextField_ficha.setText(rs.getString("ficha"));
                Beneficiarios.jDateChooser_fecha.setDate(rs.getDate("nacimiento"));
            }
        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }
    }

    public static void Farmacia_social_ventas_traer_datos() {
//        try {
//            Statement st = conexion.createStatement();
//            ResultSet rs = st.executeQuery(""
//                    + "SELECT *, facturacion_seguro.seguro as fseguro "
//                    + "FROM fs_facturacion "
//                    + "inner join beneficiario on beneficiario.id_beneficiario = fs_facturacion.id_beneficiario "
//                    + "inner join facturacion_seguro on facturacion_seguro.id_facturacion_seguro = fs_facturacion.id_facturacion_seguro "
//                    + "where id_fs_facturacion = '" + id_fs_facturacion + "'");
//            if (rs.next()) {
//                fs_facturacion_id_beneficiario = rs.getInt("id_beneficiario");
//                Farmacia_social_ventas.jt_beneficiario.setText(rs.getString("nombre"));
//                Farmacia_social_ventas.jTextField_total.setText(SepararMiles(rs.getString("total")));
//                Farmacia_social_ventas.jt_factura.setText(rs.getString("factura"));
//                Farmacia_social_ventas.jDateChooser3.setDate(rs.getDate("fecha"));
//                Farmacia_social_ventas.jTextField_seguro.setText(rs.getString("fseguro"));
//            }
//        } catch (SQLException ex) {
//            MostrarError(ex, getNombreMetodo());
//        }
    }

    public static void Configuraciones_traer_datos() {
//        try {
//            Statement st = conexion.createStatement();
//            ResultSet rs = st.executeQuery(""
//                    + "SELECT * FROM configuracion ");
//            if (rs.next()) {
//                Configuraciones.jTextField_nombre.setText(rs.getString("nombre"));
//                Configuraciones.jTextField_ruc.setText(rs.getString("ruc"));
//                Configuraciones.jTextField_telefono.setText(rs.getString("telefono"));
//                Configuraciones.jTextField_direccion.setText(rs.getString("direccion"));
//            }
//        } catch (SQLException ex) {
//            MostrarError(ex, getNombreMetodo());
//        }
    }

    public static void Productos_traer_datos() {
//        try {
//            Statement st = conexion.createStatement();
//            ResultSet rs = st.executeQuery(""
//                    + "SELECT * "
//                    + "FROM producto "
////                    + "inner join tipo on tipo.id_tipo = producto.id_tipo "
//                    + "where id_producto = '" + id_producto + "'");
//            if (rs.next()) {
//                Servicios.jTextField_descripcion.setText(rs.getString("descripcion"));
//                Servicios.jTextField_precio.setText(rs.getString("precio"));
//                Servicios.jTextField_iva.setText(rs.getString("iva"));
////                Servicios.jTextField_tipo.setText(rs.getString("tipo"));
//
////                if (rs.getInt("bloquear") == 1) {
////                    Servicios.jCheckBox1.setSelected(true);
////                } else {
////                    Servicios.jCheckBox1.setSelected(false);
////                }
//            }
//        } catch (SQLException ex) {
//            MostrarError(ex, getNombreMetodo());
//        }
    }

    public static void Farmacia_social_traer_datos() {
//        try {
//            Statement st = conexion.createStatement();
//            ResultSet rs = st.executeQuery(""
//                    + "SELECT * "
//                    + "FROM farmacia_social_producto "
//                    + "where id_farmacia_social_producto = '" + farmacia_social_producto_id_producto + "'");
//            if (rs.next()) {
//                Farmacia_social_productos.jTextField_codigo.setText(rs.getString("codigo"));
//                Farmacia_social_productos.jTextField_descripcion.setText(rs.getString("descripcion"));
//                Farmacia_social_productos.jTextField_precio.setText(rs.getString("precio"));
//                Farmacia_social_productos.jTextField_recargo.setText(rs.getString("recargo"));
//            }
//        } catch (SQLException ex) {
//            MostrarError(ex, getNombreMetodo());
//        }
    }

    public synchronized static boolean Verificar_usuario() {

        boolean resultado = false;
        try {

            hoy = new Date();
            String nombre = Logueo.jTextField_nombre.getText();
            char[] arrayC = Logueo.jPasswordField1.getPassword();
            String pass = new String(arrayC);
            String sql = "select * from usuario where usuario = '" + nombre + "' and contrasena = '" + pass + "'";
            Statement st1 = conexion.createStatement();
            ResultSet result = st1.executeQuery(sql);
            if (result.next()) {
                resultado = true;
                usuario_activo = result.getString("nombre").trim();
                id_usuario = result.getInt("id_usuario");
            }

            sql = "select * from configuracion";
            st1 = conexion.createStatement();
            result = st1.executeQuery(sql);
            if (result.next()) {
                institucion = result.getString("nombre").trim();
                institucion_ruc = result.getString("ruc").trim();
                institucion_direccion = result.getString("direccion").trim();
                institucion_telefono = result.getString("telefono").trim();
            }
        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }
        return resultado;
    }

    public synchronized static java.sql.Date util_Date_to_sql_date(Date fecha) {
        java.sql.Date fecha_sql_date = null;
        if (fecha != null) {
            java.util.Date utilDate = fecha;
            fecha_sql_date = new java.sql.Date(utilDate.getTime());
        }
        return fecha_sql_date;
    }

    public static void Beneficiario_guardar(String nombre, String documento, String direccion, Date fecha,
            String telefono, String ficha) {
        try {
            boolean existe = false;
            int telefono_long = 0;
            if (isNumeric(telefono)) {
            } else {
                JOptionPane.showMessageDialog(null, "ERROR: Numero de telefono");

            }

            if (telefono.length() > 0) {
                telefono_long = Integer.parseInt(telefono);
            }
            long documento_long = Long.parseLong(documento);
            long ficha_long = Long.parseLong(ficha);
            String nombre_duplicado = "";

            Statement st = conexion.createStatement();
            ResultSet rs = st.executeQuery("SELECT MAX(id_beneficiario) FROM beneficiario");
            if (rs.next()) {
                id_beneficiario = rs.getInt(1) + 1;
            }
            String mensaje = "";
//            st = conexion.createStatement();
//            rs = st.executeQuery("SELECT * FROM beneficiario where ficha = '" + ficha_long + "'");
//            if (rs.next()) {
//                existe = true;
//                mensaje = "Ficha ya existe.";
//            }

            st = conexion.createStatement();
            rs = st.executeQuery("SELECT * FROM beneficiario where documento = '" + documento_long + "'");
            if (rs.next()) {
                existe = true;
                nombre = rs.getString("nombre").trim();
                mensaje = mensaje + " CI duplicada. " + nombre;
            }
            if (existe == false) {
                PreparedStatement stReporteCaja = conexion.prepareStatement("INSERT INTO beneficiario VALUES(?,?,?,?,?,?,?,?,?,?)");
                stReporteCaja.setInt(1, id_beneficiario);
                stReporteCaja.setString(2, nombre);
                stReporteCaja.setLong(3, documento_long);
                stReporteCaja.setString(4, direccion);
                stReporteCaja.setDate(5, util_Date_to_sql_date(fecha));
                stReporteCaja.setLong(6, telefono_long);
                stReporteCaja.setInt(7, beneficiarios_id_barrio);
                stReporteCaja.setInt(8, beneficiarios_id_sexo);
                stReporteCaja.setInt(9, beneficiarios_id_grupo_sanguineo);
                stReporteCaja.setLong(10, ficha_long);
                stReporteCaja.executeUpdate();

                JOptionPane.showMessageDialog(null, "Guardado correctamente.");
            } else {
                JOptionPane.showMessageDialog(null, mensaje);
            }

        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }
    }

    public static void Beneficiario_grupo_sanguineo_jtable() {
        try {
            PreparedStatement ps = conexion.prepareStatement(""
                    + "select id_grupo_sanguineo, grupo_sanguineo "
                    + "from grupo_sanguineo "
                    + "");
            ResultSet rs = ps.executeQuery();
            ResultSetMetaData rsm = rs.getMetaData();
            DefaultTableModel dtm = (DefaultTableModel) Beneficiarios.jTable_grupo_sanguineo.getModel();
            for (int j = 0; j < Beneficiarios.jTable_grupo_sanguineo.getRowCount(); j++) {
                dtm.removeRow(j);
                j -= 1;
            }

            ArrayList<Object[]> data = new ArrayList<>();
            while (rs.next()) {
                Object[] rows = new Object[rsm.getColumnCount()];
                for (int i = 0; i < rows.length; i++) {
                    rows[i] = rs.getObject(i + 1).toString().trim();
                }
                data.add(rows);
            }

            dtm = (DefaultTableModel) Beneficiarios.jTable_grupo_sanguineo.getModel();
            for (int i = 0; i < data.size(); i++) {
                dtm.addRow(data.get(i));
            }

        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }
    }

    public synchronized static void Beneficiario_sexo_selected() {
        DefaultTableModel tm = (DefaultTableModel) Beneficiarios.jTable_sexo.getModel();
        beneficiarios_id_sexo = Integer.parseInt(String.valueOf(tm.getValueAt(Beneficiarios.jTable_sexo.getSelectedRow(), 0)));
        Beneficiarios.jTextField_sexo.setText(String.valueOf(tm.getValueAt(Beneficiarios.jTable_sexo.getSelectedRow(), 1)));
    }

    public synchronized static void Mensualidad_selected() {
        DefaultTableModel tm = (DefaultTableModel) Pago_mensualidad.jTable_detalle.getModel();
        id_mensualidad = Integer.parseInt(String.valueOf(tm.getValueAt(Pago_mensualidad.jTable_detalle.getSelectedRow(), 0)));
    }

    public synchronized static void Contrato_beneficiario_selected() {
//        DefaultTableModel tm = (DefaultTableModel) Contrato.jTable_beneficiarios.getModel();
//        contrato_id_titular = Integer.parseInt(String.valueOf(tm.getValueAt(Contrato.jTable_beneficiarios.getSelectedRow(), 0)));
    }

    public synchronized static void Mensualidad_beneficiario_selected() {
        DefaultTableModel tm = (DefaultTableModel) Pago_mensualidad.jTable_beneficiario.getModel();
        mensualidad_id_beneficiario = Integer.parseInt(String.valueOf(tm.getValueAt(Pago_mensualidad.jTable_beneficiario.getSelectedRow(), 0)));
        Pago_mensualidad.jTextField_beneficiario.setText(String.valueOf(tm.getValueAt(Pago_mensualidad.jTable_beneficiario.getSelectedRow(), 1)));
    }

    public synchronized static void Productos_descuento_selected() {
//        DefaultTableModel tm = (DefaultTableModel) Servicios.jTable_descuento.getModel();
//        producto_id_descuento = Integer.parseInt(String.valueOf(tm.getValueAt(Servicios.jTable_descuento.getSelectedRow(), 0)));
    }

    public synchronized static void Productos_frecuencia_selected() {
//        DefaultTableModel tm = (DefaultTableModel) Servicios.jTable_frecuencia.getModel();
//        producto_id_frecuencia = Integer.parseInt(String.valueOf(tm.getValueAt(Servicios.jTable_frecuencia.getSelectedRow(), 0)));
//        Servicios.jTextField_frecuencia.setText(String.valueOf(tm.getValueAt(Servicios.jTable_frecuencia.getSelectedRow(), 1)));
    }

    public synchronized static void Seguro_selected() {
//        DefaultTableModel tm = (DefaultTableModel) Contrato.jTable_seguro.getModel();
//        id_seguro_solidario = Integer.parseInt(String.valueOf(tm.getValueAt(Contrato.jTable_seguro.getSelectedRow(), 0)));

    }

    public synchronized static void Beneficiario_subbeneficiario_selected() {
        DefaultTableModel tm = (DefaultTableModel) Beneficiarios.jTable_buscar_subbeneficiario.getModel();
        beneficiarios_id_sub_beneficiario = Integer.parseInt(String.valueOf(tm.getValueAt(Beneficiarios.jTable_buscar_subbeneficiario.getSelectedRow(), 0)));
    }

    public synchronized static void Productos_plan_selected() {
//        DefaultTableModel tm = (DefaultTableModel) Servicios.jTable_plan.getModel();
//        productos_id_plan = Integer.parseInt(String.valueOf(tm.getValueAt(Servicios.jTable_plan.getSelectedRow(), 0)));
//        Servicios.jTextField_plan.setText(String.valueOf(tm.getValueAt(Servicios.jTable_plan.getSelectedRow(), 1)));
    }

    public synchronized static void Vencimientos_productos_selected() {
//        DefaultTableModel tm = (DefaultTableModel) Vencimientos.jTable_productos.getModel();
//        vencimiento_id_producto = Integer.parseInt(String.valueOf(tm.getValueAt(Vencimientos.jTable_productos.getSelectedRow(), 0)));
//        Vencimientos.jTextField_producto.setText(String.valueOf(tm.getValueAt(Vencimientos.jTable_productos.getSelectedRow(), 1)));
    }

    public synchronized static void Farmacia_social_ventas_buscar_selected() {
//        DefaultTableModel tm = (DefaultTableModel) Farmacia_social_ventas.jTable_buscar.getModel();
//        id_fs_facturacion = Integer.parseInt(String.valueOf(tm.getValueAt(Farmacia_social_ventas.jTable_buscar.getSelectedRow(), 0)));
    }

    public synchronized static void Farmacia_social_facturacion_detalle_selected() {
//        DefaultTableModel tm = (DefaultTableModel) Farmacia_social_ventas.jTable_detalle.getModel();
//        fs_facturacion_detalle_id = Integer.parseInt(String.valueOf(tm.getValueAt(Farmacia_social_ventas.jTable_detalle.getSelectedRow(), 0)));

    }

    public synchronized static void Farmacia_social_ventas_beneficiario_selected() {
//        DefaultTableModel tm = (DefaultTableModel) Farmacia_social_ventas.jTable_beneficiario.getModel();
//        fs_facturacion_id_beneficiario = Integer.parseInt(String.valueOf(tm.getValueAt(Farmacia_social_ventas.jTable_beneficiario.getSelectedRow(), 0)));
//        Farmacia_social_ventas.jt_beneficiario.setText(String.valueOf(tm.getValueAt(Farmacia_social_ventas.jTable_beneficiario.getSelectedRow(), 1)));
    }

    public synchronized static void Acceso_selected() {
//        DefaultTableModel tm = (DefaultTableModel) Acceso.jTable_acceso.getModel();
//        id_acceso = Integer.parseInt(String.valueOf(tm.getValueAt(Acceso.jTable_acceso.getSelectedRow(), 0)));

    }

    public synchronized static void Acceso_detalle_selected() {
//        DefaultTableModel tm = (DefaultTableModel) Acceso.jTable_acceso_detalle.getModel();
//        id_detalle_acceso = Integer.parseInt(String.valueOf(tm.getValueAt(Acceso.jTable_acceso_detalle.getSelectedRow(), 0)));

    }

    public synchronized static void Facturacion_detalle_selected() {
//        DefaultTableModel tm = (DefaultTableModel) Facturacion_coronel_bogado.jTable_detalle.getModel();
//        id_facturacion_detalle = Integer.parseInt(String.valueOf(tm.getValueAt(Facturacion_coronel_bogado.jTable_detalle.getSelectedRow(), 0)));
    }

    public synchronized static void Facturacion_selected() {
//        DefaultTableModel tm = (DefaultTableModel) Facturacion_coronel_bogado.jTable_buscar.getModel();
//        id_facturacion = Integer.parseInt(String.valueOf(tm.getValueAt(Facturacion_coronel_bogado.jTable_buscar.getSelectedRow(), 0)));

    }

    public synchronized static void Caja_selected() {
        DefaultTableModel tm = (DefaultTableModel) Caja.jTable1.getModel();
        id_caja = Integer.parseInt(String.valueOf(tm.getValueAt(Caja.jTable1.getSelectedRow(), 0)));
        Caja.jTextField_ultimo.setText(String.valueOf(tm.getValueAt(Caja.jTable1.getSelectedRow(), 1)));
        Caja.jTextField_descripcion.setText(String.valueOf(tm.getValueAt(Caja.jTable1.getSelectedRow(), 2)));
    }

    public synchronized static void Facturacion_seguro_selected() {
//        DefaultTableModel tm = (DefaultTableModel) Facturacion_coronel_bogado.jTable_seguro.getModel();
//        facturacion_id_seguro = Integer.parseInt(String.valueOf(tm.getValueAt(Facturacion_coronel_bogado.jTable_seguro.getSelectedRow(), 0)));
//        Facturacion_coronel_bogado.jTextField_seguro.setText(String.valueOf(tm.getValueAt(Facturacion_coronel_bogado.jTable_seguro.getSelectedRow(), 1)));
    }

    public synchronized static void Seguro_beneficiario_selected() {
//        DefaultTableModel tm = (DefaultTableModel) Contrato.jTable_beneficiario.getModel();
//        seguro_id_beneficiario = Integer.parseInt(String.valueOf(tm.getValueAt(Contrato.jTable_beneficiario.getSelectedRow(), 0)));
//        Contrato.jTextField_beneficiario.setText(String.valueOf(tm.getValueAt(Contrato.jTable_beneficiario.getSelectedRow(), 1)));
    }

    public synchronized static void Seguro_plan_selected() {
//        DefaultTableModel tm = (DefaultTableModel) Contrato.jTable_beneficiario1.getModel();
//        seguro_id_plan = Integer.parseInt(String.valueOf(tm.getValueAt(Contrato.jTable_beneficiario1.getSelectedRow(), 0)));
//        Contrato.jTextField_plan.setText(String.valueOf(tm.getValueAt(Contrato.jTable_beneficiario1.getSelectedRow(), 1)));
    }

    public synchronized static void Configuracion_caja_detalle_selected() {
//        DefaultTableModel tm = (DefaultTableModel) Configuraciones.jTable_caja.getModel();
//        configuraciones_id_caja_usuario = Integer.parseInt(String.valueOf(tm.getValueAt(Configuraciones.jTable_caja.getSelectedRow(), 0)));
    }

    public synchronized static void Configuraciones_usuarios_selected() {
//        DefaultTableModel tm = (DefaultTableModel) Configuraciones.jTable_usuario.getModel();
//        configuraciones_id_usuario = Integer.parseInt(String.valueOf(tm.getValueAt(Configuraciones.jTable_usuario.getSelectedRow(), 0)));
//        Configuraciones.jTextField_usuario.setText(String.valueOf(tm.getValueAt(Configuraciones.jTable_usuario.getSelectedRow(), 1)));
    }

    public synchronized static void Configuraciones_caja_buscar_selected() {
//        DefaultTableModel tm = (DefaultTableModel) Configuraciones.jTable_caja_buscar.getModel();
//        configuraciones_id_caja = Integer.parseInt(String.valueOf(tm.getValueAt(Configuraciones.jTable_caja_buscar.getSelectedRow(), 0)));
//        Configuraciones.jTextField_caja.setText(String.valueOf(tm.getValueAt(Configuraciones.jTable_caja_buscar.getSelectedRow(), 1)));
    }

    public synchronized static void Productos_selected() {
//        DefaultTableModel tm = (DefaultTableModel) Servicios.jTable_productos.getModel();
//        id_producto = Integer.parseInt(String.valueOf(tm.getValueAt(Servicios.jTable_productos.getSelectedRow(), 0)));
    }

    public synchronized static void Farmacia_social_productos_selected() {
//        DefaultTableModel tm = (DefaultTableModel) Farmacia_social_productos.jTable_buscar.getModel();
//        farmacia_social_producto_id_producto = Integer.parseInt(String.valueOf(tm.getValueAt(Farmacia_social_productos.jTable_buscar.getSelectedRow(), 0)));
    }

    public synchronized static void Beneficiario_buscar_selected() {
        DefaultTableModel tm = (DefaultTableModel) Beneficiarios.jTable_buscar.getModel();
        id_beneficiario = Integer.parseInt(String.valueOf(tm.getValueAt(Beneficiarios.jTable_buscar.getSelectedRow(), 0)));
    }

    public synchronized static void Beneficiario_selected_contrato() {
        DefaultTableModel tm = (DefaultTableModel) Beneficiarios.jTable_buscar.getModel();
        contrato_id_beneficiario = Integer.parseInt(String.valueOf(tm.getValueAt(Beneficiarios.jTable_buscar.getSelectedRow(), 0)));
    }

    public synchronized static void Beneficiario_grupo_sanguineo_selected() {
        DefaultTableModel tm = (DefaultTableModel) Beneficiarios.jTable_grupo_sanguineo.getModel();
        beneficiarios_id_grupo_sanguineo = Integer.parseInt(String.valueOf(tm.getValueAt(Beneficiarios.jTable_grupo_sanguineo.getSelectedRow(), 0)));
//        Beneficiarios.jTextField_grupo_sanguineo.setText(String.valueOf(tm.getValueAt(Beneficiarios.jTable_grupo_sanguineo.getSelectedRow(), 1)));
    }

    public synchronized static void Beneficiario_barrio_selected() {
        DefaultTableModel tm = (DefaultTableModel) Beneficiarios.jTable_barrio.getModel();
        beneficiarios_id_barrio = Integer.parseInt(String.valueOf(tm.getValueAt(Beneficiarios.jTable_barrio.getSelectedRow(), 0)));
        Beneficiarios.jTextField_barrio.setText(String.valueOf(tm.getValueAt(Beneficiarios.jTable_barrio.getSelectedRow(), 1)));
    }


    public static String db = null;
    public static String host = null;
    public static String user = null;
    public static String pass = null;

    public static void Iniciar_Conexion() {
        db = "sms";
        host = "localhost";
        user = "postgres";
        pass = "postgres";
        try {
            conexion = DriverManager.getConnection("jdbc:postgresql://" + host + ":5433/" + db, user, pass);
        } catch (SQLException ex) {
            Logger.getLogger(Code.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static void Cerrar_Conexion() {
        try {
            if (conexion != null) {
                conexion.close();
                System.err.println("Conexion finalizada");
            }
        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }
    }

    public synchronized static String SepararMiles(String txtprec) {
        String valor = txtprec;

        int largo = valor.length();
        if (largo > 9) {
            valor = valor.substring(largo - 12, largo - 9) + "." + valor.substring(largo - 9, largo - 6) + "." + valor.substring(largo - 6, largo - 3) + "." + valor.substring(largo - 3, largo);
        }
        if (largo > 8) {
            valor = valor.substring(largo - 9, largo - 6) + "." + valor.substring(largo - 6, largo - 3) + "." + valor.substring(largo - 3, largo);
        } else if (largo > 7) {
            valor = valor.substring(largo - 8, largo - 6) + "." + valor.substring(largo - 6, largo - 3) + "." + valor.substring(largo - 3, largo);
        } else if (largo > 6) {
            valor = valor.substring(largo - 7, largo - 6) + "." + valor.substring(largo - 6, largo - 3) + "." + valor.substring(largo - 3, largo);
        } else if (largo > 5) {
            valor = valor.substring(largo - 6, largo - 3) + "." + valor.substring(largo - 3, largo);
        } else if (largo > 4) {
            valor = valor.substring(largo - 5, largo - 3) + "." + valor.substring(largo - 3, largo);
        } else if (largo > 3) {
            valor = valor.substring(largo - 4, largo - 3) + "." + valor.substring(largo - 3, largo);
        }
        txtprec = valor;
        return valor;
    }

    public static Date hoy = new Date();

    static void Farmacia_social_producto_guardar(String codigo, String descripcion, String precio, String iva, String recargo) {
        try {
            if (recargo.length() > 0) {

            } else {
                recargo = "0";
            }

            precio = precio.replace(".", "");

            Statement st = conexion.createStatement();
            ResultSet rs = st.executeQuery("SELECT MAX(id_farmacia_social_producto) FROM farmacia_social_producto");
            if (rs.next()) {
                id_farmacia_social_producto = rs.getInt(1) + 1;
            }

            PreparedStatement stReporteCaja = conexion.prepareStatement("INSERT INTO farmacia_social_producto VALUES(?,?,?,?,?,?)");
            stReporteCaja.setInt(1, id_farmacia_social_producto);
            stReporteCaja.setString(2, descripcion);
            stReporteCaja.setLong(3, Long.parseLong(precio));
            stReporteCaja.setString(4, codigo);
            stReporteCaja.setInt(5, Integer.parseInt(iva));
            stReporteCaja.setInt(6, Integer.parseInt(recargo));
            stReporteCaja.executeUpdate();

            JOptionPane.showMessageDialog(null, "Guardado correctamente.");

        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }
    }

    static void Productos_guardar(String descripcion, String precio, String iva, boolean bloquear) {
        try {

            if (isNumericLong(precio.replace(".", ""))) {

                Statement st = conexion.createStatement();
                ResultSet rs = st.executeQuery("SELECT MAX(id_producto) FROM producto");
                if (rs.next()) {
                    id_producto = rs.getInt(1) + 1;
                }

                PreparedStatement stReporteCaja = conexion.prepareStatement("INSERT INTO producto VALUES(?,?,?,?)");
                stReporteCaja.setInt(1, id_producto);
                stReporteCaja.setString(2, descripcion);
                stReporteCaja.setLong(3, Long.parseLong(precio.replace(".", "")));
                stReporteCaja.setInt(4, Integer.parseInt(iva));
//                stReporteCaja.setInt(5, productos_id_tipo);

//                if (bloquear == true) {
//                    stReporteCaja.setInt(6, 1);
//                } else {
//                    stReporteCaja.setInt(6, 0);
//                }
                stReporteCaja.executeUpdate();

                JOptionPane.showMessageDialog(null, "Guardado correctamente.");
            } else {
                JOptionPane.showMessageDialog(null, "Precio no valido.");
            }

        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }
    }

    static void Caja_guardar(String descripcion, String ultimo) {
        try {
            Statement st = conexion.createStatement();
            ResultSet rs = st.executeQuery("SELECT MAX(id_caja) FROM caja");
            if (rs.next()) {
                id_caja = rs.getInt(1) + 1;
            }

            PreparedStatement stReporteCaja = conexion.prepareStatement("INSERT INTO caja VALUES(?,?,?)");
            stReporteCaja.setInt(1, id_caja);
            stReporteCaja.setLong(2, Long.parseLong(ultimo));
            stReporteCaja.setString(3, descripcion);
            stReporteCaja.executeUpdate();

            JOptionPane.showMessageDialog(null, "Guardado correctamente.");

        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }
    }

    public static void Productos_update(String descripcion, String precio, String iva, boolean bloquear) {
        try {
            if (isNumericLong(precio.replace(".", ""))) {

                if (bloquear == true) {
                    PreparedStatement stUpdateAuxiliar4 = conexion.prepareStatement(""
                            + "UPDATE producto "
                            + "SET descripcion = '" + descripcion + "', "
                            + "precio = '" + Long.parseLong(precio.replace(".", "")) + "', "
                            + "iva = '" + Integer.parseInt(iva) + "' "
                            //                            + "bloquear = '1', "
                            //                            + "id_tipo = '" + productos_id_tipo + "' "
                            + "WHERE id_producto = '" + id_producto + "'");
                    stUpdateAuxiliar4.executeUpdate();

                } else {
                    PreparedStatement stUpdateAuxiliar4 = conexion.prepareStatement(""
                            + "UPDATE producto "
                            + "SET descripcion = '" + descripcion + "', "
                            + "precio = '" + Long.parseLong(precio.replace(".", "")) + "', "
                            + "iva = '" + Integer.parseInt(iva) + "' "
                            //                            + "bloquear = '0', "
                            //                            + "id_tipo = '" + productos_id_tipo + "' "
                            + "WHERE id_producto = '" + id_producto + "'");
                    stUpdateAuxiliar4.executeUpdate();

                }

                JOptionPane.showMessageDialog(null, "Actualizado correctamente.");
            } else {
                JOptionPane.showMessageDialog(null, "Precio no valido.");
            }

        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }

    }

    public static void Contrato_update_titular() {
        try {
            PreparedStatement stUpdateAuxiliar4 = conexion.prepareStatement(""
                    + "UPDATE seguro_solidario "
                    + "SET id_beneficiario = '" + contrato_id_titular + "' "
                    + "WHERE id_seguro = '" + id_seguro_solidario + "'");
            stUpdateAuxiliar4.executeUpdate();
        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }

    }


    static void Configuraciones_caja_guardar() {
        try {
            Statement st = conexion.createStatement();
            ResultSet rs = st.executeQuery("SELECT MAX(id_caja_usuario) FROM caja_usuario");
            if (rs.next()) {
                id_caja_usuario = rs.getInt(1) + 1;
            }

            PreparedStatement stReporteCaja = conexion.prepareStatement("INSERT INTO caja_usuario VALUES(?,?,?)");
            stReporteCaja.setInt(1, id_caja_usuario);
            stReporteCaja.setInt(2, configuraciones_id_caja);
            stReporteCaja.setInt(3, configuraciones_id_usuario);
            stReporteCaja.executeUpdate();

        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }
    }

    static void beneficiarios_subbeneficiario_guardar() {
        try {
            Statement st = conexion.createStatement();
            ResultSet rs = st.executeQuery("SELECT MAX(id_subbeneficiario) FROM subbeneficiario");
            if (rs.next()) {
                id_subbeneficiario = rs.getInt(1) + 1;
            }

            PreparedStatement stReporteCaja = conexion.prepareStatement("INSERT INTO subbeneficiario VALUES(?,?,?)");
            stReporteCaja.setInt(1, id_subbeneficiario);
            stReporteCaja.setInt(2, beneficiarios_id_sub_beneficiario);
            stReporteCaja.setInt(3, id_beneficiario);
            stReporteCaja.executeUpdate();

        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }
    }

    static void Acceso_guardar() {
        try {
            Statement st = conexion.createStatement();
            ResultSet rs = st.executeQuery("SELECT MAX(id_acceso) FROM acceso");
            if (rs.next()) {
                id_acceso = rs.getInt(1) + 1;
            }

            PreparedStatement stReporteCaja = conexion.prepareStatement("INSERT INTO acceso VALUES(?,?,?,?)");
            stReporteCaja.setInt(1, id_acceso);
            stReporteCaja.setInt(2, acceso_id_usuario);
            stReporteCaja.setInt(3, acceso_id_menu);
            stReporteCaja.setInt(4, id_detalle_acceso);
            stReporteCaja.executeUpdate();

        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }
    }

    static void Seguro_guardar(String numero, Date fecha) {
        try {
            if (seguro_id_plan != 0) {

                Verificar_conexion();
                id_seguro_solidario = MAX_tabla("id_seguro", "seguro_solidario");
                PreparedStatement stReporteCaja = conexion.prepareStatement("INSERT INTO seguro_solidario VALUES(?,?,?,?,?)");
                stReporteCaja.setInt(1, id_seguro_solidario);
                stReporteCaja.setString(2, numero);
                stReporteCaja.setInt(3, seguro_id_beneficiario);
                stReporteCaja.setDate(4, util_Date_to_sql_date(fecha));
                stReporteCaja.setInt(5, seguro_id_plan);
                stReporteCaja.executeUpdate();

                JOptionPane.showMessageDialog(null, "Agregado correctamente.");

            } else {
                JOptionPane.showMessageDialog(null, "Selecciona un plan");
            }

        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }
    }

    static void Facturacion_guardar() {
        try {
            Statement st = conexion.createStatement();
            ResultSet rs = st.executeQuery("SELECT MAX(id_facturacion) FROM facturacion ");
            if (rs.next()) {
                id_facturacion = rs.getInt(1) + 1;
            }
            int factura = 0;
            st = conexion.createStatement();
            rs = st.executeQuery("SELECT * "
                    + "FROM caja_usuario "
                    + "inner join caja on caja_usuario.id_caja = caja.id_caja "
                    + "where caja_usuario.id_usuario = '" + id_usuario + "'");
            if (rs.next()) {
                factura = rs.getInt("factura") + 1;
                id_caja = rs.getInt("id_caja");
            }

            PreparedStatement stReporteCaja = conexion.prepareStatement("INSERT INTO facturacion VALUES(?,?,?,?,?,?,?,?,?)");
            stReporteCaja.setInt(1, id_facturacion);
            stReporteCaja.setInt(2, 0);
            stReporteCaja.setDate(3, util_Date_to_sql_date(hoy));
            stReporteCaja.setInt(4, 0);
            stReporteCaja.setInt(5, 0);
            stReporteCaja.setInt(6, 2);
            stReporteCaja.setInt(7, factura);
            stReporteCaja.setInt(8, 0);
            stReporteCaja.setInt(9, 0);
            stReporteCaja.executeUpdate();

//            Facturacion_coronel_bogado.cod_factura.setText(String.valueOf(id_facturacion));
            Code.Auditoria_guardar("GENERADO", "FACTURA", "Número: " + factura);

        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }
    }

    static void Seguro_update(String numero, Date fecha) {
        try {
            PreparedStatement stUpdateAuxiliar4 = conexion.prepareStatement(""
                    + "UPDATE seguro_solidario "
                    + "SET numero = '" + numero + "', "
                    + "id_plan = '" + seguro_id_plan + "', "
                    + "id_beneficiario = '" + seguro_id_beneficiario + "' "
                    + "WHERE id_seguro = '" + id_seguro_solidario + "'");
            stUpdateAuxiliar4.executeUpdate();

            Code.Auditoria_guardar("ACTUALIZADO", "SEGURO", "Número: " + numero);
            JOptionPane.showMessageDialog(null, "Actualizado correctamnete.");
        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }
    }

//    public static void Print() {
//        try {
//
//            String ruta = "c:/Sistema/archivo.txt";
//            File archivo = new File(ruta);
//            BufferedWriter bw;
//            if (archivo.exists()) {
//                bw = new BufferedWriter(new FileWriter(archivo));
////                bw.write("El fichero de texto ya estaba creado.");
//            } else {
//                bw = new BufferedWriter(new FileWriter(archivo));
////                bw.write("Acabo de crear el fichero de texto.");
//                bw.close();
//            }
//
//            ///imprimir a archivo 
//            bw.newLine();
//            bw.newLine();
//            bw.newLine();
//            bw.newLine();
//            bw.newLine();
//            bw.newLine();
//
//            Statement st = conexion.createStatement();
//            ResultSet rs = st.executeQuery(""
//                    + "SELECT * FROM "
//                    + "facturacion "
//                    + "inner join beneficiario on beneficiario.id_beneficiario = facturacion.id_beneficiario "
//                    + "where id_facturacion = '" + id_facturacion + "' "
//                    + "");
//            if (rs.next()) {
//
//                long total = rs.getLong("total") - rs.getLong("seguro");
//
//                if (total < 0) {
//                    total = 0;
//                }
//
//                PreparedStatement stUpdateAuxiliar4 = conexion.prepareStatement(""
//                        + "UPDATE facturacion "
//                        + "SET total = '" + total + "' "
//                        + "WHERE id_facturacion = '" + id_facturacion + "'");
//                stUpdateAuxiliar4.executeUpdate();
//
//                String nombre = rs.getString("nombre").toUpperCase().trim();
//                long seguro = rs.getLong("id_facturacion_seguro");
//
//                String direccion = rs.getString("direccion").trim();
//
//                int nombre_tamaño = nombre.length();
//                String espacio = "";
//                while (nombre_tamaño < 25) {
//                    espacio = espacio + " ";
//                    nombre_tamaño = nombre_tamaño + 1;
//                }
//                int direccion_tamaño = direccion.length();
//                String espacio_direccion = "";
//                while (direccion_tamaño < 32) {
//                    espacio_direccion = espacio_direccion + " ";
//                    direccion_tamaño = direccion_tamaño + 1;
//                }
//                String fecha = rs.getString("fecha");
//
//                String dia_str = fecha.substring(8, 10);
//                String mes_str = fecha.substring(5, 7);
//                String ano_str = fecha.substring(2, 4);
//
//                bw.write("              " + dia_str + "       " + mes_str + "       " + ano_str + "             X");
//                bw.newLine();
//                bw.write("                " + nombre + espacio + rs.getString("documento"));
//                bw.newLine();
//                bw.write("           " + rs.getString("direccion").trim() + espacio_direccion + rs.getString("telefono"));
//
//                bw.newLine();
//                bw.newLine();
//                bw.newLine();
//                bw.newLine();
//                bw.newLine();
//
//                int lineas = 0;
//                long exentas = 0;
//                long iva5 = 0;
//                long iva10 = 0;
//                st = conexion.createStatement();
//                rs = st.executeQuery(""
//                        + "SELECT * FROM "
//                        + "facturacion_detalle "
//                        + "inner join producto on producto.id_producto = facturacion_detalle.id_producto "
//                        + "where id_facturacion = '" + id_facturacion + "' "
//                        + "");
//                while (rs.next()) {
//                    exentas = exentas + rs.getLong("exentas");
//                    iva5 = iva5 + rs.getLong("iva5");
//                    iva10 = iva10 + rs.getLong("iva10");
//                    String producto = rs.getString("descripcion") + "                  ";
//
//                    int producto_tamaño = producto.length();
//                    String espacio_producto = "";
//                    while (producto_tamaño < 19) {
//                        espacio_producto = espacio_producto + " ";
//                        producto_tamaño = producto_tamaño + 1;
//                    }
//                    String nombre_recortado = producto.substring(0, 18);
//                    String esp = "";
//                    if (rs.getString("precio").length() <= 3) {
//                        esp = " ";
//                    }
//                    if (rs.getString("precio").length() <= 4) {
//                        esp = " " + esp;
//                    }
//
//                    bw.write("          " + rs.getString("unidades") + "  " + nombre_recortado + "  " + rs.getString("precio") + "   " + esp + rs.getString("exentas") + "   " + rs.getString("iva5") + "   " + rs.getString("iva10"));
//                    lineas = lineas + 1;
//                    bw.newLine();
//                }
//
//                if (seguro > 0) {
//                    if ((exentas + iva5 + iva10 - 10000) > 0) {
//                        bw.write("             APORTE SOLIDARIO:    " + (exentas + iva5 + iva10 - 10000));
//                    } else {
//                        bw.write("             APORTE SOLIDARIO:      0 ");
//                    }
//                }
//
//                while (lineas < 14) {
//                    bw.newLine();
//                    lineas = lineas + 1;
//                }
//                bw.write("                                         " + total + "    " + iva5 + "  " + iva10);
//                bw.newLine();
//                bw.write("              " + Numero_a_String(total));
//
//                while (lineas < 33) {
//                    bw.newLine();
//                    lineas = lineas + 1;
//                }
//            }
//
//            bw.flush();
//            bw.close();
//
//            FileInputStream inputStream = null;
//            inputStream = new FileInputStream("c:/Sistema/archivo.txt");
//            if (inputStream == null) {
//                return;
//            }
//
//            DocFlavor docFormat = DocFlavor.INPUT_STREAM.AUTOSENSE;
//            Doc document = new SimpleDoc(inputStream, docFormat, null);
//            PrintRequestAttributeSet attributeSet = new HashPrintRequestAttributeSet();
//            PrintService defaultPrintService = PrintServiceLookup.lookupDefaultPrintService();
//            if (defaultPrintService != null) {
//                DocPrintJob printJob = defaultPrintService.createPrintJob();
//                printJob.print(document, attributeSet);
//            } else {
//                System.err.println("No existen impresoras instaladas");
//            }
//            inputStream.close();
//
//            PreparedStatement stUpdateAuxiliar4 = conexion.prepareStatement(""
//                    + "UPDATE facturacion "
//                    + "SET id_estado = '2' "
//                    + "WHERE id_facturacion = '" + id_facturacion + "'");
//            stUpdateAuxiliar4.executeUpdate();
//
//        } catch (IOException | PrintException | SQLException | ClassNotFoundException ex) {
//            MostrarError(ex, getNombreMetodo());
//        }
//    }
//    public static void Farmacia_social_facturacion_print() {
//        try {
//
//            String ruta = "c:/Sistema/archivo.txt";
//            File archivo = new File(ruta);
//            BufferedWriter bw;
//            if (archivo.exists()) {
//                bw = new BufferedWriter(new FileWriter(archivo));
//            } else {
//                bw = new BufferedWriter(new FileWriter(archivo));
//                bw.close();
//            }
//
//            ///imprimir a archivo 
//            bw.newLine();
//            bw.newLine();
//            bw.newLine();
//            bw.newLine();
//            bw.newLine();
//            bw.newLine();
//
//            Statement st = conexion.createStatement();
//            ResultSet rs = st.executeQuery(""
//                    + "SELECT * FROM "
//                    + "fs_facturacion "
//                    + "inner join beneficiario on beneficiario.id_beneficiario = fs_facturacion.id_beneficiario "
//                    + "where id_fs_facturacion = '" + id_fs_facturacion + "' "
//                    + "");
//            if (rs.next()) {
//
//                String nombre = rs.getString("nombre").toUpperCase().trim();
//                long seguro = rs.getLong("id_facturacion_seguro");
//
//                String direccion = rs.getString("direccion").trim();
//
//                int nombre_tamaño = nombre.length();
//                String espacio = "";
//                while (nombre_tamaño < 25) {
//                    espacio = espacio + " ";
//                    nombre_tamaño = nombre_tamaño + 1;
//                }
//                int direccion_tamaño = direccion.length();
//                String espacio_direccion = "";
//                while (direccion_tamaño < 32) {
//                    espacio_direccion = espacio_direccion + " ";
//                    direccion_tamaño = direccion_tamaño + 1;
//                }
//                String fecha = rs.getString("fecha");
//
//                String dia_str = fecha.substring(8, 10);
//                String mes_str = fecha.substring(5, 7);
//                String ano_str = fecha.substring(2, 4);
//
//                bw.write("              " + dia_str + "       " + mes_str + "       " + ano_str + "             X");
//                bw.newLine();
//                bw.write("                " + nombre + espacio + rs.getString("documento"));
//                bw.newLine();
//                bw.write("           " + rs.getString("direccion").trim() + espacio_direccion + rs.getString("telefono"));
//
//                bw.newLine();
//                bw.newLine();
//                bw.newLine();
//                bw.newLine();
//                bw.newLine();
//
//                int lineas = 0;
//                long exentas = 0;
//                long iva5 = 0;
//                long iva10 = 0;
//                st = conexion.createStatement();
//                rs = st.executeQuery(""
//                        + "SELECT * FROM "
//                        + "fs_facturacion_detalle "
//                        + "inner join farmacia_social_producto on farmacia_social_producto.id_farmacia_social_producto = fs_facturacion_detalle.id_farmacia_social_producto "
//                        + "where id_fs_facturacion = '" + id_fs_facturacion + "' "
//                        + "");
//                while (rs.next()) {
//                    exentas = exentas + rs.getLong("exentas");
//                    iva5 = iva5 + rs.getLong("iva5");
//                    iva10 = iva10 + rs.getLong("iva10");
//                    String producto = rs.getString("descripcion") + "                  ";
//
//                    int producto_tamaño = producto.length();
//                    String espacio_producto = "";
//                    while (producto_tamaño < 19) {
//                        espacio_producto = espacio_producto + " ";
//                        producto_tamaño = producto_tamaño + 1;
//                    }
//                    String nombre_recortado = producto.substring(0, 18);
//                    String esp = "";
//                    if (rs.getString("precio").length() <= 3) {
//                        esp = " ";
//                    }
//                    if (rs.getString("precio").length() <= 4) {
//                        esp = " " + esp;
//                    }
//
//                    bw.write("          " + rs.getString("unidades") + "  " + nombre_recortado + "  " + rs.getString("precio") + "   " + esp + rs.getString("exentas") + "   " + rs.getString("iva5") + "   " + rs.getString("iva10"));
//                    lineas = lineas + 1;
//                    bw.newLine();
//                }
//                long total = exentas + iva5 + iva10;
////                if (seguro > 0) {
////                    long diez = (((exentas + iva5 + iva10) * 10) / 100);
////                    if ((exentas + iva5 + iva10 - diez) > 0) {
////                        total = exentas + iva5 + iva10 - diez;
////                        bw.write("             APORTE SOLIDARIO:   " + (total));
////                    } else {
////                        total = 0;
////                        bw.write("             APORTE SOLIDARIO:     0 ");
////                    }
////
////                }
//
//                PreparedStatement stUpdateAuxiliar4 = conexion.prepareStatement(""
//                        + "UPDATE fs_facturacion "
//                        + "SET total = '" + total + "' "
//                        + "WHERE id_fs_facturacion = '" + id_fs_facturacion + "'");
//                stUpdateAuxiliar4.executeUpdate();
//
//                while (lineas < 14) {
//                    bw.newLine();
//                    lineas = lineas + 1;
//                }
//                bw.write("                                         " + total + "      0      0");
//                bw.newLine();
//                bw.write("              " + Numero_a_String(total));
//
//                while (lineas < 33) {
//                    bw.newLine();
//                    lineas = lineas + 1;
//                }
//            }
//
//            bw.flush();
//            bw.close();
//
//            FileInputStream inputStream = null;
//            inputStream = new FileInputStream("c:/Sistema/archivo.txt");
//            if (inputStream == null) {
//                return;
//            }
//            DocFlavor docFormat = DocFlavor.INPUT_STREAM.AUTOSENSE;
//            Doc document = new SimpleDoc(inputStream, docFormat, null);
//            PrintRequestAttributeSet attributeSet = new HashPrintRequestAttributeSet();
//            PrintService defaultPrintService = PrintServiceLookup.lookupDefaultPrintService();
//            if (defaultPrintService != null) {
//                DocPrintJob printJob = defaultPrintService.createPrintJob();
//                printJob.print(document, attributeSet);
//            } else {
//                System.err.println("No existen impresoras instaladas");
//            }
//            inputStream.close();
//
//            PreparedStatement stUpdateAuxiliar4 = conexion.prepareStatement(""
//                    + "UPDATE fs_facturacion "
//                    + "SET id_estado = '2' "
//                    + "WHERE id_fs_facturacion = '" + id_fs_facturacion + "'");
//            stUpdateAuxiliar4.executeUpdate();
//
//        } catch (IOException | PrintException | SQLException | ClassNotFoundException ex) {
//            MostrarError(ex, getNombreMetodo());
//        }
//    }
    public synchronized static String Numero_a_String(long numeroINT) throws ClassNotFoundException, SQLException {

        Numero_a_Letra NumLetra = new Numero_a_Letra();
        String cantidad_string = Long.toString(numeroINT);
        String aRemplazar = NumLetra.Convertir(cantidad_string, true);
        String remplazado = aRemplazar.replace("0", "");
        return remplazado;

    }

    public synchronized static boolean isNumeric(String cadena) {
        try {
            Integer.parseInt(cadena);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }

    public synchronized static boolean isNumericLong(String cadena) {
        try {
            Long.parseLong(cadena);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }

    static void Historial_beneficiario(String ficha, String documento, Date desde, Date hasta) {
//        try {
//            String sql = "";
//            if (isNumeric(ficha)) {
//                sql = "select id_facturacion_detalle,  fecha, nombre, descripcion "
//                        + "from facturacion_detalle "
//                        + "inner join facturacion on facturacion.id_facturacion = facturacion_detalle.id_facturacion "
//                        + "inner join beneficiario on beneficiario.id_beneficiario = facturacion.id_beneficiario "
//                        + "inner join producto on producto.id_producto = facturacion_detalle.id_producto "
//                        + "where (ficha = '" + ficha + "') "
//                        + "and fecha >= '" + desde + "' and fecha <= '" + hasta + "'  ";
//            }
//            if (isNumeric(documento)) {
//                sql = "select id_facturacion_detalle, fecha, nombre, descripcion "
//                        + "from facturacion_detalle "
//                        + "inner join facturacion on facturacion.id_facturacion = facturacion_detalle.id_facturacion "
//                        + "inner join beneficiario on beneficiario.id_beneficiario = facturacion.id_beneficiario "
//                        + "inner join producto on producto.id_producto = facturacion_detalle.id_producto "
//                        + "where (documento = '" + documento + "') "
//                        + "and fecha >= '" + desde + "' and fecha <= '" + hasta + "'  ";
//            }
//
//            PreparedStatement ps = conexion.prepareStatement(sql);
//            ResultSet rs = ps.executeQuery();
//            ResultSetMetaData rsm = rs.getMetaData();
//            DefaultTableModel dtm = (DefaultTableModel) Historial_Beneficiarios.jTable_historial.getModel();
//            for (int j = 0; j < Historial_Beneficiarios.jTable_historial.getRowCount(); j++) {
//                dtm.removeRow(j);
//                j -= 1;
//            }
//
//            ArrayList<Object[]> data = new ArrayList<>();
//            while (rs.next()) {
//                Object[] rows = new Object[rsm.getColumnCount()];
//                for (int i = 0; i < rows.length; i++) {
//                    rows[i] = rs.getObject(i + 1).toString().trim();
//                }
//                data.add(rows);
//            }
//
//            dtm = (DefaultTableModel) Historial_Beneficiarios.jTable_historial.getModel();
//            for (int i = 0; i < data.size(); i++) {
//                dtm.addRow(data.get(i));
//            }
//
//        } catch (SQLException ex) {
//            MostrarError(ex, getNombreMetodo());
//        }
    }

    static void Farmacia_social_ventas_beneficiarios_jtable(String buscar) {
//        try {
//            PreparedStatement ps = conexion.prepareStatement(""
//                    + "select id_beneficiario, nombre "
//                    + "from beneficiario "
//                    + "where nombre ilike '%" + buscar + "%'"
//                    + "");
//            ResultSet rs = ps.executeQuery();
//            ResultSetMetaData rsm = rs.getMetaData();
//            DefaultTableModel dtm = (DefaultTableModel) Farmacia_social_ventas.jTable_beneficiario.getModel();
//            for (int j = 0; j < Farmacia_social_ventas.jTable_beneficiario.getRowCount(); j++) {
//                dtm.removeRow(j);
//                j -= 1;
//            }
//
//            ArrayList<Object[]> data = new ArrayList<>();
//            while (rs.next()) {
//                Object[] rows = new Object[rsm.getColumnCount()];
//                for (int i = 0; i < rows.length; i++) {
//                    rows[i] = rs.getObject(i + 1).toString().trim();
//                }
//                data.add(rows);
//            }
//
//            dtm = (DefaultTableModel) Farmacia_social_ventas.jTable_beneficiario.getModel();
//            for (int i = 0; i < data.size(); i++) {
//                dtm.addRow(data.get(i));
//            }
//
//        } catch (SQLException ex) {
//            MostrarError(ex, getNombreMetodo());
//        }
    }

    static void Farmacia_social_facturacion_detalle_guardar(String codigo) {
        try {

            Verificar_conexion();

            long precio = 0;
            long unidades = 1;
            int iva = 0;
            int descuento = 0;
            Statement st = conexion.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM farmacia_social_producto where codigo = '" + codigo + "' ");
            if (rs.next()) {

                fs_facturacion_detalle_id_producto = rs.getInt(1);
                precio = rs.getLong("precio");
                iva = rs.getInt("iva");

                st = conexion.createStatement();
                rs = st.executeQuery("SELECT MAX(id_fs_facturacion_detalle) FROM fs_facturacion_detalle ");
                if (rs.next()) {
                    id_fs_facturacion_detalle = rs.getInt(1) + 1;
                }

                st = conexion.createStatement();
                rs = st.executeQuery("SELECT * from seguro_solidario where id_beneficiario = '" + fs_facturacion_id_beneficiario + "' ");
                if (rs.next()) {
                    precio = precio - (precio * 10 / 100);
                }

                PreparedStatement st2 = conexion.prepareStatement("INSERT INTO fs_facturacion_detalle VALUES(?,?,?,?,?,?,?,?)");
                st2.setInt(1, id_fs_facturacion_detalle);
                st2.setInt(2, id_fs_facturacion);
                st2.setInt(3, fs_facturacion_detalle_id_producto);
                st2.setLong(4, precio);
                st2.setLong(5, unidades);

                if (iva == 0) {
                    st2.setLong(6, unidades * precio);
                } else {
                    st2.setLong(6, 0);
                }
                if (iva == 5) {
                    st2.setLong(7, unidades * precio);
                } else {
                    st2.setLong(7, 0);
                }
                if (iva == 10) {
                    st2.setLong(8, unidades * precio);
                } else {
                    st2.setLong(8, 0);
                }
                st2.executeUpdate();
            }

            Code.Auditoria_guardar("AGREGAR DETALLE", "FACTURA", "");
        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }

    }

    static void Farmacia_social_facturacion_detalle_unidades(String unidades) {
        try {

            long unidades_long = Long.parseLong(unidades);

            Statement st = conexion.createStatement();
            ResultSet rs = st.executeQuery(""
                    + "SELECT * FROM fs_facturacion_detalle "
                    + "inner join farmacia_social_producto on farmacia_social_producto.id_farmacia_social_producto =  fs_facturacion_detalle.id_farmacia_social_producto "
                    + "where id_fs_facturacion_detalle = '" + id_fs_facturacion_detalle + "' ");
            if (rs.next()) {
                long precio = rs.getLong("precio");
                int iva = rs.getInt("iva");
                long exentas = 0;
                long iva5 = 0;
                long iva10 = 0;

                if (iva == 0) {
                    exentas = precio * unidades_long;
                }
                if (iva == 5) {
                    iva5 = precio * unidades_long;
                }
                if (iva == 10) {
                    iva10 = precio * unidades_long;
                }

                PreparedStatement stUpdateAuxiliar4 = conexion.prepareStatement(""
                        + "UPDATE fs_facturacion_detalle "
                        + "SET unidades = '" + unidades_long + "',"
                        + "exentas = '" + exentas + "', "
                        + "iva5 = '" + iva5 + "', "
                        + "iva10 = '" + iva10 + "' "
                        + "WHERE id_fs_facturacion_detalle = '" + id_fs_facturacion_detalle + "'");
                stUpdateAuxiliar4.executeUpdate();

                Code.Auditoria_guardar("ACTUALIZADO", "F.S. FACTURA DETALLE", "--");
            }

        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }
    }

    static void Facturacion_guardar_beneficiario(String documento, String nombre) {
        try {
            float documento_long = 0;

            if ((documento.length() > 0) && (isNumeric(documento))) {
                documento_long = Float.parseFloat(documento);
            }

            boolean existe = false;
            Statement st = conexion.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM beneficiario where documento = '" + documento_long + "'");
            if (rs.next()) {
                existe = true;
                id_beneficiario = rs.getInt("id_beneficiario");
            }

            if (existe == false) {

                id_beneficiario = MAX_tabla("id_beneficiario", "beneficiario");
                PreparedStatement stReporteCaja = conexion.prepareStatement("INSERT INTO beneficiario VALUES(?,?,?,?,?,?,?,?,?,?)");
                stReporteCaja.setInt(1, id_beneficiario);
                stReporteCaja.setString(2, nombre);
                stReporteCaja.setFloat(3, documento_long);
                stReporteCaja.setString(4, "No especificada");
                stReporteCaja.setDate(5, util_Date_to_sql_date(null));
                stReporteCaja.setLong(6, 0);
                stReporteCaja.setInt(7, 1);
                stReporteCaja.setInt(8, 1);
                stReporteCaja.setInt(9, 1);
                stReporteCaja.setLong(10, 0);
                stReporteCaja.executeUpdate();

            }

            PreparedStatement stUpdateAuxiliar4 = conexion.prepareStatement(""
                    + "UPDATE facturacion "
                    + "SET id_beneficiario = '" + id_beneficiario + "' "
                    + "WHERE id_facturacion = '" + id_facturacion + "'");
            stUpdateAuxiliar4.executeUpdate();

        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }
    }

    static void Facturacion_guardar_factura(String factura) {
        try {
            if (isNumericLong(factura)) {
                long fac = Long.parseLong(factura);
                if (fac > 0) {
                    PreparedStatement stUpdateAuxiliar4 = conexion.prepareStatement(""
                            + "UPDATE facturacion "
                            + "SET factura = '" + factura + "' "
                            + "WHERE id_facturacion = '" + id_facturacion + "'");
                    stUpdateAuxiliar4.executeUpdate();

                    stUpdateAuxiliar4 = conexion.prepareStatement(""
                            + "UPDATE caja "
                            + "SET factura = '" + (factura) + "' "
                            + "WHERE id_caja = '" + 1 + "'");
                    stUpdateAuxiliar4.executeUpdate();

                    Code.Factura_imprimir();

                } else {
                    JOptionPane.showMessageDialog(null, "Ingrese un número valido de factura.");
                }
            } else {
                JOptionPane.showMessageDialog(null, "Ingrese un número valido de factura.");
            }
        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());
        }
    }

    static void Facturacion_beneficiario_por_ci(String documento) {
        try {
            float documento_long = Float.parseFloat(documento);
            Statement st = conexion.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM beneficiario where documento = '" + documento_long + "'");
            if (rs.next()) {
                //  Facturacion_coronel_bogado.jt_beneficiario.setText(rs.getString("nombre"));
            }
        } catch (SQLException ex) {
            MostrarError(ex, getNombreMetodo());

        }
    }

    public static class Numero_a_Letra {

        private final String[] UNIDADES = {"", "un ", "dos ", "tres ", "cuatro ", "cinco ", "seis ", "siete ", "ocho ", "nueve "};
        private final String[] DECENAS = {"diez ", "once ", "doce ", "trece ", "catorce ", "quince ", "dieciseis ",
            "diecisiete ", "dieciocho ", "diecinueve", "veinte ", "treinta ", "cuarenta ",
            "cincuenta ", "sesenta ", "setenta ", "ochenta ", "noventa "};
        private final String[] CENTENAS = {"", "ciento ", "doscientos ", "trecientos ", "cuatrocientos ", "quinientos ", "seiscientos ",
            "setecientos ", "ochocientos ", "novecientos "};

        public Numero_a_Letra() {
        }

        public String Convertir(String numero, boolean mayusculas) {
            String literal = "";
            String parte_decimal;
            //si el numero utiliza (.) en lugar de (,) -> se reemplaza
            numero = numero.replace(".", ",");
            //si el numero no tiene parte decimal, se le agrega ,00
            if (numero.indexOf(",") == -1) {
                numero = numero + ",0";
            }
            //se valida formato de entrada -> 0,00 y 999 999 999,00
            if (Pattern.matches("\\d{1,9},\\d{1,2}", numero)) {
                //se divide el numero 0000000,00 -> entero y decimal
                String Num[] = numero.split(",");
                //de da formato al numero decimal
                parte_decimal = Num[1] + "";
                //se convierte el numero a literal
                if (Integer.parseInt(Num[0]) == 0) {//si el valor es cero
                    literal = "cero ";
                } else if (Integer.parseInt(Num[0]) > 999999) {//si es millon
                    literal = getMillones(Num[0]);
                } else if (Integer.parseInt(Num[0]) > 999) {//si es miles
                    literal = getMiles(Num[0]);
                } else if (Integer.parseInt(Num[0]) > 99) {//si es centena
                    literal = getCentenas(Num[0]);
                } else if (Integer.parseInt(Num[0]) > 9) {//si es decena
                    literal = getDecenas(Num[0]);
                } else {//sino unidades -> 9
                    literal = getUnidades(Num[0]);
                }
                //devuelve el resultado en mayusculas o minusculas
                if (mayusculas) {
                    return (literal + parte_decimal).toUpperCase();
                } else {
                    return (literal + parte_decimal);
                }
            } else {//error, no se puede convertir
                return literal = null;
            }
        }

        /* funciones para convertir los numeros a literales */
        private String getUnidades(String numero) {// 1 - 9
            //si tuviera algun 0 antes se lo quita -> 09 = 9 o 009=9
            String num = numero.substring(numero.length() - 1);
            return UNIDADES[Integer.parseInt(num)];
        }

        private String getDecenas(String num) {// 99                        
            int n = Integer.parseInt(num);
            if (n < 10) {//para casos como -> 01 - 09
                return getUnidades(num);
            } else if (n > 19) {//para 20...99
                String u = getUnidades(num);
                if (u.equals("")) { //para 20,30,40,50,60,70,80,90
                    return DECENAS[Integer.parseInt(num.substring(0, 1)) + 8];
                } else {
                    return DECENAS[Integer.parseInt(num.substring(0, 1)) + 8] + "y " + u;
                }
            } else {//numeros entre 11 y 19
                return DECENAS[n - 10];
            }
        }

        private String getCentenas(String num) {// 999 o 099
            if (Integer.parseInt(num) > 99) {//es centena
                if (Integer.parseInt(num) == 100) {//caso especial
                    return " cien ";
                } else {
                    return CENTENAS[Integer.parseInt(num.substring(0, 1))] + getDecenas(num.substring(1));
                }
            } else {//por Ej. 099 
                //se quita el 0 antes de convertir a decenas
                return getDecenas(Integer.parseInt(num) + "");
            }
        }

        private String getMiles(String numero) {// 999 999
            //obtiene las centenas
            String c = numero.substring(numero.length() - 3);
            //obtiene los miles
            String m = numero.substring(0, numero.length() - 3);
            String n = "";
            //se comprueba que miles tenga valor entero
            if (Integer.parseInt(m) > 0) {
                n = getCentenas(m);
                return n + "mil " + getCentenas(c);
            } else {
                return "" + getCentenas(c);
            }

        }

        private String getMillones(String numero) { //000 000 000        
            //se obtiene los miles
            String miles = numero.substring(numero.length() - 6);
            //se obtiene los millones
            String millon = numero.substring(0, numero.length() - 6);
            String n = "";

            int mill = Integer.parseInt(millon);

            if (millon.length() > 1) {
                n = getCentenas(millon) + "millones ";
            } else {
                if (mill == 1) {
                    n = getCentenas(millon) + "millon ";
                }
                if (mill > 1) {
                    n = getCentenas(millon) + "millones ";
                }
            }

            return n + getMiles(miles);
        }
    }

}
