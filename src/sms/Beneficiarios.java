package sms;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

public class Beneficiarios extends javax.swing.JFrame {

    public Beneficiarios() {
        initComponents();
        setLocationRelativeTo(null);
        setTitle("Beneficiarios");
        Code.Verificar_conexion();
        Clear();
        jDateChooser_fecha.setVisible(false);
        jTextField_buscar_beneficiario.requestFocus();
    }

    public static int id_usuario;

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDialog_barrios = new javax.swing.JDialog();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable_barrio = new javax.swing.JTable();
        jDialog_sexo = new javax.swing.JDialog();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable_sexo = new javax.swing.JTable();
        jDialog_grupo_sanguineo = new javax.swing.JDialog();
        jPanel6 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTable_grupo_sanguineo = new javax.swing.JTable();
        jDialog_buscar = new javax.swing.JDialog();
        jPanel7 = new javax.swing.JPanel();
        jDialog_agregar_beneficiario = new javax.swing.JDialog();
        jPanel8 = new javax.swing.JPanel();
        jScrollPane6 = new javax.swing.JScrollPane();
        jTable_buscar_subbeneficiario = new javax.swing.JTable();
        jTextField_buscar_beneficiario1 = new javax.swing.JTextField();
        jPanel1 = new javax.swing.JPanel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel9 = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        jTable_buscar = new javax.swing.JTable();
        jTextField_buscar_beneficiario = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        jTextField_ci = new javax.swing.JTextField();
        jTextField_nombre = new javax.swing.JTextField();
        jTextField_sexo = new javax.swing.JTextField();
        jTextField_direccion = new javax.swing.JTextField();
        jTextField_barrio = new javax.swing.JTextField();
        jDateChooser_fecha = new com.toedter.calendar.JDateChooser();
        jTextField_telefono = new javax.swing.JTextField();
        jTextField_ficha = new javax.swing.JTextField();
        jButton5 = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();

        jDialog_barrios.setAlwaysOnTop(true);
        jDialog_barrios.setUndecorated(true);

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(51, 51, 255)));

        jTable_barrio.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Barrio"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable_barrio.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTable_barrioKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(jTable_barrio);
        if (jTable_barrio.getColumnModel().getColumnCount() > 0) {
            jTable_barrio.getColumnModel().getColumn(0).setResizable(false);
            jTable_barrio.getColumnModel().getColumn(0).setPreferredWidth(30);
            jTable_barrio.getColumnModel().getColumn(1).setResizable(false);
            jTable_barrio.getColumnModel().getColumn(1).setPreferredWidth(300);
        }

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 378, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 285, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(14, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jDialog_barriosLayout = new javax.swing.GroupLayout(jDialog_barrios.getContentPane());
        jDialog_barrios.getContentPane().setLayout(jDialog_barriosLayout);
        jDialog_barriosLayout.setHorizontalGroup(
            jDialog_barriosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jDialog_barriosLayout.setVerticalGroup(
            jDialog_barriosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog_barriosLayout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jDialog_sexo.setAlwaysOnTop(true);
        jDialog_sexo.setUndecorated(true);

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(51, 51, 255)));
        jPanel3.setPreferredSize(new java.awt.Dimension(300, 300));

        jTable_sexo.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Genero"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable_sexo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTable_sexoKeyPressed(evt);
            }
        });
        jScrollPane2.setViewportView(jTable_sexo);
        if (jTable_sexo.getColumnModel().getColumnCount() > 0) {
            jTable_sexo.getColumnModel().getColumn(0).setResizable(false);
            jTable_sexo.getColumnModel().getColumn(0).setPreferredWidth(30);
            jTable_sexo.getColumnModel().getColumn(1).setResizable(false);
            jTable_sexo.getColumnModel().getColumn(1).setPreferredWidth(300);
        }

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 278, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 276, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jDialog_sexoLayout = new javax.swing.GroupLayout(jDialog_sexo.getContentPane());
        jDialog_sexo.getContentPane().setLayout(jDialog_sexoLayout);
        jDialog_sexoLayout.setHorizontalGroup(
            jDialog_sexoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jDialog_sexoLayout.setVerticalGroup(
            jDialog_sexoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        jDialog_grupo_sanguineo.setAlwaysOnTop(true);
        jDialog_grupo_sanguineo.setUndecorated(true);

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));
        jPanel6.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(51, 51, 255)));
        jPanel6.setPreferredSize(new java.awt.Dimension(300, 300));

        jTable_grupo_sanguineo.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Grupo Sanguineo"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable_grupo_sanguineo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTable_grupo_sanguineoKeyPressed(evt);
            }
        });
        jScrollPane4.setViewportView(jTable_grupo_sanguineo);
        if (jTable_grupo_sanguineo.getColumnModel().getColumnCount() > 0) {
            jTable_grupo_sanguineo.getColumnModel().getColumn(0).setResizable(false);
            jTable_grupo_sanguineo.getColumnModel().getColumn(0).setPreferredWidth(30);
            jTable_grupo_sanguineo.getColumnModel().getColumn(1).setResizable(false);
            jTable_grupo_sanguineo.getColumnModel().getColumn(1).setPreferredWidth(300);
        }

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 278, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 276, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jDialog_grupo_sanguineoLayout = new javax.swing.GroupLayout(jDialog_grupo_sanguineo.getContentPane());
        jDialog_grupo_sanguineo.getContentPane().setLayout(jDialog_grupo_sanguineoLayout);
        jDialog_grupo_sanguineoLayout.setHorizontalGroup(
            jDialog_grupo_sanguineoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jDialog_grupo_sanguineoLayout.setVerticalGroup(
            jDialog_grupo_sanguineoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        jDialog_buscar.setResizable(false);

        jPanel7.setBackground(new java.awt.Color(255, 255, 255));
        jPanel7.setPreferredSize(new java.awt.Dimension(600, 600));

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 600, Short.MAX_VALUE)
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 588, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jDialog_buscarLayout = new javax.swing.GroupLayout(jDialog_buscar.getContentPane());
        jDialog_buscar.getContentPane().setLayout(jDialog_buscarLayout);
        jDialog_buscarLayout.setHorizontalGroup(
            jDialog_buscarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        jDialog_buscarLayout.setVerticalGroup(
            jDialog_buscarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, 588, Short.MAX_VALUE)
        );

        jDialog_agregar_beneficiario.setResizable(false);

        jPanel8.setBackground(new java.awt.Color(255, 255, 255));
        jPanel8.setPreferredSize(new java.awt.Dimension(600, 600));

        jTable_buscar_subbeneficiario.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Nombre", "Ficha", "Documento"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable_buscar_subbeneficiario.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable_buscar_subbeneficiarioMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jTable_buscar_subbeneficiarioMouseEntered(evt);
            }
        });
        jTable_buscar_subbeneficiario.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTable_buscar_subbeneficiarioKeyPressed(evt);
            }
        });
        jScrollPane6.setViewportView(jTable_buscar_subbeneficiario);
        if (jTable_buscar_subbeneficiario.getColumnModel().getColumnCount() > 0) {
            jTable_buscar_subbeneficiario.getColumnModel().getColumn(0).setResizable(false);
            jTable_buscar_subbeneficiario.getColumnModel().getColumn(0).setPreferredWidth(30);
            jTable_buscar_subbeneficiario.getColumnModel().getColumn(1).setResizable(false);
            jTable_buscar_subbeneficiario.getColumnModel().getColumn(1).setPreferredWidth(370);
            jTable_buscar_subbeneficiario.getColumnModel().getColumn(2).setResizable(false);
            jTable_buscar_subbeneficiario.getColumnModel().getColumn(2).setPreferredWidth(70);
            jTable_buscar_subbeneficiario.getColumnModel().getColumn(3).setResizable(false);
            jTable_buscar_subbeneficiario.getColumnModel().getColumn(3).setPreferredWidth(100);
        }

        jTextField_buscar_beneficiario1.setBorder(javax.swing.BorderFactory.createTitledBorder("Buscar"));
        jTextField_buscar_beneficiario1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField_buscar_beneficiario1KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextField_buscar_beneficiario1KeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 580, Short.MAX_VALUE)
                    .addComponent(jTextField_buscar_beneficiario1))
                .addContainerGap())
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTextField_buscar_beneficiario1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 226, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jDialog_agregar_beneficiarioLayout = new javax.swing.GroupLayout(jDialog_agregar_beneficiario.getContentPane());
        jDialog_agregar_beneficiario.getContentPane().setLayout(jDialog_agregar_beneficiarioLayout);
        jDialog_agregar_beneficiarioLayout.setHorizontalGroup(
            jDialog_agregar_beneficiarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        jDialog_agregar_beneficiarioLayout.setVerticalGroup(
            jDialog_agregar_beneficiarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, 293, Short.MAX_VALUE)
        );

        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowActivated(java.awt.event.WindowEvent evt) {
                formWindowActivated(evt);
            }
        });
        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                formKeyPressed(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jPanel9.setBackground(new java.awt.Color(255, 255, 255));

        jTable_buscar.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Nombre", "Ficha", "Documento"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable_buscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTable_buscarKeyPressed(evt);
            }
        });
        jScrollPane5.setViewportView(jTable_buscar);
        if (jTable_buscar.getColumnModel().getColumnCount() > 0) {
            jTable_buscar.getColumnModel().getColumn(0).setResizable(false);
            jTable_buscar.getColumnModel().getColumn(0).setPreferredWidth(30);
            jTable_buscar.getColumnModel().getColumn(1).setResizable(false);
            jTable_buscar.getColumnModel().getColumn(1).setPreferredWidth(370);
            jTable_buscar.getColumnModel().getColumn(2).setResizable(false);
            jTable_buscar.getColumnModel().getColumn(2).setPreferredWidth(70);
            jTable_buscar.getColumnModel().getColumn(3).setResizable(false);
            jTable_buscar.getColumnModel().getColumn(3).setPreferredWidth(100);
        }

        jTextField_buscar_beneficiario.setBorder(javax.swing.BorderFactory.createTitledBorder("Buscar"));
        jTextField_buscar_beneficiario.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField_buscar_beneficiarioKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextField_buscar_beneficiarioKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextField_buscar_beneficiario)
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 594, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTextField_buscar_beneficiario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 481, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Buscar Beneficiario", jPanel9);

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));

        jTextField_ci.setBorder(javax.swing.BorderFactory.createTitledBorder("Número de Documento"));
        jTextField_ci.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField_ciKeyPressed(evt);
            }
        });

        jTextField_nombre.setBorder(javax.swing.BorderFactory.createTitledBorder("Nombre y Apellido"));
        jTextField_nombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField_nombreKeyPressed(evt);
            }
        });

        jTextField_sexo.setToolTipText("ENTER Buscar");
        jTextField_sexo.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Genero", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(51, 51, 255))); // NOI18N
        jTextField_sexo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField_sexoActionPerformed(evt);
            }
        });
        jTextField_sexo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField_sexoKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextField_sexoKeyReleased(evt);
            }
        });

        jTextField_direccion.setBorder(javax.swing.BorderFactory.createTitledBorder("Dirección"));
        jTextField_direccion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField_direccionKeyPressed(evt);
            }
        });

        jTextField_barrio.setToolTipText("ENTER Buscar - F1 Agregar");
        jTextField_barrio.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Barrio", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(51, 51, 255))); // NOI18N
        jTextField_barrio.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField_barrioKeyPressed(evt);
            }
        });

        jDateChooser_fecha.setBackground(new java.awt.Color(255, 255, 255));
        jDateChooser_fecha.setBorder(javax.swing.BorderFactory.createTitledBorder("Fecha Nacimiento"));
        jDateChooser_fecha.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jDateChooser_fechaKeyPressed(evt);
            }
        });

        jTextField_telefono.setBorder(javax.swing.BorderFactory.createTitledBorder("Teléfono"));
        jTextField_telefono.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField_telefonoKeyPressed(evt);
            }
        });

        jTextField_ficha.setBorder(javax.swing.BorderFactory.createTitledBorder("Ficha"));
        jTextField_ficha.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField_fichaKeyPressed(evt);
            }
        });

        jButton5.setText("Nuevo (F1)");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });
        jButton5.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jButton5KeyPressed(evt);
            }
        });

        jButton1.setText("Guardar (F4)");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jButton1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jButton1KeyPressed(evt);
            }
        });

        jButton2.setText("Buscar (F5)");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jButton2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jButton2KeyPressed(evt);
            }
        });

        jButton3.setText("Borrar");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jSeparator1.setForeground(new java.awt.Color(102, 102, 255));

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextField_direccion)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jTextField_ci)
                            .addComponent(jTextField_sexo, javax.swing.GroupLayout.DEFAULT_SIZE, 137, Short.MAX_VALUE))
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextField_barrio))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                                .addGap(5, 5, 5)
                                .addComponent(jTextField_nombre, javax.swing.GroupLayout.PREFERRED_SIZE, 452, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextField_ficha, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(jTextField_telefono, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jDateChooser_fecha, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton5, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
            .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.TRAILING)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jDateChooser_fecha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jTextField_ficha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTextField_ci, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField_nombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTextField_sexo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField_barrio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextField_direccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextField_telefono, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 251, Short.MAX_VALUE)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jButton5, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Información General", jPanel4);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowActivated(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowActivated
//        Conexion.Verificar_conexion();
    }//GEN-LAST:event_formWindowActivated

    private void formKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyPressed


    }//GEN-LAST:event_formKeyPressed

    private void jTextField_sexoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField_sexoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField_sexoActionPerformed

    private void jTextField_barrioKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField_barrioKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            jDialog_barrios();
            Code.Beneficiarios_barrios_jtable();
        }

        if (evt.getKeyCode() == KeyEvent.VK_F4) {
            Guardar();
        }
        if (evt.getKeyCode() == KeyEvent.VK_F5) {
            Buscar();
        }
        if (evt.getKeyCode() == KeyEvent.VK_F1) {
            Clear();
        }
    }//GEN-LAST:event_jTextField_barrioKeyPressed

    private void jTable_barrioKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable_barrioKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            Code.Beneficiario_barrio_selected();
            jDialog_barrios.setVisible(false);
        }
        if (evt.getKeyCode() == KeyEvent.VK_ESCAPE) {
            jDialog_barrios.setVisible(false);
        }
    }//GEN-LAST:event_jTable_barrioKeyPressed

    private void jTextField_sexoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField_sexoKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            jDialog_sexo();
            Code.Beneficiario_sexo_jtable();
        }
        if (evt.getKeyCode() == KeyEvent.VK_F4) {
            Guardar();
        }
        if (evt.getKeyCode() == KeyEvent.VK_F5) {
            Buscar();
        }
        if (evt.getKeyCode() == KeyEvent.VK_F1) {
            Clear();
        }

    }//GEN-LAST:event_jTextField_sexoKeyPressed

    private void jTable_sexoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable_sexoKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            jDialog_sexo.setVisible(false);
            Code.Beneficiario_sexo_selected();
        }
        if (evt.getKeyCode() == KeyEvent.VK_ESCAPE) {
            jDialog_sexo.setVisible(false);
        }
    }//GEN-LAST:event_jTable_sexoKeyPressed

    private void jTable_grupo_sanguineoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable_grupo_sanguineoKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            Code.Beneficiario_grupo_sanguineo_selected();
            jDialog_grupo_sanguineo.setVisible(false);
        }
        if (evt.getKeyCode() == KeyEvent.VK_ESCAPE) {
            jDialog_grupo_sanguineo.setVisible(false);
        }
    }//GEN-LAST:event_jTable_grupo_sanguineoKeyPressed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        Guardar();
    }//GEN-LAST:event_jButton1ActionPerformed

    public static void Guardar() {
        boolean error = false;
        String mensaje = "ERROR: ";
        if (Code.beneficiarios_id_barrio == 0) {
            error = true;
            mensaje = mensaje + " (Barrio) ";
        }
        if (Code.beneficiarios_id_sexo == 0) {
            error = true;
            mensaje = mensaje + " (Genero) ";
        }
        if (jTextField_ficha.getText().length() <= 0) {
            error = true;
            mensaje = mensaje + " (Ficha) ";
        }

        if (error == false) {
            if (Code.id_beneficiario == 0) {

                Code.Beneficiario_guardar(jTextField_nombre.getText(), jTextField_ci.getText(), jTextField_direccion.getText(), jDateChooser_fecha.getDate(),
                        jTextField_telefono.getText(), jTextField_ficha.getText());
            } else {
                Code.Beneficiario_update(jTextField_nombre.getText(), jTextField_ci.getText(), jTextField_direccion.getText(), jDateChooser_fecha.getDate(),
                        jTextField_telefono.getText(), jTextField_ficha.getText());
            }
//            Clear();
        } else {
            JOptionPane.showMessageDialog(null, mensaje);
        }
    }

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        Buscar();
    }//GEN-LAST:event_jButton2ActionPerformed

    public void Buscar() {
        jDialog_buscar();
        jTextField_buscar_beneficiario.setText("");
        Code.Beneficiarios_buscar_jtable(jTextField_buscar_beneficiario.getText());
        jTextField_buscar_beneficiario.requestFocus();
    }

    private void jTextField_buscar_beneficiarioKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField_buscar_beneficiarioKeyReleased

    }//GEN-LAST:event_jTextField_buscar_beneficiarioKeyReleased

    private void jTextField_buscar_beneficiarioKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField_buscar_beneficiarioKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            Code.Beneficiarios_buscar_jtable(jTextField_buscar_beneficiario.getText());
        }

        if (evt.getKeyCode() == KeyEvent.VK_ESCAPE) {
            jDialog_buscar.setVisible(false);
        }

        if ((evt.getKeyCode() == KeyEvent.VK_DOWN)) {
            try {
                jTable_buscar.requestFocus();
                Robot r = new Robot();
                r.keyPress(KeyEvent.VK_DOWN);
            } catch (AWTException ex) {
                System.err.println(ex);
            }
        }


    }//GEN-LAST:event_jTextField_buscar_beneficiarioKeyPressed

    private void jTable_buscarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable_buscarKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ESCAPE) {
            jDialog_buscar.setVisible(false);
        }

        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

            if (Code.llamar_de == 1) {
                Code.Beneficiario_selected_contrato();
                Code.Contrato_agregar_beneficiario();
                Code.Contrato_beneficiarios_jtable();
                this.setVisible(false);
                
            } else {

                Code.Beneficiario_buscar_selected();
                Code.Beneficiario_traer_datos();
                try {
                    jTabbedPane1.requestFocus();
                    Robot r = new Robot();
                    r.keyPress(KeyEvent.VK_RIGHT);

                    r = new Robot();
                    r.keyPress(KeyEvent.VK_TAB);

                } catch (AWTException ex) {
                    System.err.println(ex);
                }
            }
        }
        if (evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            try {
                jTabbedPane1.requestFocus();
                Robot r = new Robot();
                r.keyPress(KeyEvent.VK_RIGHT);

                r = new Robot();
                r.keyPress(KeyEvent.VK_TAB);

            } catch (AWTException ex) {
                System.err.println(ex);
            }
        }
    }//GEN-LAST:event_jTable_buscarKeyPressed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        Clear();
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jDateChooser_fechaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jDateChooser_fechaKeyPressed

    }//GEN-LAST:event_jDateChooser_fechaKeyPressed

    private void jTextField_fichaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField_fichaKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_F4) {
            Guardar();
        }
        if (evt.getKeyCode() == KeyEvent.VK_F5) {
            Buscar();
        }
        if (evt.getKeyCode() == KeyEvent.VK_F1) {
            Clear();
        }
    }//GEN-LAST:event_jTextField_fichaKeyPressed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        Code.Beneficiario_delete();
        Clear();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jTextField_ciKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField_ciKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_F4) {
            Guardar();
        }
        if (evt.getKeyCode() == KeyEvent.VK_F5) {
            Buscar();
        }
        if (evt.getKeyCode() == KeyEvent.VK_F1) {
            Clear();
        }
    }//GEN-LAST:event_jTextField_ciKeyPressed

    private void jTextField_nombreKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField_nombreKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_F4) {
            Guardar();
        }
        if (evt.getKeyCode() == KeyEvent.VK_F5) {
            Buscar();
        }
        if (evt.getKeyCode() == KeyEvent.VK_F1) {
            Clear();
        }
    }//GEN-LAST:event_jTextField_nombreKeyPressed

    private void jTextField_direccionKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField_direccionKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_F4) {
            Guardar();
        }
        if (evt.getKeyCode() == KeyEvent.VK_F5) {
            Buscar();
        }
        if (evt.getKeyCode() == KeyEvent.VK_F1) {
            Clear();
        }
    }//GEN-LAST:event_jTextField_direccionKeyPressed

    private void jTextField_telefonoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField_telefonoKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_F4) {
            Guardar();
        }
        if (evt.getKeyCode() == KeyEvent.VK_F5) {
            Buscar();
        }
        if (evt.getKeyCode() == KeyEvent.VK_F1) {
            Clear();
        }
    }//GEN-LAST:event_jTextField_telefonoKeyPressed

    private void jButton5KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButton5KeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_F4) {
            Guardar();
        }
        if (evt.getKeyCode() == KeyEvent.VK_F5) {
            Buscar();
        }
        if (evt.getKeyCode() == KeyEvent.VK_F1) {
            Clear();
        }
    }//GEN-LAST:event_jButton5KeyPressed

    private void jButton2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButton2KeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_F4) {
            Guardar();
        }
        if (evt.getKeyCode() == KeyEvent.VK_F5) {
            Buscar();
        }
        if (evt.getKeyCode() == KeyEvent.VK_F1) {
            Clear();
        }
    }//GEN-LAST:event_jButton2KeyPressed

    private void jButton1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButton1KeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_F4) {
            Guardar();
        }
        if (evt.getKeyCode() == KeyEvent.VK_F5) {
            Buscar();
        }
        if (evt.getKeyCode() == KeyEvent.VK_F1) {
            Clear();
        }
    }//GEN-LAST:event_jButton1KeyPressed

    private void jTextField_sexoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField_sexoKeyReleased

    }//GEN-LAST:event_jTextField_sexoKeyReleased

    private void jTable_buscar_subbeneficiarioKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable_buscar_subbeneficiarioKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            Code.Beneficiario_subbeneficiario_selected();
            Code.beneficiarios_subbeneficiario_guardar();
//            Code.Beneficiarios_sub_beneficiarios_jtable();
            jDialog_agregar_beneficiario.setVisible(false);
        }
    }//GEN-LAST:event_jTable_buscar_subbeneficiarioKeyPressed

    private void jTextField_buscar_beneficiario1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField_buscar_beneficiario1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField_buscar_beneficiario1KeyPressed

    private void jTextField_buscar_beneficiario1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField_buscar_beneficiario1KeyReleased
        Code.Beneficiarios_sub_beneficiario_jtable(jTextField_buscar_beneficiario1.getText());
    }//GEN-LAST:event_jTextField_buscar_beneficiario1KeyReleased

    private void jTable_buscar_subbeneficiarioMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable_buscar_subbeneficiarioMouseClicked

    }//GEN-LAST:event_jTable_buscar_subbeneficiarioMouseClicked

    private void jTable_buscar_subbeneficiarioMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable_buscar_subbeneficiarioMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_jTable_buscar_subbeneficiarioMouseEntered

    public static void Clear() {

        Code.id_beneficiario = 0;
        Code.beneficiarios_id_barrio = 0;
        Code.beneficiarios_id_grupo_sanguineo = 0;
        Code.beneficiarios_id_sexo = 0;

        jTextField_barrio.setText("");
        jTextField_ficha.setText("");
        jTextField_ci.setText("");
        jTextField_direccion.setText("");
//        jTextField_grupo_sanguineo.setText("");
        jTextField_nombre.setText("");
        jTextField_sexo.setText("");
        jTextField_telefono.setText("");
        jDateChooser_fecha.setDate(null);

        jTextField_ficha.requestFocus();

    }

    public void jDialog_barrios() {
        jDialog_barrios.setVisible(true);
        jDialog_barrios.setTitle("Barrios");
        jDialog_barrios.setSize(400, 312);
        jDialog_barrios.setLocationRelativeTo(null);

    }

    public void jDialog_sexo() {
        jDialog_sexo.setVisible(true);
        jDialog_sexo.setTitle("Sexo");
        jDialog_sexo.setSize(400, 312);
        jDialog_sexo.setLocationRelativeTo(null);
    }

    public void jDialog_buscar() {
        jDialog_buscar.setVisible(true);
        jDialog_buscar.setTitle("Buscar");
        jDialog_buscar.setSize(600, 588);
        jDialog_buscar.setLocationRelativeTo(null);
    }

    public void jDialog_agregar_beneficiario() {

        int x = 597;
        int y = 293;

        jDialog_agregar_beneficiario.setVisible(true);
        jDialog_agregar_beneficiario.setTitle("Agregar un sub beneficiario");
        jDialog_agregar_beneficiario.setSize(x + 20, y + 40);
        jDialog_agregar_beneficiario.setLocationRelativeTo(null);
    }

    public void jDialog_grupo_sanguineo() {
        jDialog_grupo_sanguineo.setVisible(true);
        jDialog_grupo_sanguineo.setTitle("Grupo Sanguineo");
        jDialog_grupo_sanguineo.setSize(400, 312);
        jDialog_grupo_sanguineo.setLocationRelativeTo(null);
    }

    public static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Beneficiarios.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        java.awt.EventQueue.invokeLater(() -> {
            new Beneficiarios().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton5;
    public static com.toedter.calendar.JDateChooser jDateChooser_fecha;
    private javax.swing.JDialog jDialog_agregar_beneficiario;
    private javax.swing.JDialog jDialog_barrios;
    private javax.swing.JDialog jDialog_buscar;
    private javax.swing.JDialog jDialog_grupo_sanguineo;
    private javax.swing.JDialog jDialog_sexo;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTabbedPane jTabbedPane1;
    public static javax.swing.JTable jTable_barrio;
    public static javax.swing.JTable jTable_buscar;
    public static javax.swing.JTable jTable_buscar_subbeneficiario;
    public static javax.swing.JTable jTable_grupo_sanguineo;
    public static javax.swing.JTable jTable_sexo;
    public static javax.swing.JTextField jTextField_barrio;
    private javax.swing.JTextField jTextField_buscar_beneficiario;
    private javax.swing.JTextField jTextField_buscar_beneficiario1;
    public static javax.swing.JTextField jTextField_ci;
    public static javax.swing.JTextField jTextField_direccion;
    public static javax.swing.JTextField jTextField_ficha;
    public static javax.swing.JTextField jTextField_nombre;
    public static javax.swing.JTextField jTextField_sexo;
    public static javax.swing.JTextField jTextField_telefono;
    // End of variables declaration//GEN-END:variables
}
